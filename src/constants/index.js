export const comodidades = [
    { label: 'Cocina', value: 'Cocina' },
    { label: 'Nevera', value: 'Nevera' },
    { label: 'Microondas', value: 'Microondas' },
    { label: 'Lavadora', value: 'Lavadora' },
    { label: 'Secadora', value: 'Secadora' },
    { label: 'TV en sala', value: 'TV en sala' },
    { label: 'TV en habitación', value: 'TV en habitación' },
    { label: 'Comedor', value: 'Comedor' },
    { label: 'Muebles', value: 'Muebles' },
    { label: 'Cama matrimonial', value: 'Cama matrimonial' }
];

export const servicios = [
    { label: 'Luz', value: 'Luz' },
    { label: 'Agua', value: 'Agua' },
    { label: 'Teléfono', value: 'Teléfono' },
    { label: 'Internet residencial', value: 'Internet residencial' },
    { label: 'Otro', value: 'Otro' }
];
