const initialState = {
	iniciarSesion: false,
	seleccionarInfo: false,
	cedula: false,
	email: false,
	telefono: false,
	message: false
};
export default (state = initialState, { type, ...action }) => {
	switch (type) {
		case 'SetIniciarSesion':
			return { ...state, iniciarSesion: action.payload };
		case 'SetSeleccionarInfo':
			return { ...state, seleccionarInfo: action.payload };
		case 'SetCedula':
			return { ...state, cedula: action.payload };
		case 'SetTelefono':
			return { ...state, telefono: action.payload };
		case 'SetEmail':
			return { ...state, email: action.payload };
		case 'SetMessage':
			return { ...state, message: action.payload };
		default:
			return state;
	}
};
