const initialState = {
    images: {},
    videos: {}
};
export default (state = initialState, { type, ...action }) => {
    switch (type) {
        case 'SetImages':
            return { ...state, images: action.payload };
        case 'SetVideos':
            return { ...state, videos: action.payload };
        default:
            return state;
    }
};
