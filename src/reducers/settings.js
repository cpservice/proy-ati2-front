const initialState = {
    opt6: false,
    logged: false
};
export default (state = initialState, { type, ...action }) => {
    switch (type) {
        case "SetRegisterOpt6":
            return { ...state, opt6: action.payload };
        case "SetLogged":
            return { ...state, logged: action.payload };
        default:
            return state;
    }
};
