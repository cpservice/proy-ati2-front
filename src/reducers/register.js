const initialState = {
    howKnow: {},
    signUpUser: {},
    lang: "",
    login: {},
    frequency: {}
};
export default (state = initialState, { type, ...action }) => {
    switch (type) {
        case "SetHowKnow":
            return { ...state, howKnow: action.payload };
        case "SetSignUpUser":
            return { ...state, signUpUser: action.payload };
        case "SetLang":
            return { ...state, lang: action.payload };
        case "SetLogin":
            return { ...state, login: action.payload };
        case "SetFrecuency":
            return { ...state, frequency: action.payload };
        default:
            return state;
    }
};
