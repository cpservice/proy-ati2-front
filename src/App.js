import React, { Fragment, useEffect, useState } from "react";
import Navbar from "./components/Navbar";
import Router from "./Router";
import Modals from './components/Modals'
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

const App = () => {
    const [showNav, setShowNav] = useState(false);

    useEffect(_ => {
        window.document.onscroll = () => {
            let vertical = window.scrollY;
            if (vertical > 5) {
                setShowNav(true);
            } else {
                setShowNav(false);
            }
        };
    }, []);

    return (
        <Fragment>
            <Navbar />
            <Router />
            {showNav && <Navbar />}
            <Modals/>
            <ToastContainer />
        </Fragment>
    );
};

export default App;
