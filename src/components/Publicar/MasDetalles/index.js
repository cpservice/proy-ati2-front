import React, { Fragment, useState } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import './index.css';

export default function MasDetalles({
    handleDetalles,
    handleCercanias,
    handleUbicacion,
    handlePrecio,
    handleMoneda,
    handleOtraMoneda
}) {
    const [otra, setOtra] = useState(false);
    const handleSelect = e => {
        handleMoneda(e);
        e.target.value === 'otra' ? setOtra(true) : setOtra(false);
    };
    return (
        <Fragment>
            <Row className="text-center mb-5 mt-3">
                <Col>
                    <div className="fotos-viviendas-title">
                        Otros detalles sobre la vivienda
                    </div>
                    <textarea
                        className="mas-detalles-textarea"
                        onChange={handleDetalles}
                    />
                </Col>
            </Row>
            <Row className="text-center">
                <Col>
                    <div className="fotos-viviendas-title">
                        Cercanías de servicios y transporte
                    </div>
                    <textarea
                        className="mas-detalles-textarea"
                        onChange={handleCercanias}
                    />
                </Col>
                <Col>
                    <div className="fotos-viviendas-title">
                        Ubicación exacta
                    </div>
                    <textarea
                        className="mas-detalles-textarea"
                        onChange={handleUbicacion}
                    />
                </Col>
            </Row>
            <Row className="text-center my-4">
                <Col>
                    <Button
                        disabled
                        className="detalles-button mb-2"
                        variant="primary"
                    >
                        Precio
                    </Button>
                    <div>
                        <input
                            type="number"
                            style={{ width: '80%' }}
                            onChange={handlePrecio}
                        />
                    </div>
                </Col>
                <Col>
                    <Button disabled className="detalles-button mb-2">
                        Moneda
                    </Button>
                    <Form.Control
                        as="select"
                        style={{ width: '80%', marginLeft: '10%' }}
                        onChange={handleSelect}
                    >
                        <option value="usd">USD</option>
                        <option value="eur">EUR</option>
                        <option value="otra">Otra</option>
                    </Form.Control>
                </Col>
                {otra ? (
                    <Col>
                        <Button disabled className="detalles-button mb-2">
                            Coloque las siglas de la otra moneda
                        </Button>
                        <div>
                            <input
                                type="text"
                                style={{ width: '80%' }}
                                onChange={handleOtraMoneda}
                                maxLength={4}
                            />
                        </div>
                    </Col>
                ) : null}
            </Row>
        </Fragment>
    );
}
