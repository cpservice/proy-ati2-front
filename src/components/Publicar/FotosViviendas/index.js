import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import ImageInput from './ImageInput';
import './index.css';

export default function FotosViviendas() {
    return (
        <div className="text-center">
            <div className="fotos-viviendas-title">Fotos de la vivienda</div>
            <Card className="text-center">
                <Card.Body>
                    <Row className="mb-2">
                        <Col>
                            Arrastre las fotos que desea cargar en cada uno de
                            los recuadros
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ImageInput id={0} />
                        </Col>
                        <Col>
                            <ImageInput id={1} />
                        </Col>
                        <Col>
                            <ImageInput id={2} />
                        </Col>
                        <Col>
                            <ImageInput id={3} />
                        </Col>
                        <Col>
                            <ImageInput id={4} />
                        </Col>
                        <Col>
                            <ImageInput id={5} />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ImageInput id={6} />
                        </Col>
                        <Col>
                            <ImageInput id={7} />
                        </Col>
                        <Col>
                            <ImageInput id={8} />
                        </Col>
                        <Col>
                            <ImageInput id={9} />
                        </Col>
                        <Col>
                            <ImageInput id={10} />
                        </Col>
                        <Col>
                            <ImageInput id={11} />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ImageInput id={12} />
                        </Col>
                        <Col>
                            <ImageInput id={13} />
                        </Col>
                        <Col>
                            <ImageInput id={14} />
                        </Col>
                        <Col>
                            <ImageInput id={15} />
                        </Col>
                        <Col>
                            <ImageInput id={16} />
                        </Col>
                        <Col>
                            <ImageInput id={17} />
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </div>
    );
}
