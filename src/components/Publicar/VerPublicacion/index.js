import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Button, Accordion, Form } from 'react-bootstrap';
import Flash from '../../Home/Search/Flash.js';
import Detallada from '../../Home/Search/Detallada.js';
import Response from './Response';
import axios from 'axios';

import './index.css';

const url = process.env.REACT_APP_URL + '/api/vivienda';

export default class Services extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list_type: 'foto',
            tipo_vivienda: '',
            vivienda_state: '',
            orden_result: '',
            data: []
        };
    }

    updateData = url => {
        axios
            .get(url)
            .then(res => {
                let pubs = res.data.map(v => {
                    let images = JSON.parse(v.images);
                    let cants = JSON.parse(v.cantidad);
                    return {
                        foto: images[0],
                        tipo_vivienda: v.tipo,
                        vivienda_state: v.vivienda,
                        precio: v.precio,
                        moneda: v.moneda === 'otra' ? v.otra_moneda : v.moneda,
                        habitaciones: cants.rooms,
                        banos: cants.baths,
                        parks: cants.parks,
                        comodidades: 'Comedor, Lavadora, Cocina',
                        servicios: 'Agua, Luz',
                        pais: v.pais,
                        estado: v.estado,
                        ciudad: v.ciudad,
                        zona: v.zona,
                        otros_detalles: v.detalles,
                        servicios_transporte: '',
                        ubicacion: v.ubicacion,
                        datos_contacto: {
                            nombre: 'Carlos',
                            apellido: 'Pino',
                            email: 'correo@ejemplo.com',
                            cod_tlfMovi: '+58',
                            tlf_movil: '4161111111',
                            cod_tlfFijo: '+58',
                            tlf_fijo: '2121111111'
                        },
                        dias_contacto: ['Lunes', 'Martes', 'Jueves'],
                        horas_contacto: {
                            desde: 1,
                            desde_m: 'pm',
                            hasta: 4,
                            hasta_m: 'pm'
                        }
                    };
                });

                this.setState({ data: pubs });
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    componentDidMount() {
        this.updateData(url);
    }

    handleListType = e => {
        switch (e.target.label) {
            case 'Tipo foto':
                this.setState({ list_type: 'foto' });
                break;
            case 'Tipo lista':
                this.setState({ list_type: 'lista' });
                break;
            default:
                break;
        }
    };

    handleTipoVivienda = e => {
        let tipo = e.target.innerText;
        let newUrl = url + '?tipo=' + tipo;
        this.updateData(newUrl);
        this.setState({ tipo_vivienda: e.target.innerText });
    };

    handleViviendaEn = e => {
        let vivienda = e.target.innerText;
        let newUrl = url + '?vivienda=' + vivienda;
        this.updateData(newUrl);
        this.setState({ vivienda_state: e.target.innerText });
    };

    handleOrdenarResults = e => {
        this.setState({ orden_result: e.target.innerText });
    };

    //Resultados del filtro lateral
    handleSearchKey = data => {
        if (data !== false) {
            console.log(data, 'Busqueda por palabra clave');
        }
    };

    handleSearchFlash = data => {
        if (data !== false) {
            console.log(data, 'Busqueda rapida');
        }
    };

    handleSearchDetailed = data => {
        if (data !== false) {
            console.log(data, 'Busqueda detallada');
        }
    };

    render() {
        return (
            <Fragment>
                <Container fluid>
                    <Row>
                        <Col xs={4} className="mt-5">
                            <Accordion>
                                <Flash handleSearch={this.handleSearchFlash} />
                                <Detallada
                                    handleSearch={this.handleSearchDetailed}
                                />
                            </Accordion>
                        </Col>
                        <Col>
                            <Row>
                                <Col xs={12}>
                                    <h5 className="font-weight-light">
                                        Haz clic en la vivienda de tu
                                        preferencia, para ver más información, o
                                        selecciona el adiestramiento de tu
                                        preferencia para habilitar o
                                        deshabilitar los botones expresados a
                                        continuación:
                                    </h5>
                                </Col>
                            </Row>
                            <Row className="my-2">
                                <Col xs={12}>
                                    <Row>
                                        <Col>
                                            <Button variant="primary">
                                                Ver detalle
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Button
                                                variant="success"
                                                className="edit"
                                            >
                                                Modificar
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Button
                                                variant="warning"
                                                className="text-light"
                                            >
                                                Deshabilitar
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Button variant="success">
                                                Habilitar
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Button variant="danger">
                                                Eliminar
                                            </Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className="mb-4">
                                <Col>
                                    <h5 className="font-weight-light">
                                        Haz clic en la vivienda de tu
                                        preferencia, para ver más información
                                    </h5>
                                </Col>
                            </Row>
                            <Row className="mr-2">
                                <Col
                                    xs={12}
                                    className="bg-primary text-light p-2"
                                >
                                    <h6>Publicaciones realizadas</h6>
                                </Col>
                                <Col xs={12}>
                                    <Row className="border border-warning py-2">
                                        <Col>
                                            <p className="text-danger">
                                                Ver listado como:
                                            </p>
                                        </Col>
                                        <Col>
                                            <Form.Check
                                                type="radio"
                                                label="Tipo foto"
                                                name="type-list"
                                                defaultChecked={true}
                                                onChange={e =>
                                                    this.handleListType(e)
                                                }
                                            />
                                        </Col>
                                        <Col>
                                            <Form.Check
                                                type="radio"
                                                label="Tipo lista"
                                                name="type-list"
                                                onChange={e =>
                                                    this.handleListType.bind(e)
                                                }
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="border border-warning py-2">
                                        <p className="text-primary mx-auto">
                                            Seleccione los filtros especificados
                                            a continuación, si desea filtrar los
                                            resultados obtenidos
                                        </p>
                                    </Row>
                                    <Row className="border border-warning py-2">
                                        <Col xs={2}>
                                            <p className="text-danger">
                                                Tipo de vivienda
                                            </p>
                                        </Col>
                                        <Col xs={2}>
                                            <Button
                                                variant="primary"
                                                onClick={e =>
                                                    this.handleTipoVivienda(e)
                                                }
                                            >
                                                Apartamento
                                            </Button>
                                        </Col>
                                        <Col xs={2}>
                                            <Button
                                                variant="warning"
                                                onClick={e =>
                                                    this.handleTipoVivienda(e)
                                                }
                                                className="text-light"
                                            >
                                                Casa o quinta
                                            </Button>
                                        </Col>
                                        <Col xs={3}>
                                            <Button
                                                variant="danger"
                                                onClick={e =>
                                                    this.handleTipoVivienda(e)
                                                }
                                            >
                                                Apartamento y casa
                                            </Button>
                                        </Col>
                                        <Col>
                                            <p className="text-danger">
                                                Ordenar resultados por
                                            </p>
                                        </Col>
                                    </Row>
                                    <Row className="border border-warning py-2">
                                        <Col xs={2}>
                                            <p className="text-danger">
                                                Vivienda en:
                                            </p>
                                        </Col>
                                        <Col xs={2}>
                                            <Button
                                                variant="primary"
                                                onClick={e =>
                                                    this.handleViviendaEn(e)
                                                }
                                            >
                                                Venta
                                            </Button>
                                        </Col>
                                        <Col xs={2}>
                                            <Button
                                                variant="warning"
                                                className="text-light"
                                                onClick={e =>
                                                    this.handleViviendaEn(e)
                                                }
                                            >
                                                Alquiler
                                            </Button>
                                        </Col>
                                        <Col xs={3}>
                                            <Button
                                                variant="danger"
                                                onClick={e =>
                                                    this.handleViviendaEn(e)
                                                }
                                            >
                                                Alquiler y venta
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <Col>
                                                    <Button
                                                        variant="primary"
                                                        onClick={e =>
                                                            this.handleOrdenarResults(
                                                                e
                                                            )
                                                        }
                                                    >
                                                        Precio
                                                    </Button>
                                                </Col>
                                                <Col>
                                                    <Button
                                                        variant="warning"
                                                        onClick={e =>
                                                            this.handleOrdenarResults(
                                                                e
                                                            )
                                                        }
                                                        className="text-light"
                                                    >
                                                        Alquiler
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Response data={this.state.data} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        );
    }
}
