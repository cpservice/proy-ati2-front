import React, { Component, Fragment } from 'react';
import { Row, Col, Button, Form, Pagination } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import iconEdit from '../../../../icons/write.png';
import iconDelete from '../../../../icons/delete.png';
import iconView from '../../../../icons/view.png';
import './index.css';

export default class Services extends Component {
    constructor(props) {
        super(props);

        this.state = {
            publicacion: []
        };
    }

    componentDidMount() {
        // this.setState({
        //     publicacion: [
        //         {
        //             foto: '',
        //             tipo_vivienda: 'Apartamento',
        //             vivienda_state: 'Venta',
        //             precio: '300.000',
        //             moneda: 'USD',
        //             habitaciones: 3,
        //             banos: 2,
        //             estacionamiento: 0,
        //             comodidades: 'Comedor, Lavadora, Cocina',
        //             servicios: 'Agua, Luz',
        //             pais: 'Venezuela',
        //             estado: 'Caracas',
        //             ciudad: 'Caracas',
        //             zona: 'UCV',
        //             otros_detalles: 'Tiene terraza',
        //             servicios_transporte: '',
        //             ubicacion:
        //                 ' Chacao. 3era Transversal de la calle Páez, edf Marinela, piso 1.  Caracas - Venezuela',
        //             datos_contacto: {
        //                 nombre: 'Carlos',
        //                 apellido: 'Pino',
        //                 email: 'correo@ejemplo.com',
        //                 cod_tlfMovi: '+58',
        //                 tlf_movil: '4161111111',
        //                 cod_tlfFijo: '+58',
        //                 tlf_fijo: '2121111111'
        //             },
        //             dias_contacto: ['Lunes', 'Martes', 'Jueves'],
        //             horas_contacto: {
        //                 desde: 1,
        //                 desde_m: 'pm',
        //                 hasta: 4,
        //                 hasta_m: 'pm'
        //             }
        //         }
        //     ]
        // });
    }

    paginaNum = _ => {
        let active = 1;
        let items = [];
        for (let number = 1; number <= 5; number++) {
            items.push(
                <Pagination.Item key={number} active={number === active}>
                    {number}
                </Pagination.Item>
            );
        }

        return items;
    };

    resultModeFoto = _ => {
        // const publicacion = this.state.publicacion;
        const publicacion = this.props.data;

        if (publicacion) {
            const publicaciones = publicacion.map((publicar, key) => {
                return (
                    <Col xs={6} key={key} className="border border-warning p-2">
                        <Row>
                            <Col xs={1}>
                                <Form.Check type="checkbox" name="vivienda" />
                            </Col>
                            <Col>
                                <div className="border b-foto">
                                    <img
                                        width="100"
                                        height="100"
                                        src={publicar.foto}
                                        alt="Foto principal"
                                    />
                                </div>
                            </Col>
                            <Col className="mx-auto">
                                <h5 className="text-primary text-uppercase">
                                    <u>{publicar.tipo_vivienda}</u>
                                </h5>
                                <h5 className="text-danger text-uppercase">
                                    <u>{publicar.vivienda_state}</u>
                                </h5>
                                <span className="d-flex justify-content-end mr-4">
                                    <Link to="./">
                                        <img
                                            width="15"
                                            height="15"
                                            src={iconView}
                                            alt="Ver"
                                        />
                                    </Link>
                                    <Link to="./">
                                        <img
                                            width="15"
                                            height="15"
                                            src={iconEdit}
                                            alt="Editar"
                                        />
                                    </Link>
                                    <Link to="./">
                                        <img
                                            width="15"
                                            height="15"
                                            src={iconDelete}
                                            alt="Eliminar"
                                        />
                                    </Link>
                                </span>
                            </Col>
                        </Row>
                        <Row className="ml-3">
                            <Col>
                                <p className="font-weight-bold">
                                    <span>{publicar.precio}</span>{' '}
                                    <span>{publicar.moneda}</span>
                                </p>
                                <p>
                                    <span>{publicar.habitaciones}</span>{' '}
                                    habitaciones, <span>{publicar.banos}</span>{' '}
                                    baños , <span>{publicar.parks}</span>{' '}
                                    estacionamientos
                                </p>
                                <p>
                                    <span>{publicar.otros_detalles}</span>
                                </p>
                            </Col>
                            <Col>
                                <p>
                                    <span className="font-weight-bold">
                                        <u>País:</u>
                                    </span>{' '}
                                    <span>{publicar.pais}</span>
                                </p>
                                <p>
                                    <span className="font-weight-bold">
                                        <u>Estado:</u>
                                    </span>{' '}
                                    <span>{publicar.estado}</span>
                                </p>
                                <p>
                                    <span className="font-weight-bold">
                                        <u>Zona:</u>
                                    </span>{' '}
                                    <span>{publicar.zona}</span>
                                </p>
                            </Col>
                        </Row>
                        <Row className="">
                            <Col xs={7}>
                                <ul>
                                    <li>
                                        <Button variant="link">
                                            Ver Comodidades
                                        </Button>
                                    </li>
                                    <li>
                                        <Button variant="link">
                                            Ver Servicios
                                        </Button>
                                    </li>
                                    <li>
                                        <Button variant="link">
                                            Ver Ubicación exacta
                                        </Button>
                                    </li>
                                    <li>
                                        <Button variant="link">
                                            Ver Fotos
                                        </Button>
                                    </li>
                                    <li>
                                        <Button variant="link">
                                            Ver Videos
                                        </Button>
                                    </li>
                                </ul>
                            </Col>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                >
                                    Contactar al anunciante
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="text-center">
                                <Button variant="link" className="text-danger">
                                    <u>Ver información completa</u>
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                );
            });

            return publicaciones;
        }
    };

    render() {
        return (
            <Fragment>
                <Row className="border border-warning py-2 d-flex justify-content-center">
                    <Col xs={1}>
                        <p className="text-primary">Página:</p>
                    </Col>
                    <Col xs={3}>
                        <Pagination size="sm">{this.paginaNum()}</Pagination>
                    </Col>
                </Row>
                <Row className="border border-warning py-2">
                    {this.resultModeFoto()}
                </Row>
            </Fragment>
        );
    }
}
