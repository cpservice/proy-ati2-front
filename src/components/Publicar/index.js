import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';
import { comodidades, servicios } from '../../constants';
import FotosViviendas from './FotosViviendas';
import Videos from './Videos';
import Select from 'react-select';
import MasDetalles from './MasDetalles';
import Contacto from './Contacto';
import csc from 'country-state-city';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';

import './index.css';

const url = process.env.REACT_APP_URL + '/api/vivienda';

export default function Publicar() {
    const history = useHistory();

    const [rooms, setRooms] = useState(0);
    const [baths, setBaths] = useState(0);
    const [parks, setParks] = useState(0);
    const [precioRangoBool, setPrecioRangoBool] = useState(false);
    const [countries, setCountries] = useState([]);
    const [states, setStates] = useState([]);
    const [cities, setCities] = useState([]);
    const [state, setState] = useState({});

    const { images, videos } = useSelector(state => state.publicar);

    // useEffect(
    //     _ => {
    //         console.log('images', images);
    //     },
    //     [images]
    // );

    // useEffect(
    //     _ => {
    //         console.log('videos', videos);
    //     },
    //     [videos]
    // );

    useEffect(_ => {
        setCountries(csc.getAllCountries());
        setStates(csc.getStatesOfCountry('1'));
        setCities(csc.getCitiesOfState('42'));

        setState({
            ...state,
            pais: csc.getAllCountries()[0].name,
            estado: csc.getStatesOfCountry('1')[0].name,
            ciudad: csc.getCitiesOfState('42')[0].name,
            continente: 'Norteamérica',
            zona: 'Zone 1',
            vivienda: '',
            tipo: '',
            comodidades: '',
            servicios: '',
            tipo_precio: '',
            minimo: 0,
            maximo: 0,
            resultados: '',
            detalles: '',
            cercanias: '',
            ubicacion: '',
            precio: '',
            moneda: 'USD',
            otra_moneda: '',
            nombre: '',
            apellido: '',
            email: '',
            movil: '',
            fijo: '',
            dias_contacto: '',
            horas_contacto: ''
        });
        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(
        _ => {
            if (countries[0]) {
                setState({
                    ...state,
                    pais: countries[0].name
                });
            }
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [countries]
    );

    const handleSubmit = async e => {
        const data = {
            rooms,
            baths,
            parks
        };

        if (checkForm(data) !== false) {
            let tipo_precio =
                state.tipo_precio === 'rango'
                    ? { rango: [state.minimo, state.maximo] }
                    : 'cualquier precio';

            const body = {
                ...state,
                cantidad: JSON.stringify(data),
                tipo_precio: JSON.stringify(tipo_precio)
            };

            let bodyData = new FormData();

            for (let key in body) {
                bodyData.append(key, body[key]);
            }

            for (let id in images) {
                let name = 'image_' + id;
                bodyData.append(name, images[id]);
            }

            for (let id in videos) {
                let nameV = 'video_' + id;
                bodyData.append(nameV, videos[id]);
            }

            // console.log('request', bodyData);
            toast.info('Guardando vivienda ...');
            axios
                .post(url, bodyData)
                .then(res => {
                    toast.success('Vivienda guardada!');

                    console.log(res.data);

                    setTimeout(() => {
                        history.push('/publicar/ver');
                    }, 3000);
                })
                .catch(err => {
                    toast.error('Error al guardar vivienda');

                    if (err.response) {
                        console.log(err.response.data);
                    } else {
                        console.log(err.response);
                    }
                });
        }
    };

    const handleCountry = e => {
        setStates(csc.getStatesOfCountry(e.target.value));
        setCities([]);

        setState({
            ...state,
            pais: csc.getCountryById(e.target.value).name
        });
    };

    const handleState = e => {
        setCities(csc.getCitiesOfState(e.target.value));

        setState({
            ...state,
            estado: csc.getStateById(e.target.value).name
        });
    };

    const handleCity = (e, text) => {
        if (text) {
            setState({
                ...state,
                ciudad: e.target.value
            });
        } else {
            setState({
                ...state,
                ciudad: csc.getCityById(e.target.value).name
            });
        }
    };

    const checkForm = form => {
        if (form.rooms < 1) {
            alert('Error: debe incluir al menos una habitacion.');
            return false;
        }
        if (form.baths < 1) {
            alert('Error: debe incluir al menos un baño.');
            return false;
        }
        if (state.vivienda === '') {
            alert('Error: debe elegir Vivienda en');
            return false;
        }
        if (state.tipo === '') {
            alert('Error: debe elegir el tipo de inmueble');
            return false;
        }
        if (state.resultados === '') {
            alert('Error: debe elegir el tipo de listado');
            return false;
        }
        if (state.precio === '') {
            alert('Error: debe ingresar precio');
            return false;
        }
        if (state.moneda === '') {
            alert('Error: debe ingresar moneda');
            return false;
        }
        if (state.nombre === '') {
            alert('Error: debe ingresar nombre');
            return false;
        }
        if (state.apellido === '') {
            alert('Error: debe ingresar apellido');
            return false;
        }
        let re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

        if (state.email === '' || !re.test(String(state.email).toLowerCase())) {
            alert('Error: debe ingresar un email valido');
            return false;
        }
        // if (state.movil === '') {
        //     alert('Error: debe ingresar movil');
        //     return false;
        // }
        // if (state.fijo === '') {
        //     alert('Error: debe ingresar fijo');
        //     return false;
        //}
        if (state.dias_contacto === '') {
            alert('Error: debe ingresar dias de contacto');
            return false;
        }
        if (state.horas_contacto === '') {
            alert('Error: debe ingresar horas de contacto');
            return false;
        }
        if (
            state.comodidades === '' ||
            state.comodidades === 'null' ||
            state.comodidades === '[]'
        ) {
            alert('Error: debe elegir comodidades');
            return false;
        }
        if (
            state.servicios === '' ||
            state.servicios === 'null' ||
            state.servicios === '[]'
        ) {
            alert('Error: debe elegir servicios');
            return false;
        }
        if (
            state.typo_precio === 'rango' &&
            (Number(state.maximo) < 0 || Number(state.minimo) < 0)
        ) {
            alert('Error: debe elegir un rango valido');
            return false;
        }
        // if (form.parks < 1) {
        //     alert('Error: debe incluir la menos un estacionamiento.');
        //     return false;
        // }
        return true;
    };

    const handleContinent = e =>
        setState({ ...state, continente: e.target.value });

    const handleZone = e => setState({ ...state, zona: e.target.value });

    const handleVivienda = e => setState({ ...state, vivienda: e });
    const handleTipo = e => setState({ ...state, tipo: e });

    const handleComodidades = e => {
        let comodidades = JSON.stringify(e);
        setState({ ...state, comodidades });
    };

    const handleServicios = e => {
        let servicios = JSON.stringify(e);
        setState({ ...state, servicios });
    };

    const handleMin = e =>
        setState({ ...state, minimo: e.target.value, tipo_precio: 'rango' });

    const handleMax = e =>
        setState({ ...state, maximo: e.target.value, tipo_precio: 'rango' });

    const handleResultados = e => setState({ ...state, resultados: e });
    const handleDetalles = e =>
        setState({ ...state, detalles: e.target.value });
    const handleCercanias = e =>
        setState({ ...state, cercanias: e.target.value });
    const handleUbicacion = e =>
        setState({ ...state, ubicacion: e.target.value });

    const handlePrecio = e => setState({ ...state, precio: e.target.value });

    const handleMoneda = e => setState({ ...state, moneda: e.target.value });

    const handleOtraMoneda = e =>
        setState({ ...state, otra_moneda: e.target.value });

    const handleNombre = e => setState({ ...state, nombre: e.target.value });
    const handleApellido = e =>
        setState({ ...state, apellido: e.target.value });
    const handleEmail = e => setState({ ...state, email: e.target.value });
    const handleMovil = e => setState({ ...state, movil: e });
    const handleFijo = e => setState({ ...state, fijo: e });

    const handleDiasContacto = e =>
        setState({ ...state, dias_contacto: JSON.stringify(e) });
    const handleHorasContacto = e =>
        setState({ ...state, horas_contacto: JSON.stringify(e) });

    return (
        <Container>
            <Row className="text-center justify-content-center">
                <Col className="publicar-header" md="8">
                    <h4>Publicar Vivienda</h4>
                </Col>
            </Row>
            <Row>
                <Col className="text-center mb-2">
                    <h4>Proporcione los datos especificados a continuación</h4>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Card className="outlineA mb-5">
                        <Card.Body>
                            <Row>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Continente
                                    </Button>
                                    <Form.Group
                                        className="mt-2"
                                        controlId="exampleForm.ControlSelect1"
                                    >
                                        <Form.Control
                                            as="select"
                                            onChange={handleContinent}
                                        >
                                            <option>Norteamérica</option>
                                            <option>Suramérica</option>
                                            <option>Europa</option>
                                            <option>Asia</option>
                                            <option>Oceanía</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        País
                                    </Button>
                                    <Form.Group
                                        className="mt-2"
                                        controlId="exampleForm.ControlSelect2"
                                    >
                                        <Form.Control
                                            as="select"
                                            onChange={handleCountry}
                                        >
                                            {countries &&
                                                countries.map((p, i) => (
                                                    <option
                                                        key={i}
                                                        value={p.id}
                                                    >
                                                        {p.name}
                                                    </option>
                                                ))}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Estado
                                    </Button>
                                    <Form.Group
                                        className="mt-2"
                                        controlId="exampleForm.ControlSelect1"
                                    >
                                        <Form.Control
                                            as="select"
                                            onChange={handleState}
                                        >
                                            {states &&
                                                states.map((p, i) => (
                                                    <option
                                                        key={i}
                                                        value={p.id}
                                                    >
                                                        {p.name}
                                                    </option>
                                                ))}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Ciudad
                                    </Button>
                                    <Form.Group
                                        className="mt-2"
                                        controlId="exampleForm.ControlSelect2"
                                    >
                                        {cities.length === 0 ? (
                                            <input
                                                type="text"
                                                className="form-control"
                                                maxLength={20}
                                                onChange={e =>
                                                    handleCity(e, true)
                                                }
                                            />
                                        ) : (
                                            <Form.Control
                                                as="select"
                                                onChange={handleCity}
                                            >
                                                {cities &&
                                                    cities.map((p, i) => (
                                                        <option
                                                            key={i}
                                                            value={p.id}
                                                        >
                                                            {p.name}
                                                        </option>
                                                    ))}
                                            </Form.Control>
                                        )}
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Zona
                                    </Button>
                                    <Form.Group
                                        className="mt-2"
                                        controlId="exampleForm.ControlSelect2"
                                    >
                                        <Form.Control
                                            as="select"
                                            onChange={handleZone}
                                        >
                                            <option value="Zona 1">
                                                Zona 1
                                            </option>
                                            <option value="Zona 2">
                                                Zona 2
                                            </option>
                                            <option value="Zona 3">
                                                Zona 3
                                            </option>
                                            <option value="Zona 4">
                                                Zona 4
                                            </option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row className="mt-5">
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Vivienda en
                                    </Button>
                                    <fieldset className="mt-2">
                                        <Form.Group as={Row}>
                                            <Col sm={10}>
                                                <Form.Check
                                                    type="radio"
                                                    label="Alquiler"
                                                    name="vivienda_en"
                                                    id="formHorizontalRadios1"
                                                    onClick={_ =>
                                                        handleVivienda(
                                                            'Alquiler'
                                                        )
                                                    }
                                                />
                                                <Form.Check
                                                    type="radio"
                                                    label="Venta"
                                                    name="vivienda_en"
                                                    id="formHorizontalRadios2"
                                                    onClick={_ =>
                                                        handleVivienda('Venta')
                                                    }
                                                />
                                                <Form.Check
                                                    type="radio"
                                                    label="Alquiler y venta"
                                                    name="vivienda_en"
                                                    id="formHorizontalRadios3"
                                                    onClick={_ =>
                                                        handleVivienda(
                                                            'Alquiler y venta'
                                                        )
                                                    }
                                                />
                                            </Col>
                                        </Form.Group>
                                    </fieldset>
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Tipo de inmueble
                                    </Button>
                                    <fieldset className="mt-2">
                                        <Form.Group as={Row}>
                                            <Col sm={10}>
                                                <Form.Check
                                                    type="radio"
                                                    label="Apartamento"
                                                    name="inmueble"
                                                    id="forminmueble1"
                                                    onClick={_ =>
                                                        handleTipo(
                                                            'Apartamento'
                                                        )
                                                    }
                                                />
                                                <Form.Check
                                                    type="radio"
                                                    label="Casa o quinta"
                                                    name="inmueble"
                                                    id="forminmueble2"
                                                    onClick={_ =>
                                                        handleTipo(
                                                            'Casa o quinta'
                                                        )
                                                    }
                                                />
                                                <Form.Check
                                                    type="radio"
                                                    label="Apartamento y casa"
                                                    name="inmueble"
                                                    id="forminmueble3"
                                                    onClick={_ =>
                                                        handleTipo(
                                                            'Apartamento y casa'
                                                        )
                                                    }
                                                />
                                            </Col>
                                        </Form.Group>
                                    </fieldset>
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Cantidad
                                    </Button>
                                    <Form.Group as={Row} controlId="formRooms">
                                        <Form.Label
                                            column
                                            sm="7"
                                            className="labelUpDown"
                                        >
                                            Habitaciones:
                                        </Form.Label>
                                        <Col sm="1">
                                            <Button
                                                variant="light"
                                                onClick={_ =>
                                                    setRooms(rooms - 1)
                                                }
                                                size="sm"
                                            >
                                                -
                                            </Button>
                                        </Col>
                                        <Col sm="1" className="upDown">
                                            <Form.Control
                                                className="text-center"
                                                plaintext
                                                readOnly
                                                value={rooms}
                                            />
                                        </Col>
                                        <Col sm="1" className="upDown">
                                            <Button
                                                variant="light"
                                                onClick={_ =>
                                                    setRooms(rooms + 1)
                                                }
                                                size="sm"
                                            >
                                                +
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group
                                        as={Row}
                                        controlId="formBathrooms"
                                    >
                                        <Form.Label
                                            column
                                            sm="7"
                                            className="labelUpDown"
                                        >
                                            Baños:
                                        </Form.Label>
                                        <Col sm="1">
                                            <Button
                                                variant="light"
                                                onClick={_ =>
                                                    setBaths(baths - 1)
                                                }
                                                size="sm"
                                            >
                                                -
                                            </Button>
                                        </Col>
                                        <Col sm="1" className="upDown">
                                            <Form.Control
                                                className="text-center"
                                                plaintext
                                                readOnly
                                                value={baths}
                                            />
                                        </Col>
                                        <Col sm="1" className="upDown">
                                            <Button
                                                variant="light"
                                                onClick={_ =>
                                                    setBaths(baths + 1)
                                                }
                                                size="sm"
                                            >
                                                +
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group
                                        as={Row}
                                        controlId="formParking"
                                    >
                                        <Form.Label
                                            column
                                            sm="8"
                                            className="labelUpDown"
                                        >
                                            Estacionamiento:
                                        </Form.Label>
                                        <Col sm="1">
                                            <Button
                                                variant="light"
                                                onClick={_ =>
                                                    setParks(parks - 1)
                                                }
                                                size="sm"
                                            >
                                                -
                                            </Button>
                                        </Col>
                                        <Col sm="1" className="upDown">
                                            <Form.Control
                                                className="text-center"
                                                plaintext
                                                readOnly
                                                value={parks}
                                            />
                                        </Col>
                                        <Col sm="1" className="upDown">
                                            <Button
                                                variant="light"
                                                onClick={_ =>
                                                    setParks(parks + 1)
                                                }
                                                size="sm"
                                            >
                                                +
                                            </Button>
                                        </Col>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Comodidades
                                    </Button>
                                    <Select
                                        placeholder="Seleccione comodidades"
                                        options={comodidades}
                                        isMulti
                                        onChange={handleComodidades}
                                    />
                                </Col>
                                <Col>
                                    <Button disabled variant="primary" block>
                                        Servicios
                                    </Button>
                                    <Select
                                        placeholder="Seleccione servicios"
                                        options={servicios}
                                        isMulti
                                        onChange={handleServicios}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-5">
                                <Col />
                                <Col>
                                    <Button
                                        variant="primary"
                                        className="text-light"
                                        block
                                        disabled
                                    >
                                        Precio
                                    </Button>
                                    <fieldset className="mt-2">
                                        <Form.Group as={Row}>
                                            <Col sm={10}>
                                                <Form.Check
                                                    type="radio"
                                                    label="Por rango"
                                                    name="precio"
                                                    id="formprecio1"
                                                    onChange={_ =>
                                                        setPrecioRangoBool(
                                                            !precioRangoBool
                                                        )
                                                    }
                                                />
                                                {precioRangoBool && (
                                                    <div>
                                                        <Form.Group
                                                            as={Row}
                                                            controlId="formRangoMin"
                                                        >
                                                            <Form.Label
                                                                column
                                                                sm="5"
                                                            >
                                                                Mínimo
                                                            </Form.Label>
                                                            <Col sm="7">
                                                                <Form.Control
                                                                    type="text"
                                                                    onChange={
                                                                        handleMin
                                                                    }
                                                                />
                                                            </Col>
                                                        </Form.Group>
                                                        <Form.Group
                                                            as={Row}
                                                            controlId="formRangoMax"
                                                        >
                                                            <Form.Label
                                                                column
                                                                sm="5"
                                                            >
                                                                Máximo
                                                            </Form.Label>
                                                            <Col sm="7">
                                                                <Form.Control
                                                                    type="text"
                                                                    onChange={
                                                                        handleMax
                                                                    }
                                                                />
                                                            </Col>
                                                        </Form.Group>
                                                    </div>
                                                )}
                                                <Form.Check
                                                    type="radio"
                                                    label="Cualquier precio"
                                                    name="precio"
                                                    id="formprecio2"
                                                    onClick={_ =>
                                                        setState({
                                                            ...state,
                                                            tipo_precio: ''
                                                        })
                                                    }
                                                    onChange={_ =>
                                                        setPrecioRangoBool(
                                                            !precioRangoBool
                                                        )
                                                    }
                                                />
                                            </Col>
                                        </Form.Group>
                                    </fieldset>
                                </Col>
                                <Col>
                                    <Button
                                        variant="primary"
                                        className="text-light"
                                        block
                                        disabled
                                    >
                                        Listar resultados de forma
                                    </Button>
                                    <fieldset className="mt-2">
                                        <Form.Group as={Row}>
                                            <Col sm={10}>
                                                <Form.Check
                                                    type="radio"
                                                    label="Ascendente"
                                                    name="list"
                                                    id="formlist1"
                                                    onClick={_ =>
                                                        handleResultados(
                                                            'Ascendente'
                                                        )
                                                    }
                                                />
                                                <Form.Check
                                                    type="radio"
                                                    label="Descendente"
                                                    name="list"
                                                    id="formlist2"
                                                    onClick={_ =>
                                                        handleResultados(
                                                            'Descendente'
                                                        )
                                                    }
                                                />
                                                <Form.Check
                                                    type="radio"
                                                    label="Más recomendados"
                                                    name="list"
                                                    id="formlist3"
                                                    onClick={_ =>
                                                        handleResultados(
                                                            'Más recomendados'
                                                        )
                                                    }
                                                />
                                            </Col>
                                        </Form.Group>
                                    </fieldset>
                                </Col>
                                <Col />
                            </Row>
                            <Row>
                                <Col>
                                    <FotosViviendas />
                                </Col>
                                <Col>
                                    <Videos />
                                </Col>
                            </Row>
                            <MasDetalles
                                handleDetalles={handleDetalles}
                                handleCercanias={handleCercanias}
                                handleUbicacion={handleUbicacion}
                                handlePrecio={handlePrecio}
                                handleMoneda={handleMoneda}
                                handleOtraMoneda={handleOtraMoneda}
                            />
                            <Contacto
                                handleNombre={handleNombre}
                                handleApellido={handleApellido}
                                handleEmail={handleEmail}
                                handleFijo={handleFijo}
                                handleMovil={handleMovil}
                                handleDiasContacto={handleDiasContacto}
                                handleHorasContacto={handleHorasContacto}
                            />
                            <Row className="text-center">
                                <Col>
                                    <p style={{ color: 'blue' }}>
                                        <span style={{ color: 'red' }}>*</span>{' '}
                                        Por favor verifique que los datos sean
                                        los correctos, ya que serán utilizados
                                        para dar a conocer su vivienda
                                    </p>
                                </Col>
                            </Row>
                            <Row className="mt-5">
                                <Col />
                                <Col>
                                    <Button
                                        variant="warning"
                                        className="text-light"
                                        block
                                        onClick={_ => handleSubmit()}
                                    >
                                        Publicar Vivienda
                                    </Button>
                                </Col>
                                <Col>
                                    <Button
                                        variant="warning"
                                        className="text-light"
                                        block
                                    >
                                        Cancelar
                                    </Button>
                                </Col>
                                <Col />
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
