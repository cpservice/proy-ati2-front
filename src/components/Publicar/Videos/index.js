import React, { useState, Fragment, useEffect } from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import VideoInput from './VideoInput';

export default function FotosViviendas() {
    const [video, setVideo] = useState(false);
    const [videosCant, setVideosCant] = useState(2);
    const [videosInputs, setVideosInputs] = useState(null);

    const updateInputs = _ => {
        let newVideosInputs = [];
        for (let i = 0; i < videosCant; i++) {
            newVideosInputs.push(
                <Col key={i}>
                    <VideoInput id={i} />
                </Col>
            );
        }
        setVideosInputs(newVideosInputs);
    };

    useEffect(
        _ => {
            updateInputs();
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [videosCant]
    );

    const handleChange = e => {
        if (e.target.value === 'si' && e.target.checked) setVideo(true);
        else setVideo(false);
    };

    const handleSelect = e => {
        setVideosCant(Number(e.target.value));
    };
    return (
        <div className="text-center">
            <div className="fotos-viviendas-title">¿Desea agregar videos?</div>
            <Card className="text-center">
                <Card.Body>
                    <Row className="mb-2">
                        <Col>
                            <Form.Check
                                type="radio"
                                name="video"
                                label="Si"
                                value="si"
                                onChange={handleChange}
                            />
                        </Col>
                        <Col>
                            <Form.Check
                                type="radio"
                                name="video"
                                label="No"
                                value="no"
                                onChange={handleChange}
                            />
                        </Col>
                    </Row>
                    {video ? (
                        <Fragment>
                            <Row>
                                <Col>
                                    <Button disabled>
                                        {' '}
                                        Cantidad de videos
                                    </Button>
                                </Col>
                                <Col>
                                    <Form.Control
                                        as="select"
                                        onChange={handleSelect}
                                    >
                                        <option value="2">Hasta 2</option>
                                        <option value="5">Hasta 5</option>
                                    </Form.Control>
                                </Col>
                            </Row>
                            <Row className="mb-2">
                                <Col>
                                    Arrastre los videos que desea cargar, en
                                    cada uno de los recuadros
                                </Col>
                            </Row>
                            <Row>{videosInputs}</Row>
                        </Fragment>
                    ) : null}
                </Card.Body>
            </Card>
        </div>
    );
}
