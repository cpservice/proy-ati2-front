import React, { useEffect, useState, useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { useSelector, useDispatch } from 'react-redux';

const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
    // marginTop: 16
};

const thumb = {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    // marginBottom: 8,
    // marginRight: 8,
    width: 150,
    height: 100,
    padding: 4,
    boxSizing: 'border-box'
};

const thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
};

export default function VideoInput({ id }) {
    const [files, setFiles] = useState([]);
    const dispatch = useDispatch();
    const { videos } = useSelector(state => state.publicar);

    const setVideos = useCallback(
        data => dispatch({ type: 'SetVideos', payload: data }),
        [dispatch]
    );

    useEffect(
        () => () => {
            // Make sure to revoke the data uris to avoid memory leaks
            files.forEach(file => URL.revokeObjectURL(file.preview));
        },
        [files]
    );

    const { getRootProps, getInputProps } = useDropzone({
        accept: 'video/*',
        onDrop: acceptedFiles => {
            setVideos({ ...videos, [id]: acceptedFiles[0] });

            setFiles(
                acceptedFiles.map(file =>
                    Object.assign(file, {
                        preview: URL.createObjectURL(file)
                    })
                )
            );
        }
    });

    const thumbs = files.map(file => (
        <div style={thumb} key={file.name}>
            <div style={thumbInner}>
                <video controls>
                    <source src={file.preview} />
                    Your browser does not support HTML5 video.
                </video>
            </div>
        </div>
    ));

    return (
        <div className="image-input">
            {files.length === 0 ? (
                <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <p></p>
                </div>
            ) : null}

            <aside onClick={_ => setFiles([])} style={thumbsContainer}>
                {thumbs}
            </aside>
        </div>
    );
}
