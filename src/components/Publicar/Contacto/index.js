import React, { Fragment, useState, useEffect } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import CountriesCode from '../../Register/CountriesCode';

import './index.css';

export default function Contacto({
    handleNombre,
    handleApellido,
    handleEmail,
    handleFijo,
    handleMovil,
    handleDiasContacto,
    handleHorasContacto
}) {
    const [showMovil, setShowMovil] = useState(false);
    const [showFijo, setShowFijo] = useState(false);
    const [fijo, setFijo] = useState({ code: '', number: '', ext: '' });
    const [movil, setMovil] = useState({ code: '', number: '' });
    const [dias, setDias] = useState({});
    const [horas, setHoras] = useState({});

    useEffect(
        _ => {
            handleFijo(JSON.stringify(fijo));
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [fijo]
    );

    useEffect(
        _ => {
            handleMovil(JSON.stringify(movil));
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [movil]
    );

    useEffect(
        _ => {
            handleDiasContacto(dias);
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [dias]
    );

    useEffect(
        _ => {
            handleHorasContacto(horas);
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [horas]
    );

    const handleTel = e => {
        switch (e.target.id) {
            case 'movil':
                setShowMovil(e.target.checked);
                break;
            case 'fijo':
                setShowFijo(e.target.checked);
                break;
            default:
                break;
        }
    };

    const handleCountryCodeF = e => {
        setFijo({
            ...fijo,
            code: e.value
        });
    };

    const handleCountryCodeM = e => {
        setMovil({
            ...movil,
            code: e.value
        });
    };

    const handleChangePh = e => {
        switch (e.target.id) {
            case 'movil':
                setMovil({
                    ...movil,
                    number: e.target.value
                });
                break;
            case 'fijo':
                setFijo({
                    ...fijo,
                    number: e.target.value
                });
                break;
            case 'ext':
                setFijo({
                    ...fijo,
                    ext: e.target.value
                });
                break;
            default:
                break;
        }
    };

    const handleDias = e => {
        setDias({
            ...dias,
            [e.target.id]: e.target.checked
        });
    };

    const handleDesdeTime = e => {
        setHoras({
            ...horas,
            desde: e.target.value
        });
    };
    const handleDesdeTimeA = e => {
        setHoras({
            ...horas,
            desde_a: e.target.value
        });
    };
    const handleHastaTime = e => {
        setHoras({
            ...horas,
            hasta: e.target.value
        });
    };
    const handleHastaTimeA = e => {
        setHoras({
            ...horas,
            hasta_a: e.target.value
        });
    };

    return (
        <Fragment>
            <Row className="text-center mb-5 mt-3">
                <Col>
                    <div className="fotos-viviendas-title">
                        Datos de contacto
                    </div>
                    <Row className="datos-contacto-container">
                        <Col>
                            <Row>
                                <Col className="padding-contact text-left">
                                    <p>
                                        <span style={{ color: 'red' }}>*</span>{' '}
                                        Nombre
                                    </p>
                                    <p>
                                        <span style={{ color: 'red' }}>*</span>{' '}
                                        Apellido
                                    </p>
                                    <p>
                                        <span style={{ color: 'red' }}>*</span>{' '}
                                        Correo electrónico
                                    </p>
                                </Col>
                                <Col className="padding-contact">
                                    <input
                                        type="text"
                                        onChange={handleNombre}
                                    />
                                    <input
                                        type="text"
                                        onChange={handleApellido}
                                    />
                                    <input type="text" onChange={handleEmail} />
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col className="mb-2">
                                    <p
                                        style={{
                                            color: 'blue',
                                            fontWeight: 'normal'
                                        }}
                                    >
                                        Seleccione, el o los teléfonos de su
                                        preferencia
                                    </p>
                                </Col>
                            </Row>
                            <Row className="text-center">
                                <Col>
                                    <Form.Check
                                        type="checkbox"
                                        label=""
                                        onChange={handleTel}
                                        id="movil"
                                        style={{ display: 'inline' }}
                                    />
                                    <div
                                        className="fotos-viviendas-title"
                                        style={{
                                            display: 'inline',
                                            padding: '4px 30px'
                                        }}
                                    >
                                        Móvil
                                    </div>
                                </Col>
                                <Col>
                                    <Form.Check
                                        type="checkbox"
                                        label=""
                                        onChange={handleTel}
                                        id="fijo"
                                        style={{ display: 'inline' }}
                                    />
                                    <div
                                        className="fotos-viviendas-title"
                                        style={{
                                            display: 'inline',
                                            padding: '4px 30px'
                                        }}
                                    >
                                        Fijo
                                    </div>
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                {showMovil && (
                                    <>
                                        <Col md={3}>
                                            {/* <Form.Control as="select" /> */}
                                            <CountriesCode
                                                handleChange={
                                                    handleCountryCodeM
                                                }
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <Form.Control
                                                type="number"
                                                placeholder=""
                                                id="movil"
                                                onChange={handleChangePh}
                                                maxLength={20}
                                            />
                                        </Col>
                                    </>
                                )}
                            </Row>
                            <Row className="mt-2">
                                {showFijo && (
                                    <>
                                        <Col md={3}>
                                            {/* <Form.Control as="select" /> */}
                                            <CountriesCode
                                                handleChange={
                                                    handleCountryCodeF
                                                }
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <Form.Control
                                                type="number"
                                                placeholder=""
                                                id="fijo"
                                                onChange={handleChangePh}
                                                maxLength={20}
                                            />
                                        </Col>
                                        <Col
                                            md={1}
                                            style={{
                                                fontSize: '14px',
                                                padding: '0'
                                            }}
                                        >
                                            Ext
                                        </Col>
                                        <Col md={3}>
                                            {/* <Form.Label>Ext</Form.Label> */}
                                            <Form.Control
                                                type="number"
                                                id="ext"
                                                onChange={handleChangePh}
                                                maxLength={20}
                                            />
                                        </Col>
                                    </>
                                )}
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <Row>
                        <Col>
                            <Row>
                                <div className="fotos-viviendas-title">
                                    Días de contacto
                                </div>
                            </Row>
                            <Row className="dias-contacto-container mb-3">
                                <Col>
                                    <Row className="mb-3">
                                        <Form.Check
                                            type="checkbox"
                                            label="Lunes"
                                            style={{ display: 'inline' }}
                                            id="Lunes"
                                            onChange={handleDias}
                                        />
                                        <Form.Check
                                            type="checkbox"
                                            label="Martes"
                                            style={{ display: 'inline' }}
                                            onChange={handleDias}
                                            id="Martes"
                                        />
                                        <Form.Check
                                            type="checkbox"
                                            label="Miércoles"
                                            style={{ display: 'inline' }}
                                            onChange={handleDias}
                                            id="Miércoles"
                                        />
                                        <Form.Check
                                            type="checkbox"
                                            label="Jueves"
                                            style={{ display: 'inline' }}
                                            onChange={handleDias}
                                            id="Jueves"
                                        />
                                    </Row>
                                    <Row>
                                        <Form.Check
                                            type="checkbox"
                                            label="Viernes"
                                            style={{ display: 'inline' }}
                                            onChange={handleDias}
                                            id="Viernes"
                                        />
                                        <Form.Check
                                            type="checkbox"
                                            label="Sábado"
                                            style={{ display: 'inline' }}
                                            onChange={handleDias}
                                            id="Sábado"
                                        />
                                        <Form.Check
                                            type="checkbox"
                                            label="Domingo"
                                            style={{ display: 'inline' }}
                                            onChange={handleDias}
                                            id="Domingo"
                                        />
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Row>
                                <div className="fotos-viviendas-title">
                                    Horas de contacto
                                </div>
                            </Row>
                            <Row>
                                <Col>
                                    <b>Desde</b>
                                    <Form.Control
                                        as="select"
                                        onChange={handleDesdeTime}
                                    >
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </Form.Control>
                                </Col>
                                <Col className="pt-4">
                                    <Form.Control
                                        as="select"
                                        onChange={handleDesdeTimeA}
                                    >
                                        <option value="am">am</option>
                                        <option value="pm">pm</option>
                                    </Form.Control>
                                </Col>
                                <Col>
                                    <b>Hasta</b>
                                    <Form.Control
                                        as="select"
                                        onChange={handleHastaTime}
                                    >
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </Form.Control>
                                </Col>
                                <Col
                                    className="pt-4"
                                    onChange={handleHastaTimeA}
                                >
                                    <Form.Control as="select">
                                        <option value="am">am</option>
                                        <option value="pm">pm</option>
                                    </Form.Control>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Fragment>
    );
}
