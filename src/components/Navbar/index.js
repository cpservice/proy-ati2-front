import React, { useCallback, Fragment, useEffect, useState } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { toast } from 'react-toastify';
import './index.css';

const NavBar = _ => {
    const history = useHistory();
    const dispatch = useDispatch();

    const { logged } = useSelector(state => state.settings);
    const [lang, setLang] = useState(false);
    const [viv, setViv] = useState(false);
    const [serv, setServ] = useState(false);
    const [empl, setEmpl] = useState(false);

    useEffect(() => {
        const token = localStorage.getItem('access_token');

        if (token && token !== '') {
            setLogged(true);
        }
        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const setIniciarSesion = useCallback(
        data => dispatch({ type: 'SetIniciarSesion', payload: data }),
        [dispatch]
    );

    const setLogged = useCallback(
        data => dispatch({ type: 'SetLogged', payload: data }),
        [dispatch]
    );

    const handleUser = _ => {
        const opts = document.querySelector('.user-login-options');
        opts.classList.toggle('active');
        // console.log(opts);
    };

    const handleLogout = _ => {
        const url = process.env.REACT_APP_URL + '/api/auth/logout';

        const token = localStorage.getItem('access_token');

        axios
            .get(url, {
                headers: { Authorization: 'Bearer ' + token }
            })
            .then(res => {
                if (res.status === 200) {
                    localStorage.removeItem('access_token');
                    setLogged(false);
                    history.push('/');
                }
            })
            .catch(err => {
                console.log(err.response);
                toast.warn('Error al cerrar sesión');
            });
    };

    const handlePublicarClick = _ => {
        setViv(false);
        history.push('/publicar');
    };

    const handleServiciosClick = _ => {
        setServ(!serv);
        history.push('/services');
    }

    const handleEmpleoClick = _ => {
        setEmpl(!empl);
        history.push('/empleo');
    }

    return (
        <section>
            <div className="signin-signup">
                {logged ? (
                    <Fragment>
                        <button onClick={handleUser}>Usuario</button>
                        <div className="user-login-options">
                            <div
                                className="opt"
                                onClick={_ => {
                                    history.push('/register/supo_nosotros');
                                    handleUser();
                                }}
                            >
                                Datos de usuario
                            </div>
                            <div className="opt" onClick={handleLogout}>
                                Cerrar sesión
                            </div>
                        </div>
                    </Fragment>
                ) : (
                    <Fragment>
                        <button onClick={_ => setIniciarSesion(true)}>
                            Iniciar Sesión
                        </button>
                        <button
                            onClick={_ =>
                                history.push('/register/supo_nosotros')
                            }
                        >
                            Registrarse
                        </button>
                    </Fragment>
                )}
            </div>
            <Navbar bg="" variant="">
                <Nav className="mr-auto">
                    <Nav.Link as="div">
                        <span
                            className="nav-item"
                            onClick={_ => history.push('/')}
                        >
                            Inicio
                        </span>
                    </Nav.Link>
                    <Nav.Link as="div">
                        <span className="nav-item" onClick={_ => setViv(!viv)}>
                            Viviendas
                        </span>
                        {viv && (
                            <div className="submenu-vi">
                                <ul>
                                    <li onClick={_ => handlePublicarClick()}>
                                        Publicar
                                    </li>
                                    <li onClick={_ => history.push('/publicar/ver')}>
                                        Ver Publicaciones
                                    </li>
                                    <li>Buscar</li>
                                    <li>Modificar</li>
                                    <li>Eliminar</li>
                                    <li>Habilitar</li>
                                    <li>Deshabilitar</li>
                                </ul>
                            </div>
                        )}
                    </Nav.Link>
                    <Nav.Link as="div">
                    <span className="nav-item" onClick={_ => handleServiciosClick()}>
                            Servicios
                        </span>
                        {serv && (
                            <div className="submenu-vi">
                                <ul>
                                    <li onClick={_ => history.push('/services/create')}>
                                        Crear
                                    </li>
                                    <li>
                                        Buscar
                                    </li>
                                    <li onClick={_ => history.push('/services/see')}>
                                        Consultar
                                    </li>
                                    <li>
                                        Modificar
                                    </li>
                                    <li>
                                        Eliminar
                                    </li>
                                </ul>
                            </div>
                        )}
                    </Nav.Link>
                    <Nav.Link as="div">
                    <span className="nav-item" onClick={_ => handleEmpleoClick()}>
                            Empleo
                        </span>
                        {empl && (
                            <div className="submenu-vi">
                                <ul>
                                    <li onClick={_ => history.push('/crear')}>
                                        Crear
                                    </li>
                                    <li>
                                        Buscar
                                    </li>
                                    <li>
                                        Consultar
                                    </li>
                                    <li>
                                        Modificar
                                    </li>
                                    <li>
                                        Eliminar
                                    </li>
                                </ul>
                            </div>
                        )}
                    </Nav.Link>
                    <Nav.Link as="div">
                        <span className="nav-item" onClick={_ => history.push('/ayuda')}>Ayuda</span>
                    </Nav.Link>
                    <Nav.Link as="div">
                        <span 
                            className="nav-item"
                            onClick={_ => history.push('/contact')}
                        >
                            Contactenos
                        </span>
                    </Nav.Link>
                    <Nav.Link as="div">
                        <span className="nav-item" onClick={_ => history.push('/conocenos')}>Conócenos más</span>
                    </Nav.Link>
                    <Nav.Link as="div">
                        <span
                            className="nav-item"
                            onClick={_ => setLang(!lang)}
                        >
                            Idioma
                        </span>
                        {lang && (
                            <div className="submenu-lang">
                                <ul>
                                    <li>Español</li>
                                    <li>English</li>
                                </ul>
                            </div>
                        )}
                    </Nav.Link>
                </Nav>
                {/* <Nav>
                    <Nav.Link href="#conocenos">Inicio sesion</Nav.Link>
                    <Nav.Link as="div">
                        <Link to="/register/supo_nosotros">Registrarse</Link>
                    </Nav.Link>
                </Nav> */}
            </Navbar>
        </section>
    );
};

export default NavBar;
