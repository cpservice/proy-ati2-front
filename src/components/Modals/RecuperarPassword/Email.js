import React, { useState, useEffect, useCallback } from 'react';
import { Modal, Button, Form, Col, Row, Container } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';

export default function Email() {
	const dispatch = useDispatch();

	const [ show, setShow ] = useState(false);
	// const [ email, setEmail ] = useState('');

	const { email } = useSelector((state) => state.modals);

	const setIniciarSesion = useCallback((data) => dispatch({ type: 'SetIniciarSesion', payload: data }), [ dispatch ]);
	const setMessage = useCallback((data) => dispatch({ type: 'SetMessage', payload: data }), [ dispatch ]);

	const handleClose = () => {
		setShow(false);
		setIniciarSesion(false);
	};
	// const handleShow = () => setShow(true);

	useEffect(
		(_) => {
			setShow(email);
		},
		[ email ]
	);

	const handleSubmit = (e) => {
		e.preventDefault();

		handleClose();
		setMessage(true);
		// setLogged(true);
	};

	return (
		<div>
			<Modal show={show} onHide={handleClose} centered>
				<Modal.Header closeButton>
					<Modal.Title>Recuperar contraseña, o mis datos</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Container className="text-center">
						<Form onSubmit={handleSubmit}>
							<Row>
								<Col>Ingresa tu correo electrónico, o usuario</Col>
							</Row>
							<Row className="mt-2">
								<Col>
									<Form.Group controlId="formBasicEmail">
										{/* <Form.Label>correo</Form.Label> */}
										<Form.Control
											type="email"
											// onChange={(e) => setEmail(e.target.value)}
											required
											// value={email}
										/>
									</Form.Group>
								</Col>
							</Row>
							<Row className="mt-3">
								<Col>
									<Button type="submit" className="btn-yellow">
										Aceptar
									</Button>
								</Col>

								<Col>
									<Button onClick={handleClose} className="btn-yellow">
										Cancelar
									</Button>
								</Col>
							</Row>
						</Form>
					</Container>
				</Modal.Body>
			</Modal>
		</div>
	);
}
