import React, { useState, useEffect, useCallback } from "react";
import { Modal, Button, Form, Col, Row, Container } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";

export default function Mensaje() {
    const dispatch = useDispatch();

    const [show, setShow] = useState(false);
    const [email, setEmail] = useState("");
    const [tel, setTel] = useState("");

    const { message } = useSelector(state => state.modals);

    const setMessage = useCallback(
        data => dispatch({ type: "SetMessage", payload: data }),
        [dispatch]
    );

    const handleClose = () => {
        setShow(false);
        setMessage(false);
    };

    useEffect(() => {
        setEmail("correo@correo.com");
        setTel("0414-389-12-34");
    }, []);

    useEffect(
        _ => {
            setShow(message);
        },
        [message]
    );

    const handleSubmit = e => {
        e.preventDefault();
    };

    return (
        <div>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Recuperar contraseña, o mis datos</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container className="text-center">
                        <Form onSubmit={handleSubmit}>
                            <Row>
                                <Col className="text-justify">
                                    <p>
                                        Acabamos de enviar tu usuario, y un link
                                        para restablecer tu contraseña, al
                                        correo: {email}{" "}
                                        <span className="text-danger">
                                            {" "}
                                            y al número {tel}
                                        </span>
                                    </p>
                                    <p>
                                        Si este no es su correo{" "}
                                        <span className="text-danger">
                                            o teléfono,
                                        </span>{" "}
                                        debe modificar su correo electrónico,{" "}
                                        <span className="text-danger">
                                            o teléfono,
                                        </span>{" "}
                                        en la cuenta que posee con la empresa, y
                                        solicitar nuevamente el envío de dicha
                                        información.
                                    </p>
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col>
                                    <Button
                                        onClick={handleClose}
                                        className="btn-yellow"
                                    >
                                        Aceptar
                                    </Button>
                                </Col>

                                <Col>
                                    <Button
                                        onClick={handleClose}
                                        className="btn-yellow"
                                    >
                                        Cancelar
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Container>
                </Modal.Body>
            </Modal>
        </div>
    );
}
