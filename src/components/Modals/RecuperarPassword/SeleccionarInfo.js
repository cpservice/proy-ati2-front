import React, { useState, useEffect, useCallback } from 'react';
import { Modal, Button, Form, Col, Row, Container } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

export default function SeleccionarInfo() {
	const dispatch = useDispatch();

	const [ show, setShow ] = useState(false);
	const [ opt, setOpt ] = useState('');

	const { seleccionarInfo } = useSelector((state) => state.modals);

	const setCedula = useCallback((data) => dispatch({ type: 'SetCedula', payload: data }), [ dispatch ]);
	const setTelefono = useCallback((data) => dispatch({ type: 'SetTelefono', payload: data }), [ dispatch ]);
	const setEmail = useCallback((data) => dispatch({ type: 'SetEmail', payload: data }), [ dispatch ]);
	const setSeleccionarInfo = useCallback((data) => dispatch({ type: 'SetSeleccionarInfo', payload: data }), [
		dispatch
	]);

	const handleClose = () => {
		setShow(false);
	};

	useEffect(
		(_) => {
			setShow(seleccionarInfo);
		},
		//eslint-disable-next-line react-hooks/exhaustive-deps
		[ seleccionarInfo ]
	);

	useEffect(
		(_) => {
			setSeleccionarInfo(show);
		},
		//eslint-disable-next-line react-hooks/exhaustive-deps
		[ show ]
	);

	const handleSubmit = (e) => {
		e.preventDefault();
		handleClose();

		switch (opt) {
			case 'dni':
				setCedula(true);
				break;
			case 'email':
				setEmail(true);
				break;
			case 'tel':
				setTelefono(true);
				break;
			default:
				break;
		}
	};

	return (
		<div>
			<Modal show={show} onHide={handleClose} centered>
				<Modal.Header closeButton>
					<Modal.Title>Recuperar contraseña, o mis datos</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Container className="text-center">
						<Form onSubmit={handleSubmit}>
							<Row>
								<Col>Seleccione la información que va a proporcionar</Col>
							</Row>
							<Row className="mt-4">
								<Col>
									<Row>
										<Col md="2">
											<Form.Check
												id="dni"
												type="radio"
												name="forma"
												onChange={(_) => setOpt('dni')}
											/>
										</Col>
										<Col className="text-left">
											<Form.Label htmlFor="dni">Cédula de identidad, DNI o pasaporte</Form.Label>
										</Col>
									</Row>

									<Row>
										<Col md="2">
											<Form.Check
												id="email"
												type="radio"
												name="forma"
												onChange={(_) => setOpt('email')}
											/>
										</Col>
										<Col className="text-left">
											<Form.Label htmlFor="email">Correo electrónico o usuario</Form.Label>
										</Col>
									</Row>

									<Row>
										<Col md="2">
											<Form.Check
												id="tel"
												type="radio"
												name="forma"
												onChange={(_) => setOpt('tel')}
											/>
										</Col>
										<Col className="text-left">
											<Form.Label htmlFor="tel">Teléfono celular o móvil</Form.Label>
										</Col>
									</Row>

									<Row className="mt-3">
										<Col>
											<Button type="submit" className="btn-yellow">
												Aceptar
											</Button>
										</Col>

										<Col>
											<Button onClick={handleClose} className="btn-yellow">
												Cancelar
											</Button>
										</Col>
									</Row>
								</Col>
							</Row>
						</Form>
					</Container>
				</Modal.Body>
			</Modal>
		</div>
	);
}
