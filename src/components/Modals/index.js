import React from 'react';
import IniciarSesion from './IniciarSesion.js';
import SeleccionarInfo from './RecuperarPassword/SeleccionarInfo.js';
import Cedula from './RecuperarPassword/Cedula';
import Email from './RecuperarPassword/Email';
import Telefono from './RecuperarPassword/Telefono';
import Message from './RecuperarPassword/Message';
import './index.css';

export default function Modals() {
	return (
		<div>
			<IniciarSesion />
			<SeleccionarInfo />
			<Cedula />
			<Email />
			<Telefono />
			<Message />
		</div>
	);
}
