import React, { useState, useEffect, useCallback } from 'react';
import { Modal, Button, Form, Col, Row, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { toast } from 'react-toastify';

export default function IniciarSesion() {
	const dispatch = useDispatch();

	const [ show, setShow ] = useState(false);
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const { iniciarSesion } = useSelector((state) => state.modals);

	const setIniciarSesion = useCallback((data) => dispatch({ type: 'SetIniciarSesion', payload: data }), [ dispatch ]);

	const setLogged = useCallback((data) => dispatch({ type: 'SetLogged', payload: data }), [ dispatch ]);

	const setSeleccionarInfo = useCallback((data) => dispatch({ type: 'SetSeleccionarInfo', payload: data }), [
		dispatch
	]);

	const handleClose = () => {
		setShow(false);
		setIniciarSesion(false);
	};

	useEffect(
		(_) => {
			setShow(iniciarSesion);
		},
		[ iniciarSesion ]
	);

	const handleSubmit = (e) => {
		e.preventDefault();

		const url = process.env.REACT_APP_URL + '/api/auth/login';
		axios
			.post(url, {
				email,
				password
			})
			.then((res) => {
				if (res.data.access_token) {
					localStorage.setItem('access_token', res.data.access_token);
					setLogged(true);
					handleClose();
					setEmail('');
					setPassword('');
				}
			})
			.catch((err) => {
				console.log('login', err.response);
				if (err.response.status === 401) {
					toast.warn('Usuario o contraseña incorrecta');
				}
			});
	};

	const handleForgot = (_) => {
		handleClose();
		setSeleccionarInfo(true);
	};

	return (
		<div>
			<Modal show={show} onHide={handleClose} centered>
				<Modal.Header closeButton>
					<Modal.Title>Iniciar Sesión</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Container className="text-center">
						<Form onSubmit={handleSubmit}>
							<Row>
								<Col>Selecciona la cuenta en la que desea acceder</Col>
							</Row>
							<Row className="mt-2">
								<Col>
									<Form.Group controlId="formBasicEmail">
										<Form.Label>correo</Form.Label>
										<Form.Control
											type="email"
											onChange={(e) => setEmail(e.target.value)}
											required
											value={email}
										/>
									</Form.Group>

									<Form.Group controlId="formBasicPassword">
										<Form.Label>contraseña</Form.Label>
										<Form.Control
											type="password"
											onChange={(e) => setPassword(e.target.value)}
											required
											value={password}
										/>
									</Form.Group>
									<Button type="submit" className="btn-yellow">
										Iniciar Sesión
									</Button>
								</Col>
							</Row>
							<Row className="mt-2">
								<Col>
									<Link to="/" onClick={handleForgot}>
										Olvidé mi contraseña, o mis datos
									</Link>
								</Col>
							</Row>
						</Form>
					</Container>
				</Modal.Body>
			</Modal>
		</div>
	);
}
