import React, { Component } from 'react';
import { Row, Col, Button } from "react-bootstrap";
import How from './How';

export default class Sale extends Component {
    constructor (props) {
        super (props)

        this.state = {

        }
    }

    render () {
        return (
            <>
                <How />
                <Row className="my-3">
                    <Col
                        xs={12}
                    >
                        <h5 className="text-danger">
                            Preguntas frecuentes
                        </h5>
                    </Col>
                    <Col
                        xs={12}
                    >
                        <p>Haga clic aquí: <a variant="link" href='/ayuda'
                                target='_blank'>
                                Ver preguntas frecuentes
                            </a>
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col
                        xs={12}                    
                    >
                        <h4 className="text-danger">
                            Propuesta de publicaciones
                        </h4>
                    </Col>
                    <Col
                        xs={12}
                    >
                        <p>
                            Conoce nuestra propuesta de publicaciones, junto con los precios y condiciones del servicio, haciendo clic <Button variant="link">
                                aquí:
                            </Button>
                        </p>
                        
                    </Col>
                    <Col>
                        <Button
                            variant="link"
                        >
                            Ver propuesta de publicaciones
                        </Button>
                    </Col>
                </Row>
            </>
        )
    }
}