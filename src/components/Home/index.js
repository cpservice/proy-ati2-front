import React, { Component } from 'react';
import './index.css';
import { Container, Row, Col, Button } from "react-bootstrap";
import Search from './Search';
import Sale from './Sale';

export default class Home extends Component {
    constructor (props) {
        super(props);

        this.state = {
            searchBool: false,
            saleBool: false
        }
    }

    handlerSearch = () => {
        this.setState({searchBool: !this.state.searchBool, saleBool: false});
    }

    handlerSale = () => {
        this.setState({saleBool: !this.state.saleBool, searchBool: false})
    }

    render () {

        const searchBool = this.state.searchBool;
        const saleBool = this.state.saleBool;
        
        return <div>

            <Container>
                <Row>
                    <Col></Col>
                    <Col>
                        <span className="float-left">Síguenos o compártelo por:</span>
                    </Col>
                </Row>
                <Row className="mt-3">
                    <Col>
                        <div className="mx-auto">
                            <h3 className="text-center">Opciones de búsqueda</h3>
                        </div>  
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p className="text-primary"><span className="text-danger">*</span>1- Haz clic en la opción de tu preferencia.</p>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Button 
                            className="float-right search_button text-light"
                            variant="warning"
                            onClick={this.handlerSearch}
                        >
                            Buscar vivienda
                        </Button>
                    </Col>
                    <Col>
                        <Button
                            className="float-left search_button text-light"
                            variant="warning"
                            onClick={this.handlerSale}
                        >
                            Vender o alquilar vivienda
                        </Button>
                    </Col>
                </Row>
                <Row>
                    {searchBool && <Search />}
                    {saleBool && <Sale />}
                </Row>
            </Container>

        </div>

    }
}