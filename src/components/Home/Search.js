import React, { Component } from 'react';
import { Container, Row, Col, Accordion } from 'react-bootstrap';
import How from './How';
import SearchClave from './Search/Clave.js';
import Flash from './Search/Flash.js';
import Detallada from './Search/Detallada.js';

export default class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    handleSearchKey = data => {
        if (data !== false) {
            console.log(data, 'Busqueda por palabra clave');
        }
    };

    handleSearchFlash = data => {
        if (data !== false) {
            console.log(data, 'Busqueda rapida');
        }
    };

    handleSearchDetailed = data => {
        if (data !== false) {
            console.log(data, 'Busqueda detallada');
        }
    };

    render() {
        return (
            <div className="mx-auto mt-5">
                <Container>
                    <Row>
                        <Col>
                            <p className="text-primary">
                                <span className="text-danger">*</span>1- Haz
                                clic en la opción de tu preferencia.
                            </p>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} className="mt-3">
                            <Accordion>
                                <SearchClave />
                                <Flash />
                                <Detallada />
                            </Accordion>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <p className="text-primary">
                                <span className="text-danger">*</span>1- Haz
                                clic en la opción de tu preferencia.
                            </p>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} className="mt-3">
                            <Accordion>
                                <SearchClave
                                    handleSearch={this.handleSearchKey}
                                />
                                <Flash handleSearch={this.handleSearchFlash} />
                                <Detallada
                                    handleSearch={this.handleSearchDetailed}
                                />
                            </Accordion>
                        </Col>
                    </Row>

                    <Row className="mt-5" style={{ width: '90rem' }}>
                        <Col>
                            <h4>Preguntas frecuentes</h4>
                        </Col>
                        <Col>
                            <p>
                                Haga clic aquí:
                                <a
                                    className="ml-1"
                                    variant="link"
                                    href="/ayuda"
                                    target="_blank"
                                >
                                    Ver más preguntas frecuentes
                                </a>
                            </p>
                        </Col>
                    </Row>
                    <How />
                </Container>
            </div>
        );
    }
}
