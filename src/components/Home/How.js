import React, { useCallback } from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import { useDispatch } from 'react-redux';

const How = _ => {
    const dispatch = useDispatch();

    const setIniciarSesion = useCallback(
        data => dispatch({ type: 'SetIniciarSesion', payload: data }),
        [dispatch]
    );

    return (
        <>
            <Row>
                <Col className="my-2" xs={12}>
                    <h5 className="text-danger">¿Cómo publico mi vivienda con ustedes?</h5>
                </Col>
                <Col className="my-1">
                    <p>1- Debes registrarte haciendo clic aquí: <Button href="./register" target="_blank" className="search_button">
                            Registrarme
                        </Button>
                    </p>
                    <p>2- Luego debes iniciar sesión, haciendo clic en: <Button onClick={_ => setIniciarSesion(true)} className="search_button">
                            Iniciar sesión
                        </Button>
                    </p>
                    <p>3- Luego de iniciar sesión seleccionas la opción <Button href="./publicar" target="_blank" variant="link">
                            Viviendas Publicar
                        </Button>
                    </p>
                    <p>4- Luego colocas los datos solicitados, y luego presiona el botón Publicar vivienda</p>
                    <p>5- Luego revisaremos la publicación, si todo esta bien, te notificaremos que ya tu vivienda está disponible para que otros usuarios la puedan visualizar, en un plazo de 3 días hábiles como máximo.</p>
                </Col>
            </Row>
        </> 
    )
}
export default How;
