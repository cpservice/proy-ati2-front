import React, { Component, Fragment } from 'react';
import { Row, Col, Button, Card, Accordion, Form } from "react-bootstrap";
import Select from 'react-select';
import csc from "country-state-city";


export default class Clave extends Component {
    constructor (props) {
        super (props)

        this.state = {
            continente: "",
            pais: "",
            estado: "",
            ciudad: "",
            countries: [],
            statesOfCountry: [],
            cities: [],
            zona: "",
            vivienda_state: "alquiler",
            tipo_vivienda: "",
            rooms: 1,
            bathroom: 1,
            parking: 0,
            comodidades: [],
            comodidadesArray: [{label:"Cocina", value:"Cocina"}, {label:"Nevera",value:"Nevera"}, {label:"Microondas", value:"Microondas"}, {label:"Lavadora", value:"Lavadora"}, {label:"Secadora", value:"Secadora"}, {label:"TV en sala", value:"TV en sala"}, {label:"TV en habitación", value:"TV en habitación"}, {label:"Comedor", value:"Comedor"}, {label:"Muebles", value:"Muebles"}, {label:"Cama matrimonial", value:"Cama matrimonial"}],
            serviciosArray: [{label:"Luz", value:"Luz"}, {label:"Agua",value:"Agua"}, {label:"Teléfono", value:"Teléfono"}, {label:"Internet residencial", value:"Internet residencial"}, {label:"Otro", value:"Otro"}],
            servicios: [],
            precioRangoBool: false,
            cualquierPrecio: true,
            precioMin: "",
            precioMax: "",
            resultados: "ascendente",
        }
    }

    componentDidMount () {
        this.setState({countries: csc.getAllCountries()});
        this.setState({statesOfCountry: csc.getStatesOfCountry("1")});
        this.setState({cities: csc.getCitiesOfState("42")})
    }

    handleChangeContinente = e => {
        const continente = e.target.value

        if(continente) {
            this.setState({
                continente
            })
        }
    }

    handleChangePais = e => {
        const id = e.target.value

        if(id){
            this.setState({
                statesOfCountry: csc.getStatesOfCountry(id),
                pais: csc.getCountryById(id).name
            })
        }
    }

    handleChangeEstado = e => {
        const id = e.target.value

        if(id) {
            this.setState({
                cities: csc.getCitiesOfState(id),
                estado: csc.getStateById(id).name
            })
        }
    }

    handleChangeCiudad = e => {
        const id = e.target.value

        if(id) {
            this.setState({
                ciudad: csc.getCityById(id).name
            })
        }
    }

    handleChangeZona = e => {
        const zona = e.target.value

        if(zona) {
            this.setState({
                zona
            })
        }
    }


    hangleChangeViviendaState = e => {
        const data = e.target.id

        this.setState({vivienda_state: data.toLowerCase()})
    }

    hangleChangeTipoVivienda = e => {
        const data = e.target.id

        this.setState({tipo_vivienda: data.toLowerCase()})
    }

    handleUp = (option) => {

        switch (option) {
            case "room":
                this.setState({rooms: this.state.rooms + 1});
                break;
            case "bath":
                this.setState({bathroom: this.state.bathroom + 1});
                break;
            case "park":
                this.setState({parking: this.state.parking + 1});
                break;
            default:
                break;
        }
    }

    handleDown = (option) => {

        switch (option) {
            case "room":
                if(this.state.rooms > 1) {
                    this.setState({rooms: this.state.rooms - 1});
                }
                break;
            case "bath":
                if(this.state.bathroom > 1) {
                    this.setState({bathroom: this.state.bathroom - 1});
                }
                break;
            case "park":
                if(this.state.parking > 0) {
                    this.setState({parking: this.state.parking - 1});
                }
                break;
            default:
                break;
        }
    }

    handleChangeComodidades = e => {
        const data = []

        if(e){
            e.forEach(element => [
                data.push(element.value)
            ])
        }

        this.setState({comodidades: data})

    }

    handleChangeServices = e => {
        const data = []

        if(e){
            e.forEach(element => [
                data.push(element.value)
            ])
        }

        this.setState({servicios: data})
    }

    handleChangeRango = _ => {

        if(!this.state.precioRangoBool) {
            this.setState({
                precioMax: "",
                precioMin: "",
            })
        }

        this.setState({
            precioRangoBool: !this.state.precioRangoBool,
            cualquierPrecio: !this.state.cualquierPrecio,
        });
    }

    handleChangePrecioMax = e => {
        const precio = e.target.value

        this.setState({
            precioMax: precio
        })
    }

    handleChangePrecioMin = e => {
        const precio = e.target.value

        this.setState({
            precioMin: precio
        })
    }

    handleChangeResult = e => {
        const data = e.target.id

        this.setState({resultados: data.toLowerCase()})
    }


    handleSubmit = _ => {
        const data = {
            continente: this.state.continente,
            pais: this.state.pais,
            estado: this.state.estado,
            ciudad: this.state.ciudad,
            zona: this.state.zona,
            vivienda_state: this.state.vivienda_state,
            tipo_vivienda: this.state.tipo_vivienda,
            rooms: this.state.rooms,
            bathroom: this.state.bathroom,
            parking: this.state.parking,
            comodidades: this.state.comodidades,
            servicios: this.state.servicios,
            cualquierPrecio: this.state.cualquierPrecio,
            precioMin: this.state.precioMin,
            precioMax: this.state.precioMax,
            resultados: this.state.resultados,
        }

        this.props.handleSearch(data)
    }


    handleCancel = _ => {
        this.setState({
            continente: "",
            pais: "",
            estado: "",
            ciudad: "",
            countries: [],
            statesOfCountry: [],
            cities: [],
            zona: "",
            vivienda_state: "alquiler",
            tipo_vivienda: "",
            rooms: 1,
            bathroom: 1,
            parking: 0,
            comodidades: [],
            comodidadesArray: [{label:"Cocina", value:"Cocina"}, {label:"Nevera",value:"Nevera"}, {label:"Microondas", value:"Microondas"}, {label:"Lavadora", value:"Lavadora"}, {label:"Secadora", value:"Secadora"}, {label:"TV en sala", value:"TV en sala"}, {label:"TV en habitación", value:"TV en habitación"}, {label:"Comedor", value:"Comedor"}, {label:"Muebles", value:"Muebles"}, {label:"Cama matrimonial", value:"Cama matrimonial"}],
            serviciosArray: [{label:"Luz", value:"Luz"}, {label:"Agua",value:"Agua"}, {label:"Teléfono", value:"Teléfono"}, {label:"Internet residencial", value:"Internet residencial"}, {label:"Otro", value:"Otro"}],
            servicios: [],
            precioRangoBool: false,
            cualquierPrecio: true,
            precioMin: "",
            precioMax: "",
            resultados: "",
        })

        document.getElementById("detailed").click()
    }

    handleCollapse = _ => {
        this.props.handleSearch(false)
    }

    render () {
        return (
            <Fragment>
                <Card className="outlineA">
                    <Accordion.Toggle as={Card.Header} id="detailed" onClick={_ => this.handleCollapse()} eventKey="2">
                        Búsqueda detallada
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey="2">
                    <Card.Body>
                        <Row>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Continente
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                    <Form.Control as="select" onChange={e => this.handleChangeContinente(e)} required>
                                    <option value="">Seleccione el continente</option>
                                    <option>Norteamérica</option>
                                    <option>Suramérica</option>
                                    <option>Europa</option>
                                    <option>Asia</option>
                                    <option>Oceanía</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    País
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select" onChange={e => this.handleChangePais(e)} required>
                                    <option value="">Seleccione el país</option>
                                    {this.state.countries &&
                                        this.state.countries.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Estado
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                    <Form.Control as="select" onChange={e => this.handleChangeEstado(e)} required>
                                    <option value="">Seleccione el estado</option>
                                    {this.state.statesOfCountry &&
                                        this.state.statesOfCountry.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Ciudad
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select" onChange={e => this.handleChangeCiudad(e)}>
                                    <option>Seleccione la ciudad</option>
                                    {this.state.cities &&
                                        this.state.cities.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Zona
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select" onChange={e => this.handleChangeZona(e)} required>
                                    <option value="">Seleccione la zona</option>
                                    <option>Norte</option>
                                    <option>Sur</option>
                                    <option>Este</option>
                                    <option>Oeste</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className="mt-5">
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Vivienda en
                                </Button>
                                <fieldset className="mt-2">
                                    <Form.Group as={Row}>
                                        <Col sm={10}>
                                            <Form.Check
                                                type="radio"
                                                label="Alquiler"
                                                name="vivienda_en"
                                                id="alquiler"
                                                defaultChecked="checked"
                                                onClick={e => this.hangleChangeViviendaState(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Venta"
                                                name="vivienda_en"
                                                id="venta"
                                                onClick={e => this.hangleChangeViviendaState(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Alquiler y venta"
                                                name="vivienda_en"
                                                id="alquiler-venta"
                                                onClick={e => this.hangleChangeViviendaState(e)}
                                            />
                                        </Col>
                                    </Form.Group>
                                </fieldset>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Tipo de inmueble
                                </Button>
                                <fieldset className="mt-2">
                                    <Form.Group as={Row}>
                                        <Col sm={10}>
                                            <Form.Check
                                                type="radio"
                                                label="Apartamento"
                                                name="inmueble"
                                                id="apartamento"
                                                onClick={e => this.hangleChangeTipoVivienda(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Casa o quinta"
                                                name="inmueble"
                                                id="casa-quinta"
                                                onClick={e => this.hangleChangeTipoVivienda(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Apartamento y casa"
                                                name="inmueble"
                                                id="apartamento-casa"
                                                onClick={e => this.hangleChangeTipoVivienda(e)}
                                            />
                                        </Col>
                                    </Form.Group>
                                </fieldset>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Cantidad
                                </Button>
                                <Form.Group as={Row} controlId="formRooms">
                                    <Form.Label column sm="7" className="labelUpDown">
                                        Habitaciones: 
                                    </Form.Label>
                                    <Col sm="1">
                                        <Button variant="light" onClick={this.handleDown.bind(this,"room")} size="sm">-</Button>
                                    </Col>
                                    <Col sm="1" className="upDown">
                                    <Form.Control className="text-center" plaintext readOnly value={this.state.rooms}/>
                                    </Col>
                                    <Col sm="1" className="upDown">
                                        <Button variant="light" onClick={this.handleUp.bind(this,"room")} size="sm">+</Button>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} controlId="formBathrooms">
                                    <Form.Label column sm="7" className="labelUpDown">
                                        Baños: 
                                    </Form.Label>
                                    <Col sm="1">
                                        <Button variant="light" onClick={this.handleDown.bind(this,"bath")} size="sm">-</Button>
                                    </Col>
                                    <Col sm="1" className="upDown">
                                    <Form.Control className="text-center" plaintext readOnly value={this.state.bathroom}/>
                                    </Col>
                                    <Col sm="1" className="upDown">
                                        <Button variant="light" onClick={this.handleUp.bind(this,"bath")} size="sm">+</Button>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} controlId="formParking">
                                    <Form.Label column sm="8" className="labelUpDown">
                                        Estacionamiento:
                                    </Form.Label>
                                    <Col sm="1">
                                        <Button variant="light" onClick={this.handleDown.bind(this,"park")} size="sm">-</Button>
                                    </Col>
                                    <Col sm="1" className="upDown">
                                    <Form.Control className="text-center" plaintext readOnly value={this.state.parking}/>
                                    </Col>
                                    <Col sm="1" className="upDown">
                                        <Button variant="light" onClick={this.handleUp.bind(this,"park")} size="sm">+</Button>
                                    </Col>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Comodidades
                                </Button>
                                <Select
                                    placeholder="Seleccione comodidades"
                                    options={this.state.comodidadesArray}
                                    onChange={this.handleChangeComodidades}
                                    isMulti
                                />
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Servicios
                                </Button>
                                <Select
                                    placeholder="Seleccione servicios"
                                    options={this.state.serviciosArray}
                                    onChange={this.handleChangeServices}
                                    isMulti
                                />
                            </Col>
                        </Row>
                        <Row className="mt-5">
                            <Col/>
                            <Col>
                                <Button
                                    variant="primary"
                                    className="text-light"
                                    block
                                    disabled
                                >
                                    Precio
                                </Button>
                                <fieldset className="mt-2">
                                    <Form.Group as={Row}>
                                        <Col sm={10}>
                                            <Form.Check
                                            type="radio"
                                            label="Por rango"
                                            name="precio"
                                            id="formprecio1"
                                            onChange={this.handleChangeRango}
                                            />
                                            {this.state.precioRangoBool &&
                                                <div>
                                                    <Form.Group as={Row} controlId="formRangoMin">
                                                        <Form.Label column sm="5">
                                                            Mínimo
                                                        </Form.Label>
                                                        <Col sm="7">
                                                            <Form.Control type="text" onChange={e => this.handleChangePrecioMax(e)}/>
                                                        </Col>
                                                    </Form.Group>
                                                    <Form.Group as={Row} controlId="formRangoMax">
                                                        <Form.Label column sm="5">
                                                            Máximo
                                                        </Form.Label>
                                                        <Col sm="7">
                                                            <Form.Control type="text" onChange={e => this.handleChangePrecioMin(e)}/>
                                                        </Col>
                                                    </Form.Group>
                                                </div>
                                            }
                                            <Form.Check
                                                type="radio"
                                                label="Cualquier precio"
                                                name="precio"
                                                id="formprecio2"
                                                defaultChecked="checked"
                                                onChange={this.handleChangeRango}
                                            />
                                        </Col>
                                    </Form.Group>
                                </fieldset>
                            </Col>
                            <Col>
                                <Button
                                    variant="primary"
                                    className="text-light"
                                    block
                                    disabled
                                >
                                    Listar resultados de forma
                                </Button>
                                <fieldset className="mt-2">
                                    <Form.Group as={Row}>
                                        <Col sm={10}>
                                            <Form.Check
                                                type="radio"
                                                label="Ascendente"
                                                name="list"
                                                id="ascendente"
                                                defaultChecked="checked"
                                                onClick={e => this.handleChangeResult(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Descendente"
                                                name="list"
                                                id="descendente"
                                                onClick={e => this.handleChangeResult(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Más recomendados"
                                                name="list"
                                                id="recomendados"
                                                onClick={e => this.handleChangeResult(e)}
                                            />
                                        </Col>
                                    </Form.Group>
                                </fieldset>
                            </Col>
                            <Col/>
                        </Row>
                        <Row className="mt-5">
                            <Col/>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    onClick={_ => this.handleSubmit()}
                                    block
                                >
                                    Buscar
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    onClick={_ => this.handleCancel()}
                                    block
                                >
                                    Cancelar
                                </Button>
                            </Col>
                            <Col/>
                        </Row>
                    </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Fragment>
        )
    }

}