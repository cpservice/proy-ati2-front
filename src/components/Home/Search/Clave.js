import React, { Component, Fragment } from 'react';
import { Button, Card, Accordion, InputGroup, FormControl } from "react-bootstrap";
//import { Link } from 'react-router-dom';


export default class Clave extends Component {
    constructor (props) {
        super (props)

        this.state = {
            search: "",
        }
    }

    handleCollapse = _ => {
        this.props.handleSearch(false)
    }


    handleChangeSearch = e => {
        this.setState({search: e.target.value})
    }

    handleSubmit = _ => {
        this.props.handleSearch(this.state.search)
    }


    render () {

        return (
            <Fragment>
                <Card className="outlineA">
                    <Accordion.Toggle as={Card.Header} onClick={_ => this.handleCollapse()} eventKey="0">
                        Búsqueda por palabra clave
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body>
                        <InputGroup className="mb-3">
                            <FormControl
                                type="text"
                                placeholder="Introduzca país, estado o ciudad"
                                className="outlineA"
                                aria-label="Introduce el país,estado o ciudad"
                                onChange={e => this.handleChangeSearch(e)}
                            />
                            <InputGroup.Append>
                                <Button
                                    variant="primary"
                                    onClick={_ => this.handleSubmit()}
                                >
                                    Buscar
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Fragment>
        )
    }

}