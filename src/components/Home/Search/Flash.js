import React, { Component, Fragment } from 'react';
import { Row, Col, Button, Card, Accordion, Form } from "react-bootstrap";
import csc from "country-state-city";


export default class Clave extends Component {
    constructor (props) {
        super (props)

        this.state = {
            pais: "",
            countries: [],
            statesOfCountry: [],
            estado: "",
            vivienda_state: "alquiler",
            tipo_vivienda: "",
        }
    }

    componentDidMount () {
        this.setState({countries: csc.getAllCountries()});
        this.setState({statesOfCountry: csc.getStatesOfCountry("1")});
    }


    handleChangePais = e => {
        const id = e.target.value

        if(id){
            this.setState({
                statesOfCountry: csc.getStatesOfCountry(id),
                pais: csc.getCountryById(id).name
            })
        }
    }

    handleChangeEstado = e => {
        const id = e.target.value

        if(id) {
            this.setState({
                estado: csc.getStateById(id).name
            })
        }
    }


    hangleChangeViviendaState = e => {
        const data = e.target.id

        this.setState({vivienda_state: data.toLowerCase()})
    }


    handleChangeTipoVivienda = e => {
        const data = e.target.value

        this.setState({tipo_vivienda: data.toLowerCase()})
    }

    handleSubmit = _ => {
        const data = {
            pais: this.state.pais,
            estado: this.state.estado,
            vivienda_state: this.state.vivienda_state,
            tipo_vivienda: this.state.tipo_vivienda,
        }

        this.props.handleSearch(data)
    }


    handleCancel = _ => {
        this.setState({
            pais: "",
            estado: "",
            vivienda_state: "",
            tipo_vivienda: "",
        })

        document.getElementById("flash").click()
    }


    handleCollapse = _ => {
        this.props.handleSearch(false)
    }

    render () {
        return (
            <Fragment>
                <Card className="outlineA">
                    <Accordion.Toggle as={Card.Header} id="flash" onClick={_ => this.handleCollapse()} eventKey="1">
                        Búsqueda rápida
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey="1">
                    <Card.Body>
                        <Row>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    País
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                    <Form.Control as="select" onChange={e => this.handleChangePais(e)} required>
                                    <option value="">Seleccione el país</option>
                                    {this.state.countries &&
                                        this.state.countries.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Estado
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select" onChange={e => this.handleChangeEstado(e)} required>
                                    <option value="">Seleccione el estado</option>
                                    {this.state.statesOfCountry &&
                                        this.state.statesOfCountry.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Vivienda en
                                </Button>
                                <fieldset className="mt-2">
                                    <Form.Group as={Row}>
                                        <Col sm={10}>
                                            <Form.Check
                                                type="radio"
                                                label="Alquiler"
                                                name="vivienda_en"
                                                id="alquile"
                                                defaultChecked="checked"
                                                onClick={e => this.hangleChangeViviendaState(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Venta"
                                                name="vivienda_en"
                                                id="venta"
                                                onClick={e => this.hangleChangeViviendaState(e)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Alquiler y venta"
                                                name="vivienda_en"
                                                id="alquiler-venta"
                                                onClick={e => this.hangleChangeViviendaState(e)}
                                            />
                                        </Col>
                                    </Form.Group>
                                </fieldset>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Tipo de inmueble
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select" onChange={e => this.handleChangeTipoVivienda(e)} required>
                                    <option value="">Seleccione el tipo de inmueble</option>
                                    <option>Apartamento</option>
                                    <option>Casa</option>
                                    <option>Apartamento y Casa</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className="mt-5">
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light float-right"
                                    onClick={_ => this.handleSubmit()}
                                >
                                    Buscar
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    onClick={_ => this.handleCancel()}
                                >
                                    Cancelar
                                </Button>
                            </Col>
                        </Row>
                    </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Fragment>
        )
    }

}