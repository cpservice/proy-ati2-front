import React from "react";
import { Button, Col, Row } from "react-bootstrap";
import { useHistory } from "react-router-dom";

const centerStyle = {
    textAlign: "center",
    backgroundColor: "#00B1EB",
    color: "#fff",
    borderRadius: "10px",
    paddingTop: "7px"
};

const RegisterHeader = ({ title, step, next, handleNext }) => {
    const history = useHistory();

    const onNext = _ => {

        if(handleNext){
            handleNext()
        }else{
            history.push("/register/" + next)
        }
    } 

    return (
        <Row>
            <Col md="3" style={{ textAlign: "center" }}>
                {step !== "0" ? (
                    <Button
                        className="btn-danger font-weight-bold"
                        onClick={history.goBack}
                    >
                        {"<"} Atras
                    </Button>
                ) : null}
            </Col>
            <Col md="6" style={centerStyle}>
                <h5 className="font-weight-bold">{title}</h5>
            </Col>
            <Col md="3" style={{ textAlign: "center" }}>
                {step !== "5" ? (
                    <Button
                        className="btn-danger font-weight-bold"
                        onClick={onNext}
                    >
                        Continuar >
                    </Button>
                ) : null}
            </Col>
        </Row>
    );
};

export default RegisterHeader;
