import React, { Component, Fragment } from 'react';
import Service from '../index';

export default class See extends Component {
    constructor (props) {
        super (props)

        this.state = {
            see: true,
        }
    }


    render () {
        return <Fragment>
                <Service see={this.state.see}/>
            </Fragment>
    }
}