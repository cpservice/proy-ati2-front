import React, { Component, Fragment } from 'react';
import { Row, Col, Button, Form } from "react-bootstrap";
import CountriesCode from '../../../Register/CountriesCode';


export default class Empresa extends Component {
    constructor (props) {
        super(props)

        this.state = {
            name: this.props.name,
            email: this.props.email,
            name_empresa: this.props.name_empresa,
            rif: this.props.rif,
            tel: {
                movil: false,
                fijo: false,
            },
        }
    }


    handleChangeTel = e => {
        switch (e.target.id) {
            case "check-movil":
                this.setState({tel: {movil: !this.state.tel.movil, fijo: this.state.tel.fijo}});
                break;
            case "check-fijo":
                this.setState({tel: {movil: this.state.tel.movil, fijo: !this.state.tel.fijo}});
                break;
            default:
                break;
        }
    }

    render () {
        return <Fragment>
            <Row>
                <Col xs={6}>
                    <Row>
                        <Col xs={12} className="mb-2">
                            <Button
                                variant="primary"
                                disabled
                                className="text-light d-flex mx-auto"
                            >
                                Datos de la empresa
                            </Button>
                        </Col>
                        <Col xs={12}>
                            <Form>
                                <Form.Group as={Row} controlId="formPlaintextPassword">
                                    <Form.Label column sm="5">
                                        <span className="text-danger">*</span> Nombre de la empresa
                                    </Form.Label>
                                    <Col sm="7">
                                        <Form.Control type="text" defaultValue={this.state.name_empresa} onChange={this.props.handleChangeName} placeholder="Nombre" />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} controlId="formPlaintextPassword">
                                    <Form.Label column sm="5">
                                        <span className="text-danger">*</span> Razón social/RIF
                                    </Form.Label>
                                    <Col sm="7">
                                        <Form.Control type="text" defaultValue={this.state.rif} onChange={this.props.handleChangeRif} placeholder="RIF" />
                                    </Col>
                                </Form.Group>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} className="mb-2">
                            <Button
                                variant="primary"
                                disabled
                                className="text-light d-flex mx-auto"
                            >
                                Datos del representante de la empresa
                            </Button>
                        </Col>
                        <Col xs={12}>
                            <Form>
                                <Form.Group as={Row} controlId="formPlaintextPassword">
                                    <Form.Label column sm="4">
                                        <span className="text-danger">*</span> Nombre y apellido
                                    </Form.Label>
                                    <Col sm="8">
                                        <Form.Control type="text" defaultValue={this.state.name} onChange={this.props.handleChangeRepresentante} placeholder="Nombre" />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} controlId="formPlaintextPassword">
                                    <Form.Label column sm="4">
                                        <span className="text-danger">*</span> Correo electrónico
                                    </Form.Label>
                                    <Col sm="8">
                                        <Form.Control type="email" defaultValue={this.state.email} onChange={this.props.handleChangeEmail} placeholder="correo@ejemplo.com" />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} controlId="formPlaintextPassword">
                                    <Form.Label column sm="4">
                                        <span className="text-danger">*</span> Teléfono
                                    </Form.Label>
                                </Form.Group>
                            </Form>
                            <Row>
                                <Col xs={12}>
                                    <h6 className="text-primary font-weight-light mx-auto">Seleccione, el o los teléfonos de su preferencia</h6>
                                </Col>
                                <Col xs={12}>
                                    <Form.Check inline name="tel" onChange={this.handleChangeTel.bind(this)} label="Móvil" type="checkbox" id="check-movil" />
                                    <Form.Check inline name="tel" onChange={this.handleChangeTel.bind(this)} label="Fijo" type="checkbox" id="check-fijo" />
                                </Col>
                                {this.state.tel.movil && <Col xs={12}>
                                    <Row>
                                        <Col md={3}>
                                            <CountriesCode
                                                handleChange={
                                                    this.props.handleCountryCodeM
                                                }
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <Form.Control
                                                type="number"
                                                placeholder=""
                                                id="movil"
                                                onChange={this.props.handleChangePh}
                                                maxLength={20}
                                            />
                                        </Col>
                                    </Row>
                                </Col>}
                                {this.state.tel.fijo && <Col>
                                    <Row>
                                        <Col md={3}>
                                            <CountriesCode
                                                handleChange={
                                                    this.props.handleCountryCodeF
                                                }
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <Form.Control
                                                type="number"
                                                placeholder=""
                                                id="fijo"
                                                onChange={this.props.handleChangePh}
                                                maxLength={20}
                                            />
                                        </Col>
                                        <Col md={1}>Ext</Col>
                                        <Col md={3}>
                                            <Form.Control
                                                type="number"
                                                id="ext"
                                                onChange={this.props.handleChangePh}
                                                maxLength={20}
                                            />
                                        </Col>
                                    </Row>
                                </Col>}
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col className="mx-3">
                    <Row>
                    <Col xs={12} className="mb-2">
                            <Button
                                variant="warning"
                                disabled
                                className="text-light"
                            >
                                Días de contacto
                            </Button>
                        </Col>
                        <Col xs={12} className="contact-service">
                            <Row>
                                <Form.Check inline label="Lunes" type="checkbox" id="lunes" />
                                <Form.Check inline label="Martes" type="checkbox" id="martes" />
                                <Form.Check inline label="Miércoles" type="checkbox" id="miercoles" />
                                <Form.Check inline label="Jueves" type="checkbox" id="jueves" />
                            </Row>
                            <Row>
                                <Form.Check inline label="Viernes" type="checkbox" id="viernes" />
                                <Form.Check inline label="Sábado" type="checkbox" id="sabado" />
                                <Form.Check inline label="Domingo" type="checkbox" id="domingo" />
                            </Row>
                        </Col>
                        <Col xs={12} className="my-2">
                            <Button
                                variant="warning"
                                disabled
                                className="text-light"
                            >
                                Horas de contacto
                            </Button>
                        </Col>
                        <Col xs={12}>
                            <Row>
                                <Col>
                                    <Form.Label>Desde</Form.Label>
                                    <Form.Control as="select" value="">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                    </Form.Control>
                                </Col>
                                <Col>
                                    <Form.Label> </Form.Label>
                                    <Form.Control className="mt-2" as="select" value="">
                                        <option>am</option>
                                        <option>pm</option>
                                    </Form.Control>
                                </Col>
                                <Col>
                                    <Form.Label>Hasta</Form.Label>
                                    <Form.Control as="select" value="">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                    </Form.Control>
                                </Col>
                                <Col>
                                    <Form.Label> </Form.Label>
                                    <Form.Control className="mt-2" as="select" value="">
                                        <option>am</option>
                                        <option>pm</option>
                                    </Form.Control>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col className="my-5 d-flex justify-content-center" xs={12}>
                    <Row>
                        <Col xs={12} className="d-flex justify-content-center mb-2">
                            <Button
                                variant="warning"
                                className="text-light"
                                disabled
                            >
                                ¿En qué lugar desea ofrecer el servicio?
                            </Button>
                        </Col>
                        <Col>
                            <Row>
                                <Col>
                                    <Button
                                        disabled
                                        variant="primary"
                                        block
                                    >
                                        Continente
                                    </Button>
                                    <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                        <Form.Control as="select">
                                        <option>Seleccione el continente</option>
                                        <option>Norteamérica</option>
                                        <option>Suramérica</option>
                                        <option>Europa</option>
                                        <option>Asia</option>
                                        <option>Oceanía</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button
                                        disabled
                                        variant="primary"
                                        block
                                    >
                                        País
                                    </Button>
                                    <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                        <Form.Control as="select">
                                        <option>Seleccione el país</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button
                                        disabled
                                        variant="primary"
                                        block
                                    >
                                        Estado
                                    </Button>
                                    <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                        <Form.Control as="select">
                                        <option>Seleccione el estado</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button
                                        disabled
                                        variant="primary"
                                        block
                                    >
                                        Ciudad
                                    </Button>
                                    <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                        <Form.Control as="select">
                                        <option>Seleccione la ciudad</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col xs={12}>
                    <Row>
                        <Col>
                        <h6 className="text-primary text-center"><span className="text-danger">*</span> Por favor verifique que los datos sean los correctos, ya que serán utilizados para dar a conocer su servicio</h6>
                        </Col>
                    </Row>
                    <Row className="d-flex justify-content-center">
                        <Col>
                            <Button
                                variant="warning"
                                className="text-light float-right"
                            >
                                Crear
                            </Button>
                        </Col>
                        <Col>
                            <Button
                                variant="warning"
                                className="text-light"
                            >
                                Cancelar
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Fragment>
    }

}