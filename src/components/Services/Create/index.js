import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Form } from "react-bootstrap";
import Natural from './Natural';
import Empresa from './Empresa';
import "./index.css";


export default class Create extends Component {
    constructor (props) {
        super (props)

        this.state = {
            type: {
                natural: false,
                empresa: false,
            },
            movil_natural: {code: '', number: ''},
            fijo_natural: {code: '', number: ''},
            name_natural: "Carlos",
            lastName_natural: "Pino",
            email_natural: "cpservice98@gmail.com",
            movil_empresa: {code: '', number: ''},
            fijo_empresa: {code: '', number: ''},
            name_empresa: "CPservice",
            rif_empresa: "j-2324324",
            representante_name_empresa: "Carlos Pino",
            representante_email_empresa: "cpservice98@gmail.com",
        }
    }

    handleChangeType = e => {

        switch (e.target.id) {
            case "radio-type1":
                this.setState({type: {natural: true, empresa: false}});
                break;
            case "radio-type2":
                this.setState({type: {natural: false, empresa: true}});
                break;
            default:
                break;
        }

    }

    //Empresa

    handleChangeNameEmpresa = name => {
        this.setState({name_empresa: name})
    }

    handleChangeNameRepresentante = name => {
        this.setState({representante_name_empresa: name})
    }

    handleChangeRifEmpresa = rif => {
        this.setState({rif_empresa: rif})
    }

    handleChangeEmailEmpresa = email => {
        this.setState({email_empresa: email})
    }

    handleCountryCodeFEmpresa = e => {
        this.setState({
            fijo_empresa: {code: e.value, number: this.state.fijo_empresa.number, ext: this.state.fijo_empresa.ext}
        });
    };

    handleCountryCodeMEmpresa = e => {
        this.setState({
            movil_empresa: {code: e.value, number: this.state.movil_empresa.number}
        });
    };

    handleChangePhEmpresa = e => {
        switch (e.target.id) {
            case 'movil':
                this.setState({
                    movil_empresa: {code: this.state.movil_empresa.code, number: e.target.value}
                });
                break;
            case 'fijo':
                this.setState({
                    fijo_empresa: {code: this.state.fijo_empresa.code, number: e.target.value, ext: this.state.fijo_empresa.ext}
                });
                break;
            case 'ext':
                this.setState({
                    fijo_empresa: {code: this.state.fijo_empresa.code, number: this.state.fijo_empresa.number, ext: e.target.value}
                });
                break;
            default:
                break;
        }
    };


    //Natural

    handleChangeNameNatural = name => {
        this.setState({name_natural: name})
    }

    handleChangeLastNameNatural = lastName => {
        this.setState({lastName_natural: lastName})
    }

    handleChangeEmailNatural = email => {
        this.setState({email_natural: email})
    }

    handleCountryCodeF = e => {
        this.setState({
            fijo_natural: {code: e.value, number: this.state.fijo_natural.number, ext: this.state.fijo_natural.ext}
        });
    };

    handleCountryCodeM = e => {
        this.setState({
            movil_natural: {code: e.value, number: this.state.movil_natural.number}
        });
    };

    handleChangePh = e => {
        switch (e.target.id) {
            case 'movil':
                this.setState({
                    movil_natural: {code: this.state.movil_natural.code, number: e.target.value}
                });
                break;
            case 'fijo':
                this.setState({
                    fijo_natural: {code: this.state.fijo_natural.code, number: e.target.value, ext: this.state.fijo_natural.ext}
                });
                break;
            case 'ext':
                this.setState({
                    fijo_natural: {code: this.state.fijo_natural.code, number: this.state.fijo_natural.number, ext: e.target.value}
                });
                break;
            default:
                break;
        }
    };

    render () {
        return <Fragment>
            <Container>
                <Row className="mt-5">
                    <Col className="title-create">
                        <h2 className="text-center font-weight-light">Crear servicios</h2>
                    </Col>
                </Row>

                <Row>
                    <Col className="text-center" xs={12}>
                        <p className="font-weight-bold">
                            Introduzca los datos del servicio que desea agregar, que se solicitan a continuación:
                        </p>
                    </Col>
                    <Col xs={12}>
                        <Form>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <Form.Label column sm="3" className="text-primary">
                                    Servicio a ofrecer
                                </Form.Label>
                                <Col sm="9">
                                    <Form.Control type="text" placeholder="Introduzca el servicio a ofrecer" />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col xs={12}>
                        <Form>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <Form.Label column sm="3" className="text-primary">
                                    Descripción del servicio
                                </Form.Label>
                                <Col sm="9">
                                    <Form.Control as="textarea" placeholder="Descripción" />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col xs={12}>
                        <Form>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <Form.Label column sm="3" className="text-primary">
                                    Tipo de usuario
                                </Form.Label>
                                <Col sm="9">
                                    <Form.Check inline name="user-type" onChange={this.handleChangeType.bind(this)} label="Persona natural" type="radio" id="radio-type1" />
                                    <Form.Check inline name="user-type" onChange={this.handleChangeType.bind(this)} label="Empresa" type="radio" id="radio-type2" />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
                {this.state.type.natural && <>
                    <Natural 
                        handleChangeTel={this.handleChangeTel}
                        handleCountryCodeF={this.handleCountryCodeF}
                        handleCountryCodeM={this.handleCountryCodeM}
                        handleChangePh={this.handleChangePh}
                        name={this.state.name_natural}
                        email={this.state.email_natural}
                        lastName={this.state.lastName_natural}
                        handleChangeName={this.state.handleChangeNameNatural}
                        handleChangeLastName={this.state.handleChangeLastNameNatural}
                        handleChangeEmail={this.state.handleChangeEmailNatural}
                    />
                </>}
                {this.state.type.empresa && <>
                    <Empresa 
                        handleChangeTel={this.handleChangeTelEmpresa}
                        handleCountryCodeF={this.handleCountryCodeFEmpresa}
                        handleCountryCodeM={this.handleCountryCodeMEmpresa}
                        handleChangePh={this.handleChangePhEmpresa}
                        name={this.state.representante_name_empresa}
                        email={this.state.representante_email_empresa}
                        name_empresa={this.state.name_empresa}
                        rif={this.state.rif_empresa}
                        handleChangeName={this.state.handleChangeNameEmpresa}
                        handleChangeRepresentante={this.state.handleChangeNameRepresentante}
                        handleChangeRif={this.state.handleChangeRifEmpresa}
                        handleChangeEmail={this.state.handleChangeEmailEmpresa}
                    />
                </>}
            </Container>
        </Fragment>
    }

}