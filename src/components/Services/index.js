import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Button, Form, Table } from "react-bootstrap";
import { Link } from 'react-router-dom';
import Select from 'react-select';
import iconEdit from '../../icons/write.png';
import iconDelete from '../../icons/delete.png';
import "./index.css"

export default class Services extends Component {
    constructor (props) {
        super(props)

        this.state = {
            servicios: [{label:"Avalúo de apartamentos", value:"Avalúo de apartamentos"}, {label:"Arreglar cocinas o pisos",value:"Arreglar cocinas o pisos"}],
            see: false,
        }
    }


    componentDidMount () {

        if(this.props.see){
            this.setState({see: this.props.see})
            
        }
    }


    render () {
        return <Fragment>
            <Container>
                <Row className="my-5">
                    <Col><h5 className="text-center">Podemos ayudarte con las siguientes actividades</h5></Col>
                </Row>
                <Row className="my-5">
                    <Col>
                        <h5 className="font-weight-light">Servicios disponibles</h5>
                        <ul>
                        <li>
                            <Button
                                variant="link"
                            >
                                Avalúo de apartamentos
                            </Button>
                        </li>
                        <li>
                            <Button
                                variant="link"
                            >
                                Arreglar cocinas o pisos
                            </Button>
                        </li>
                    </ul>
                    </Col>
                    <Col>
                        <h5 className="font-weight-light">Usuario o empresa oferente del servicio</h5>
                        <p>Puedes publicar tus servicios  como evaluador de apartamentos, ya sea que seas,
                         una empresa, o un usurio.</p>
                        <p>Puedes publicar tus servicios para arreglar cocinas o pisos, ya sea que seas, una empresa, o un usuario.</p>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p className="text-danger">
                            Los servicios que ofrecen dichos usuarios, son bajo su responsabilidad, y dichos usuarios o empresas,
                            o no poseen un carácter vinculante con dicho portal
                        </p>
                    </Col>   
                </Row>
                {!this.state.see && <Row className="my-2">
                    <Col xs={12}>
                        <h5><span className="text-primary"><b>Consultar servicios:</b></span> <span className="font-weight-light">Seleccione sus opciones de búsqueda</span></h5>
                    </Col>
                    <Col className="mb-5 filter-service" xs={12}>
                        <Row>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Continente
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                    <Form.Control as="select">
                                    <option>Seleccione el continente</option>
                                    <option>Norteamérica</option>
                                    <option>Suramérica</option>
                                    <option>Europa</option>
                                    <option>Asia</option>
                                    <option>Oceanía</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    País
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select">
                                    <option>Seleccione el país</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Estado
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                    <Form.Control as="select">
                                    <option>Seleccione el estado</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Ciudad
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select">
                                    <option>Seleccione la ciudad</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Tipo de servicio
                                </Button>
                                <Select
                                    placeholder="Seleccione el servicio a ofrecer"
                                    options={this.state.servicios}
                                    isMulti
                                />
                            </Col>
                        </Row>
                        <Row className="mt-5">
                            <Col/>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    block
                                >
                                    Crear
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    block
                                >
                                    Cancelar
                                </Button>
                            </Col>
                            <Col/>
                        </Row>
                    </Col>
                </Row>}
                <Row className="my-4">
                    <Col xs={12}><h3 className="text-primary">Avalúo de apartamentos</h3></Col>
                    <Col>
                        <Table responsive borderless className="text-center">
                            <thead className="bg-warning text-light">
                                <tr>
                                <th>País</th>
                                <th>Estado</th>
                                <th>Ciudad</th>
                                <th>Tipos de usuario</th>
                                <th>Datos de contacto</th>
                                <th>Datos de contacto</th>
                                <th>Días y horas de contacto</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                {this.state.see && <td><Link><img className="" width="20" height="20" src={iconEdit} alt="Edit"/><img className="" width="20" height="20" src={iconDelete} alt="Delete"/></Link></td>}
                                </tr>
                                <tr>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                {this.state.see && <td><Link><img className="" width="20" height="20" src={iconEdit} alt="Edit"/><img className="" width="20" height="20" src={iconDelete} alt="Delete"/></Link></td>}
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row className="my-4">
                    <Col xs={12}><h3 className="text-primary">Arreglar cocinas o pisos</h3></Col>
                    <Col>
                        <Table responsive borderless className="text-center">
                            <thead className="bg-warning text-light">
                                <tr>
                                <th>País</th>
                                <th>Estado</th>
                                <th>Ciudad</th>
                                <th>Tipos de usuario</th>
                                <th>Datos de contacto</th>
                                <th>Datos de contacto</th>
                                <th>Días y horas de contacto</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                {this.state.see && <td><Link><img className="" width="20" height="20" src={iconEdit} alt="Edit"/><img className="" width="20" height="20" src={iconDelete} alt="Delete"/></Link></td>}
                                </tr>
                                <tr>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                {this.state.see && <td><Link><img className="" width="20" height="20" src={iconEdit} alt="Edit"/><img className="" width="20" height="20" src={iconDelete} alt="Delete"/></Link></td>}
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row className="my-5 info-service" xs={12}>
                    <Col>
                        <p><b>Más información</b></p>
                        <p>Haga clic en el siguiente enlace:</p>
                        <ul>
                            <li>
                                <Button
                                    variant="link"
                                    className="text-dark"
                                    href="/contact"
                                >
                                    <b>Contáctenos</b>
                                </Button>
                            </li>
                            <li>
                                <a
                                    variant="link"
                                    className="text-dark"
                                    href='/ayuda'
                                >
                                    <b>Preguntas frecuentes</b>
                                </a>
                            </li>
                        </ul>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    }
}