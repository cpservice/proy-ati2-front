import React, { Component } from 'react';
import { Row, Col, Container } from 'react-bootstrap';

class Ayuda extends Component {
render() {
    return (
        <Container>
            <Row className="mt-5">
                <Col className="title-create">
                    <h2 className="text-center font-weight-light">Preguntas Frecuentes</h2>
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>1. ¿Cómo publico mi propiedad en Atinmueble?</p>
                    <p>Es sencillo, dirígete a nuestra sección Viviendas en el menu principal donde encontrarás la opcion de Publicar.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>2. ¿Me cobrarán alguna comisión por la venta de mi inmueble?</p>
                    <p>No. Atinmueble no cobra ninguna tarifa o comisión relacionada con el resultado de la transacción. Como sitio de fotoclasificados en Internet, Atinmueble presta un servicio de publicación y/o alojamiento de avisos clasificados en los términos contratados por el Usuario y su plataforma sirve para que contacten vendedores y compradores, arrendadores y arrendatarios, sin que participe en la negociación de estos bienes. La compra/venta y/o alquiler de los bienes inmuebles (terrenos, casas, apartamentos, etc.) se realiza directa y únicamente entre las personas involucradas. Como sitio de fotoclasificados en Internet, Atinmueble es un medio de contacto entre vendedores y compradores, arrendadores y arrendatarios; y no participa en la negociación de estos bienes.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>3. ¿Cómo puedo ver las publicaciones de las viviendas?</p>
                    <p>Es sencillo, dirígete a nuestra sección Viviendas en el menu principal donde encontrarás la opcion de Ver Publicaciones.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>4. ¿Cómo puedo ver las publicaciones de las viviendas?</p>
                    <p>Es sencillo, dirígete a nuestra sección Viviendas en el menu principal donde encontrarás la opcion de Ver Publicaciones.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>5. ¿Cómo puedo buscar una publicacion en particular?</p>
                    <p>Es sencillo, dirígete a nuestra sección Viviendas en el menu principal donde encontrarás la opcion de Buscar.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>6. ¿Puedo modificar mi publicacion?</p>
                    <p>Sí, puedes modificar los datos básicos de tu publicacion dirigiendote a nuestra sección Viviendas en el menu principal donde encontrarás la opcion de Modificar.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>7. ¿Puedo eliminar mi publicacion?</p>
                    <p>Sí, puedes eliminar tu publicacion dirigiendote a nuestra sección Viviendas en el menu principal donde encontrarás la opcion de Eliminar.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>8. ¿Cómo puedo contactarlos?</p>
                    <p>Es sencillo, dirígete a nuestra sección Contactenos en el menu principal donde podras conseguir toda la informacion referente a nuestra empresa, nuestras redes y un formulario de contacto que puedes rellenar para que nos dejes tu email, tus datos para que nosotros podamos contactarte luego.</p>
                </Col>
            </Row>
            <Row className="mt-5">
                <Col className="title-create">
                    <h2 className="text-center font-weight-light">Terminos De Servicio</h2>
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>1. Capacidad</p>
                    <p>Los Servicios solo están disponibles para personas que tengan capacidad legal para contratar. No podrán utilizar los servicios las personas que no tengan esa capacidad, los menores de edad o Usuarios de Atinmueble que hayan sido suspendidos temporalmente o inhabilitados definitivamente. Si estás inscribiendo un Usuario como Empresa, debes tener capacidad para contratar a nombre de tal entidad y de obligar a la misma en los términos de este Acuerdo.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>2. Registro</p>
                    <p>Es obligatorio completar el formulario de registro en todos sus campos con datos válidos para poder utilizar los servicios que brinda Atinmueble. El futuro Usuario deberá completarlo con su información personal de manera exacta, precisa y verdadera ("Datos Personales") y asume el compromiso de actualizar los Datos Personales conforme resulte necesario. Atinmueble podrá utilizar diversos medios que considere pertinente para identificar a sus Usuarios, sin embargo, Mercado Libre no se responsabiliza por la certeza de los Datos Personales provistos por sus Usuarios. Los Usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de los Datos Personales ingresados.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>3. Modificaciones del Acuerdo</p>
                    <p>Atinmueble podrá modificar los Términos y Condiciones Generales en cualquier momento haciendo públicos en el Sitio los términos modificados. Todos los términos modificados entrarán en vigor a los 10 (diez) días de su publicación. Dichas modificaciones serán comunicadas por Mercado Libre a los usuarios que en la Configuración de su Cuenta de Mercado Libre hayan indicado que desean recibir notificaciones de los cambios en estos Términos y Condiciones. Todo usuario que no esté de acuerdo con las modificaciones efectuadas por Mercado Libre podrá solicitar la baja de la cuenta.<br/>
                    El uso del sitio y/o sus servicios implica la aceptación de estos Términos y Condiciones generales de uso de Atinmueble.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>4. Listado de Bienes</p>
                    <p>Publicación de bienes y/o servicios. El Usuario deberá ofrecer los bienes y/o servicios en las categorías y subcategorías apropiadas. Las publicaciones podrán incluir textos descriptivos, gráficos, fotografías y otros contenidos y condiciones pertinentes para la venta del bien o contratación del servicio, siempre que no violen ninguna disposición de este acuerdo o demás políticas de Atinmueble. El producto ofrecido por el Usuario Vendedor debe ser exactamente descrito en cuanto a sus condiciones, características relevantes y cantidades. Se entiende y presume que mediante la inclusión del bien o servicio en Atinmueble, el Usuario acepta que tiene la intención y el derecho de vender el bien por él ofrecido, o está facultado para ello por su titular y lo tiene disponible para su entrega inmediata. En los casos de ventas de bienes inmuebles, un mismo Usuario solo podrá hacer una (1) publicación por cada bien ofertado. Todos los artículos a ser publicados en el Sitio Web deberán mostrar su precio de venta, salvo disposición contraria de Ley. Asimismo, se establece que los precios de los productos publicados deberán ser expresados con IVA incluido cuando corresponda la aplicación del mismo, y en moneda de curso legal. Atinmueble podrá remover cualquier publicación cuyo precio no sea expresado de esta forma para evitar confusiones o malos entendidos en cuanto al precio final del producto. Se deja expresamente establecido que ninguna descripción podrá contener datos personales o de contacto, tales como, y sin limitarse a, números telefónicos, dirección de e-mail, dirección postal, direcciones de páginas de Internet que contengan datos como los mencionados anteriormente, salvo para las categorías Carros, motos y otros, Inmuebles y Propiedades y Servicios en las que se permite la inclusión del teléfono del Vendedor en la publicación, y en la categoría Servicios donde además se publicará la dirección de correo electrónico y el teléfono del Vendedor, y se podrá incluir también el domicilio y la url de su Sitio Web.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>5. Privacidad de la Información, Seguridad y Confidencialidad de datos personales</p>
                    <p>Tal como se indicó anteriormente, para utilizar los Servicios ofrecidos por Atinmueble, los Usuarios deberán facilitar determinados datos de carácter personal. Su información personal se procesa y almacena en servidores o medios magnéticos que mantienen altos estándares de seguridad y protección tanto física como tecnológica.<br/>Atinmueble no venderá, alquilará ni negociará con otras empresas la información personal de los Usuarios salvo en las formas y casos establecidas en las políticas de privacidad publicadas en el sitio. La información personal se almacena en servidores o medios magnéticos que mantienen altos estándares de seguridad. Atinmueble hará sus mejores esfuerzos para mantener la confidencialidad y seguridad de los Datos Personales de sus Usuarios.Para mayor información sobre la privacidad de los Datos Personales y casos en los que será revelada la información personal, se pueden consultar nuestras Políticas de Privacidad.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>6. Obligaciones de los Usuarios</p>
                    <p>Obligaciones del Comprador. Durante el plazo fijado por el Usuario Vendedor, los Usuarios interesados realizarán ofertas de compra para los bienes y ofertas de contratación para los servicios. La oferta de venta se cierra una vez que vence el plazo o se acaban las cantidades estipuladas por el Vendedor, y la oferta de contratación del servicio culmina con el vencimiento del plazo de la publicación.<br/>El Comprador está obligado a intentar comunicarse con el vendedor y completar la operación si ha realizado una oferta por un artículo publicado bajo la modalidad de "compra inmediata" o si realizó la oferta más alta, en los casos en que esta modalidad esté permitida, salvo que la operación esté prohibida por la ley o los Términos y Condiciones Generales y demás políticas de Atinmueble, en cuyo caso no estará obligado a concretar la operación.<br/>Al ofertar por un artículo el Usuario acepta quedar obligado por las condiciones de venta incluidas en la descripción del artículo en la medida en que las mismas no infrinjan las leyes, los derechos del ciudadano como consumidor, los Términos y Condiciones Generales y demás políticas de Atinmueble. La oferta de compra es irrevocable salvo en circunstancias excepcionales, tales como que el vendedor cambie sustancialmente la descripción del artículo después de realizada alguna oferta, que exista un claro error tipográfico, o que no pueda verificar la identidad del vendedor.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>7. Prohibiciones</p>
                    <p>Los Usuarios no podrán: (a) manipular los precios de los artículos; (b) interferir en la puja entre distintos Usuarios; (c) mantener ningún tipo de comunicación por e-mail, o por cualquier otro medio (incluso redes sociales) durante la oferta del bien con ninguno de los Usuarios que participan en la misma, salvo en la sección de preguntas y respuestas; (d) dar a conocer y/o aceptar datos personales (incluyendo pero sin limitar a cuentas de Twitter, Facebook y/o cualquier otra red social). Excepcionalmente, se permite: 1) informar la dirección del comprador para que el vendedor calcule el costo de envío; 2) lo estipulado específicamente para la categoría Autos, motos y otros, Servicios e Inmuebles y Propiedades; (e) publicar o vender artículos prohibidos o en condiciones contrarias a lo previsto en los Términos y Condiciones Generales, demás políticas de Atinmueble o leyes vigentes; (f) insultar o agredir a otros Usuarios; (g) utilizar su reputación, calificaciones o comentarios recibidos en el sitio de Mercado Libre en cualquier ámbito fuera de Atinmueble, (h) ofrecer productos a la venta por encima de los precios permitidos o con márgenes de ganancia superiores a los establecidos en el ordenamiento jurídico vigente en Venezuela, a los que se hace referencia en la Cláusula 6 de estos Términos y Condiciones Generales; (j) publicar productos idénticos en más de una publicación.<br/>Este tipo de actividades será investigado por Atinmueble y el infractor podrá ser sancionado con la suspensión o cancelación de la oferta e incluso de su inscripción como Usuario de Atinmueble y/o de cualquier otra forma que estime pertinente, sin perjuicio de las acciones legales a que pueda dar lugar por la configuración de delitos o contravenciones o los perjuicios civiles que pueda causar a los Usuarios oferentes.</p>
                </Col>
                <Col xs={12}>
                    <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>8. Violaciones del Sistema o Bases de Datos</p>
                    <p>No está permitida ninguna acción o uso de dispositivo, software, u otro medio tendiente a interferir tanto en las actividades y operatoria de Atinmueble como en las ofertas, descripciones, cuentas o bases de datos de Atinmueble. Cualquier intromisión, tentativa o actividad violatoria o contraria a las leyes sobre derecho de propiedad intelectual y/o a las prohibiciones estipuladas en este contrato harán pasible a su responsable de las acciones legales pertinentes, y a las sanciones previstas por este acuerdo, así como lo hará responsable de indemnizar los daños ocasionados.</p>
                </Col>
            </Row>
        </Container>
    )
  }
}

export default Ayuda;