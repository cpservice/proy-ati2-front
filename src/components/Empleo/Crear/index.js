import React, { Component } from 'react';
import { Form, Row, Col, Container, Button } from 'react-bootstrap';
import Select from 'react-select';

class Crear extends Component {
    state = {
        vacante: '',
        horario: '',
        funcion: '',
        modalidad: '',
        continente: '',
        pais: '',
        estado: '',
        ciudad: ''
    };

    //   handleChange = (e) => {
    //     this.setState({
    //       [e.target.id]: e.target.value
    //     })
    //   }

    //   handleSubmit = () => {
    //     const {full_name, subject, to, message, from} = this.state;

    //     const data = {
    //       full_name: full_name,
    //       message: message,
    //       subject: subject,
    //       email: from,
    //       to: to
    //     }

    //     Axios
    //     .post('https://api.atinmueble.ml/api/email/send', data)
    //     .then(response => {
    //         console.log("response", response)
    //       }
    //     )
    //   }

    handleChange = e => {
        this.setState({
            [e.target.id]: e.target.value
        });
    };

    handleSubmit = async e => {
        // const data = {
        //     rooms,
        //     baths,
        //     parks
        // }
        // if(checkForm(data) !== false){
        //     console.log('data1', data)
        // //   Axios
        // //   .post('/', data)
        // //   .then(response => {
        // //       console.log("response", response)
        // //     }
        // //   )
        // }else{
        //     console.log('data2', data)
        // }
    };

    checkForm = form => {
        if (form.rooms < 1) {
            alert('Error: debe incluir al menos una habitacion.');
            return false;
        }
        if (form.baths < 1) {
            alert('Error: debe incluir al menos un baño.');
            return false;
        }
        if (form.parks < 1) {
            alert('Error: debe incluir la menos un estacionamiento.');
            return false;
        }
        return true;
    };

    render() {
        return (
            <Container>
                <Row className="mt-5">
                    <Col className="title-create">
                        <h2 className="text-center font-weight-light">
                            Crear Empleo
                        </h2>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center" xs={12}>
                        <p className="font-weight-bold">
                            Introduzca los datos de la vacante que desea
                            agregar, a continuación:
                        </p>
                    </Col>
                    <Col xs={12}>
                        <Form>
                            <Form.Group as={Row} controlId="Vacante">
                                <Form.Label
                                    column
                                    sm="3"
                                    className="text-primary"
                                >
                                    Vacante
                                </Form.Label>
                                <Col sm="9">
                                    <Form.Control
                                        type="text"
                                        id="vacante"
                                        value={this.vacante}
                                        onChange={e => this.handleChange(e)}
                                        placeholder="Introduzca la vacante que desea agregar"
                                    />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col xs={12}>
                        <Form>
                            <Form.Group as={Row} controlId="Horario">
                                <Form.Label
                                    column
                                    sm="3"
                                    className="text-primary"
                                >
                                    Horario
                                </Form.Label>
                                <Col sm="9">
                                    <Form.Control
                                        type="text"
                                        id="horario"
                                        value={this.horario}
                                        onChange={e => this.handleChange(e)}
                                        placeholder="Indique dias y horas en que se requiere la vacante"
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="Modalidad">
                                <Form.Label
                                    column
                                    sm="3"
                                    className="text-primary"
                                >
                                    Modalidad de contratacion
                                </Form.Label>
                                <Col sm="9">
                                    <Form.Control
                                        as="select"
                                        id={this.modalidad}
                                        onChange={e => this.handleChange(e)}
                                    >
                                        <option disabled>
                                            Seleccione la modalidad
                                        </option>
                                        <option value="Honorarios Profesionales">
                                            Honorarios Profesionales
                                        </option>
                                        <option value="Empleo fijo y con beneficios">
                                            Empleo fijo y con beneficios
                                        </option>
                                    </Form.Control>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="Funciones">
                                <Form.Label
                                    column
                                    sm="3"
                                    className="text-primary"
                                >
                                    Funciones
                                </Form.Label>
                                <Col sm="9">
                                    <Form.Control
                                        as="textarea"
                                        id="funcion"
                                        value={this.funcion}
                                        onChange={e => this.handleChange(e)}
                                        placeholder="Describa aqui las funciones de la vacante"
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="Interactua">
                                <Form.Label
                                    column
                                    sm="3"
                                    className="text-primary"
                                >
                                    Con quien interactua
                                </Form.Label>
                                <Col sm="9">
                                    <Select
                                        placeholder="Seleccione los roles"
                                        options={[
                                            {
                                                label: 'Director general',
                                                value: 'Director general'
                                            },
                                            {
                                                label:
                                                    'Asistente administrativo-recepcionista',
                                                value:
                                                    'Asistente administrativo-recepcionista'
                                            },
                                            {
                                                label: 'Diseñador Grafico',
                                                value: 'Diseñador Grafico'
                                            },
                                            {
                                                label: 'Desarrollador Web',
                                                value: 'Desarrollador Web'
                                            },
                                            {
                                                label: 'Desarrollador movil',
                                                value: 'Desarrollador movil'
                                            },
                                            {
                                                label: 'Abogado',
                                                value: 'Abogado'
                                            },
                                            {
                                                label: 'Contador',
                                                value: 'Contador'
                                            }
                                        ]}
                                        isMulti
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="Aporta">
                                <Form.Label
                                    column
                                    sm="3"
                                    className="text-primary"
                                >
                                    A quien aporta
                                </Form.Label>
                                <Col sm="9">
                                    <Select
                                        placeholder="Seleccione los roles"
                                        options={[
                                            {
                                                label: 'Director general',
                                                value: 'Director general'
                                            },
                                            {
                                                label:
                                                    'Asistente administrativo-recepcionista',
                                                value:
                                                    'Asistente administrativo-recepcionista'
                                            },
                                            {
                                                label: 'Diseñador Grafico',
                                                value: 'Diseñador Grafico'
                                            },
                                            {
                                                label: 'Desarrollador Web',
                                                value: 'Desarrollador Web'
                                            },
                                            {
                                                label: 'Desarrollador movil',
                                                value: 'Desarrollador movil'
                                            },
                                            {
                                                label: 'Abogado',
                                                value: 'Abogado'
                                            },
                                            {
                                                label: 'Contador',
                                                value: 'Contador'
                                            }
                                        ]}
                                        isMulti
                                    />
                                </Col>
                            </Form.Group>
                        </Form>
                        <Row>
                            <Col
                                xs={12}
                                className="text-primary text-center"
                                style={{ fontSize: '30px' }}
                            >
                                ¿En que lugar se solicita el empleo?
                            </Col>
                            <br />
                            <br />
                            <Col xs={3}>
                                <Button disabled variant="primary" block>
                                    Continente
                                </Button>
                                <Form.Group
                                    className="mt-2"
                                    controlId="Continente"
                                >
                                    <Form.Control
                                        as="select"
                                        id={this.continente}
                                        onChange={e => this.handleChange(e)}
                                    >
                                        <option>
                                            Seleccione el continente
                                        </option>
                                        <option value="Norteamérica">
                                            Norteamérica
                                        </option>
                                        <option value="Suramérica">
                                            Suramérica
                                        </option>
                                        <option value="Europa">Europa</option>
                                        <option value="Asia">Asia</option>
                                        <option value="Oceanía">Oceanía</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col xs={3}>
                                <Button disabled variant="primary" block>
                                    País
                                </Button>
                                <Form.Group className="mt-2" controlId="Pais">
                                    <Form.Control
                                        as="select"
                                        id={this.pais}
                                        onChange={e => this.handleChange(e)}
                                    >
                                        <option>Seleccione el país</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col xs={3}>
                                <Button disabled variant="primary" block>
                                    Estado
                                </Button>
                                <Form.Group className="mt-2" controlId="Estado">
                                    <Form.Control
                                        as="select"
                                        id={this.estado}
                                        onChange={e => this.handleChange(e)}
                                    >
                                        <option>Seleccione el estado</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col xs={3}>
                                <Button disabled variant="primary" block>
                                    Ciudad
                                </Button>
                                <Form.Group className="mt-2" controlId="Ciudad">
                                    <Form.Control
                                        as="select"
                                        id={this.ciudad}
                                        onChange={e => this.handleChange(e)}
                                    >
                                        <option>Seleccione la ciudad</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col />
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    block
                                >
                                    Crear
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    block
                                >
                                    Cancelar
                                </Button>
                            </Col>
                            <Col />
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Crear;
