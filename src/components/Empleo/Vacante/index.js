import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Button} from "react-bootstrap";
import { Link } from 'react-router-dom';
import iconFb from './../../../icons/fb.png';
import iconTw from './../../../icons/twitter.png';
import iconEmail from './../../../icons/email.png';
import iconInst from './../../../icons/instagram.jpeg';
import './index.css';

export default class Vacante extends Component {
    constructor (props) {
        super(props)

        this.state = {
            empleos:[],
            vacante: "",
            horario: "",
            contratacion: "",
            funciones: [],
            interactua: "",
            reporta: "",
            pais: "",
            estado: "",
            ciudad: "",
            email: "",
        }
    }


    componentDidMount () {
        this.setState({
            empleos: [
                {label: "Desarrollador Web", url: "./vacante"},
                {label: "Diseñador Gráfico", url: "./vacante"},
                {label: "Desarrollador de aplicaciones móviles", url: "./vacante"},
                {label: "Abogado", url: "./vacante"},
                {label: "Contador", url: "./vacante"},
                {label: "Desarrollador Web", url: "./vacante"},
                {label: "Diseñador Gráfico", url: "./vacante"},
                {label: "Desarrollador de aplicaciones móviles", url: "./vacante"},
                {label: "Abogado", url: "./vacante"},
                {label: "Contador", url: "./vacante"}
            ],
            vacante: "Diseñador Gráfico",
            horario: "A convenir",
            contratacion: "Honorarios profesionales",
            funciones: ["Crear las interfaces e iconos para el portal Web de la empresa"],
            interactua: "Director General",
            reporta: "Director General",
            pais: "Venezuela",
            estado: "Caracas",
            ciudad: "Caracas",
            email: "correo@ejemplo.com",
        })
    }

    moarEmpleos = () => {

        const empleos = this.state.empleos

        const moar = empleos.map((empleo, key) => {
            return (
                <Col key={key} xs={12} className="my-2">
                    <Link to={empleo.url}>{empleo.label}</Link>
                </Col>
            )
        })

        return moar
    }

    funcionesVacante = () => {
        const funciones = this.state.funciones

        const funct = funciones.map((funcion, key) => {
            return (
                <li key={key}>
                    {funcion}
                </li>
            )
        })

        return funct
    }


    render () {

        const vacante = {
            title: this.state.vacante,
            horario: this.state.horario,
            contratacion: this.state.contratacion,
            funciones: this.state.funciones,
            interactua: this.state.interactua,
            reporta: this.state.reporta,
            pais: this.state.pais,
            estado: this.state.estado,
            ciudad: this.state.ciudad,
            email: this.state.email,
        }

        return <Fragment>
            <Container>
                <Row className="my-3">
                    <Col>
                        <p>Siempre estamos en la búsqueda de talento. 
                            Por eso hemos dedicado esta sección de ofertas de empleos  en nuestro sitio Web
                        </p>
                        <p className="font-weight-bold">Síguenos o compártelo por: 
                            <span>
                                <Link to="./"><img width="20" height="20" className="mx-1" src={iconFb} alt="Facebook"/></Link>
                                <Link to="./"><img width="20" height="20" className="mx-1" src={iconTw} alt="Twitter"/></Link>
                                <Link to="./"><img width="20" height="20" className="mx-1" src={iconInst} alt="Instagram"/></Link>
                                <Link to="./"><img width="20" height="20" className="mx-1" src={iconEmail} alt="Email"/></Link>
                            </span>
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col xs={3}>
                        <Row>
                            <Col><h4 className="font-weight-light">Empleos disponibles</h4></Col>
                        </Row>
                        <Row className="empl-scroll">
                            {this.moarEmpleos()}
                        </Row>
                    </Col>
                    <Col className="ml-4">
                        <Row>
                            <Col>
                                <h4 className="font-weight-light">Vacante: <span>{vacante.title}</span></h4>
                            </Col>
                        </Row>
                        <Row className="my-2">
                            <Col>
                                <p className="font-weight-bold">Horario</p>
                            </Col>
                            <Col>
                                <Button variant="link">
                                    {vacante.horario}
                                </Button>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">Modalidad de contratación</p>
                            </Col>
                            <Col>
                                <Button variant="link">
                                    {vacante.contratacion}
                                </Button>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">Funciones</p>
                                <ul>
                                    {this.funcionesVacante(vacante.funciones)}
                                </ul>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">Con quién interactúa</p>
                            </Col>
                            <Col>
                                <p>
                                    {vacante.interactua}
                                </p>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">A quién reporta</p>
                            </Col>
                            <Col>
                                <p>
                                    {vacante.reporta}
                                </p>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">País</p>
                            </Col>
                            <Col>
                                <Button variant="link">
                                    {vacante.pais}
                                </Button>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">Estado</p>
                            </Col>
                            <Col>
                                <Button variant="link">
                                    {vacante.estado}
                                </Button>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">Ciudad</p>
                            </Col>
                            <Col>
                                <Button variant="link">
                                    {vacante.ciudad}
                                </Button>
                            </Col>
                        </Row>
                        <Row className="mb-2">
                            <Col>
                                <p className="font-weight-bold">
                                    Si deseas postularte, envía tu currículum vitae, al correo:
                                    <span>
                                        <Button href={'mailto:' + vacante.email} variant="link">
                                            {vacante.email}
                                        </Button>
                                    </span>
                                </p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>

        </Fragment>
    }

}