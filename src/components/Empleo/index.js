import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Button, Form, Table } from "react-bootstrap";
import { Link } from 'react-router-dom';
import Select from 'react-select';
import iconEdit from '../../icons/write.png';
import iconDelete from '../../icons/delete.png';
import iconFb from './../../icons/fb.png';
import iconTw from './../../icons/twitter.png';
import iconEmail from './../../icons/email.png';
import iconInst from './../../icons/instagram.jpeg';
import iconCreate from '../../icons/create.png';


export default class Empleo extends Component {
    constructor (props) {
        super(props)

        this.state = {
            servicios: [{label:"Permanente", value:"Permanente"}, {label:"Por honorarios profesionales",value:"Por honorarios profesionales"}, {label:"Por contrato", value:"Por contrato"}],
            see: false,
        }
    }


    // componentDidMount () {

    //     if(this.props.see){
    //         this.setState({see: this.props.see})
            
    //     }
    // }


    render () {
        return <Fragment>
            <Container>
                <Row className="my-5">
                    <Col><h5 className="text-center">Podemos ayudarte con las siguientes actividades</h5></Col>
                </Row>
                <Row className="my-5">
                    <Col>
                        <p>Siempre estamos en la búsqueda de talento. 
                            Por eso hemos dedicado esta sección de ofertas de empleos  en nuestro sitio Web
                        </p>
                        <p className="font-weight-bold">Síguenos o compártelo por: 
                            <span>
                                <Link to="./" className="mx-1"><img width="20" height="20" src={iconFb} alt="Facebook"/></Link>
                                <Link to="./" className="mx-1"><img width="20" height="20" src={iconTw} alt="Twitter"/></Link>
                                <Link to="./" className="mx-1"><img width="20" height="20" src={iconInst} alt="Instagram"/></Link>
                                <Link to="./" className="mx-1"><img width="20" height="20" src={iconEmail} alt="Email"/></Link>
                            </span>
                        </p>
                    </Col>
                </Row>
                {!this.state.see && <Row className="my-2">
                    <Col xs={12}>
                        <h3 className="text-primary font-weight-bold">Opciones de búsqueda de empleo</h3>
                    </Col>
                    <Col xs={12}>
                        <h4 className="text-danger font-weight-light text-center my-4">Seleccione sus opciones de búsqueda</h4>
                    </Col>
                    <Col className="mb-5 filter-service" xs={12}>
                        <Row>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Continente
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                    <Form.Control as="select">
                                    <option>Seleccione el continente</option>
                                    <option>Norteamérica</option>
                                    <option>Suramérica</option>
                                    <option>Europa</option>
                                    <option>Asia</option>
                                    <option>Oceanía</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    País
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select">
                                    <option>Seleccione el país</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Estado
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect1">
                                    <Form.Control as="select">
                                    <option>Seleccione el estado</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Ciudad
                                </Button>
                                <Form.Group className="mt-2" controlId="exampleForm.ControlSelect2">
                                    <Form.Control as="select">
                                    <option>Seleccione la ciudad</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Button
                                    disabled
                                    variant="primary"
                                    block
                                >
                                    Tipo de de empleo
                                </Button>
                                <Select
                                    className="mt-2"
                                    placeholder="Seleccione"
                                    options={this.state.servicios}
                                />
                            </Col>
                        </Row>
                        <Row className="mt-5">
                            <Col/>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    block
                                >
                                    Consultar
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    variant="warning"
                                    className="text-light"
                                    block
                                >
                                    Cancelar
                                </Button>
                            </Col>
                            <Col/>
                        </Row>
                    </Col>
                </Row>}
                <Row className="my-4">
                    <Col xs={9}><h3 className="text-primary">Empleos Disponibles</h3></Col>
                    <Col xs={3}>
                        {this.state.see && <Link to="./create">
                            <img width="25" height="25" className="d-flex justify-content-end" src={iconCreate} alt="Crear"/>
                        </Link>}
                    </Col>
                    <Col>
                        <Table responsive borderless className="text-center">
                            <thead className="bg-warning text-light">
                                <tr>
                                <th>País</th>
                                <th>Estado</th>
                                <th>Ciudad</th>
                                <th>Vacante</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td><Link to="./empleo/vacante">Table cell</Link></td>
                                {this.state.see && <td><Link className="mx-1" to="./"><img width="20" height="20" src={iconEdit} alt="Edit"/></Link><Link className="mx-1" to="./"><img width="20" height="20" src={iconDelete} alt="Delete"/></Link></td>}
                                </tr>
                                <tr>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td><Link to="./empleo/vacante">Table cell</Link></td>
                                {this.state.see && <td><Link className="mx-1" to="./"><img width="20" height="20" src={iconEdit} alt="Edit"/></Link><Link className="mx-1" to="./"><img width="20" height="20" src={iconDelete} alt="Delete"/></Link></td>}
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p className="text-center">
                            <span className="text-danger">*</span>
                             Haz <span className="text-primary">click</span> en el nombre de la vacante, para ver más detalles
                        </p>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    }
}