import React, { Component } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import icon1 from './../../icons/fb.png';
import icon2 from './../../icons/twitter.png';
import icon3 from './../../icons/linkedin.png';
import icon4 from './../../icons/youtube.png';
import icon5 from './../../icons/instagram.jpeg';
import casa1 from './../../icons/casa1.jpeg'

class Conocenos extends Component {
    render() {
        return (
            <Container>
                <Row className="mt-5">
                    <Col className="title-create">
                        <h2 className="text-center text-light font-weight-light">Conocenos mas...</h2>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col xs={12}>
                        <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>Nuestro propósito</p>
                        <p>Facilitando para ti un espacio donde puedas publicar tus inmuebles de una forma segura y facil. Queremos ayudarte a compartir y vender tu inmueble desde la comodidad de tu casa donde puedes administrar las publicaciones y hacer uso de nuestros servicios. Esta es la forma en que contribuyen a la sociedad al tiempo que garantiza el éxito a largo plazo de nuestra empresa.
                            <br/><br/>Tambien abrimos la puerta a las posibilidades para que consigas el inmueble de tus sueños entre los miles publicados en nuestro sitio, arriesgate y busca! no pierdas el tiempo, que estas esperando?
                        </p>
                    </Col>
                    <Col xs={12}>
                        <p className="font-weight-bold text-primary" style={{fontSize:"20px"}}>Nuestros Principios de Negocios</p>
                        <p> Nuestros valores se reflejan en la forma en la que hacemos negocios, siempre actuando de manera legal y honesta con respeto tanto para nuestra propia gente como con aquellos con quienes hacemos negocios. Lea mas sobre nuestros principios de negocio.</p>
                    </Col>
                </Row>
                <Row className="mt-2" style={{background:"#ffbf3f", borderRadius:"15px"}}>
                    <Col className="mt-2 ml-2">
                        <p className="text-left font-weight-light" style={{fontSize:"30px"}}>Registrate!</p>
                        <p className="text-left font-weight-light" style={{fontSize:"20px"}}>Unete para recibir noticias frecuentes sobre nuestros inmuebles...<Link style={{textDecoration:"none", color:"#007bff", fontWeight:"bold"}} to="/register">Registro</Link></p>
                    </Col>
                    <Col className="mt-2 ml-2">
                        <p className="text-left font-weight-light" style={{fontSize:"30px"}}>Contactanos!</p>
                        <p className="text-left font-weight-light" style={{fontSize:"20px"}}>Dejanos saber tus preocupaciones y preguntas...<Link style={{textDecoration:"none", color:"#007bff", fontWeight:"bold"}} to="/contact">Contacto</Link></p>
                    </Col>
                    <Col className="mt-2 ml-2">
                        <p className="text-left font-weight-light" style={{fontSize:"30px"}}>Ayuda!</p>
                        <p className="text-left font-weight-light" style={{fontSize:"20px"}}>Si tienes alguna duda sobre nuestros servicios pasate por las preguntas frecuentes y condiciones de servicio...<Link style={{textDecoration:"none", color:"#007bff", fontWeight:"bold"}} to="/ayuda">Ayuda</Link></p>
                    </Col>
                </Row>
                <Row className="mt-4">
                  <Col md={2}>
                    <p className="titles_contactenos">Síguenos en </p>
                  </Col>
                  <Col md={5}>
                    <p>
                      <img width="30" height="30" src={icon1} alt="img" style={{cursor:"pointer"}}></img> Facebook
                    </p>
                    <p>
                      <img width="30" height="30" src={icon2} alt="img" style={{cursor:"pointer"}}></img> Twitter
                    </p>
                    <p>
                      <img width="30" height="30" src={icon4} alt="img" style={{cursor:"pointer"}}></img> Youtube
                    </p>
                    <p>
                      <img width="30" height="30" src={icon5} alt="img" style={{cursor:"pointer"}}></img> Instagram
                    </p>
                    <p>
                      <img width="30" height="30" src={icon3} alt="img" style={{cursor:"pointer"}}></img> Linkedin
                    </p>
                  </Col>
                  <Col md={5}>
                    <Link to="/"> <img width="400" height="250" src={casa1} alt="img" style={{cursor:"pointer", borderRadius:"15px"}}></img></Link>
                  </Col>
                </Row>
                <br/>
            </Container>
        )
    }
}

export default Conocenos;