import React, { Component } from 'react';
import { Button, Row, Col } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import Axios from 'axios';
import './contactenos.css';
import icon1 from './../../icons/fb.png';
import icon2 from './../../icons/twitter.png';
import icon3 from './../../icons/linkedin.png';
import icon4 from './../../icons/youtube.png';
import icon5 from './../../icons/instagram.jpeg';

class Contactenos extends Component {
  state = {
    full_name: '',
    subject:'',
    to:'nirvana01@gmail.com',
    message:'',
    from:''
  } 

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = () => {
    const {full_name, subject, to, message, from} = this.state;

    const data = {
      full_name: full_name, 
      message: message,
      subject: subject,
      email: from,
      to: to
    }
    
    Axios
    .post('https://api.atinmueble.ml/api/email/send', data)
    .then(response => {
        console.log("response", response)
      }
    )
  }

  render() {
    return (
        <Row style={{marginRight:"0px !important", marginLeft:"0px !important"}}>
            <Col md={6} style={{paddingLeft:"50px", paddingTop:"50px"}}>
                <p><span className="titles_contactenos">Teléfonos</span><br/>+58(0212)-362-82-68<br/>+58(0414)-389-74-44</p>
                <p><span className="titles_contactenos">Atención al público</span><br/>Lunes a Viernes: De 8 am a 12m. Y de 1pm a 5pm<br/>Sábados y Domingo: De 8 am a 12m. Y de 1pm a 5pm</p>
                <p><span className="titles_contactenos">Correo electrónico</span><br/>Envíanos tus preguntas o comentarios a <br/> nirvana01@gmail.com</p>
                <p></p>
                <p><span className="titles_contactenos">Enlaces de interés</span><br/>
                <ul>
                    <a className="links" href='/ayuda'>Preguntas Frecuentes</a>
                    <br/>
                    <a className="links" href='/ayuda'>Términos de servicio</a>
                </ul></p>
                <Row>
                  <Col md={3}>
                    <p className="titles_contactenos">Síguenos en </p>
                  </Col>
                  <Col md={9}>
                    <p>
                      <img width="30" height="30" src={icon1} alt="img" style={{cursor:"pointer"}}></img> Facebook
                    </p>
                    <p>
                      <img width="30" height="30" src={icon2} alt="img" style={{cursor:"pointer"}}></img> Twitter
                    </p>
                    <p>
                      <img width="30" height="30" src={icon4} alt="img" style={{cursor:"pointer"}}></img> Youtube
                    </p>
                    <p>
                      <img width="30" height="30" src={icon5} alt="img" style={{cursor:"pointer"}}></img> Instagram
                    </p>
                    <p>
                      <img width="30" height="30" src={icon3} alt="img" style={{cursor:"pointer"}}></img> Linkedin
                    </p>
                  </Col>
                </Row>
            </Col>
            <Col md={6} >
                <div style={{border:"1px solid black", paddingLeft:"20px", paddingRight:"20px", marginTop:"10%", marginRight:"15%"}}>
                  <p style={{textAlign:"center", fontWeight:"bold", marginTop:"10px"}}>Formulario de contacto</p>
                  <input type="text" className="form-control inputs-styles" onChange={this.handleChange} placeholder={this.state.to} disabled id="to" value={this.to}></input>
                  <input type="text" className="form-control inputs-styles" onChange={this.handleChange} placeholder="Email" id="from" value={this.from}></input>
                  <input type="text" className="form-control inputs-styles" onChange={this.handleChange} placeholder="Nombre y Apellido" id="full_name" value={this.full_name}></input>
                  <input type="text" className="form-control inputs-styles" onChange={this.handleChange} placeholder="Asunto" id="subject" value={this.subject}></input>
                  <div className="superCenter">
                    <textarea rows="6" cols="50" style={{border:"1px solid blue"}} onChange={this.handleChange} value={this.message} id="message"></textarea>
                  </div>
                  <div className="superCenter mt-3 mb-3">
                    <Button
                      className="btn_blue"
                      onClick={() => this.handleSubmit()}
                    >
                      Enviar
                    </Button>
                  </div>
                </div>
            </Col>
        </Row>
    )
  }
}

export default Contactenos;