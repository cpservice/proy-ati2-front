import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import RegisterHeader from "../RegisterHeader";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

const InicioSesion = _ => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { opt6 } = useSelector(state => state.settings);
    const [state, setState] = useState({});

    const setOpt6 = useCallback(
        data => dispatch({ type: "SetRegisterOpt6", payload: data }),
        [dispatch]
    );

    const setLogin = useCallback(
        data => dispatch({ type: "SetLogin", payload: data }),
        [dispatch]
    );

    const checkForm = form => {
        if(form.email_signin){
        }else{
            toast.warn('Error: debe ingresar el correo electronico.');
            return false
        }
        if(form.password){
        }else{
            toast.warn('Error: debe ingresar la password.');
            return false
        }
        return true;
    };

    const handleChange = e => {
        if (e.target.id === "opt6") {
            setOpt6(e.target.checked);
            setState({
                ...state,
                newsletter: e.target.checked
            });
        } else {
            setState({
                ...state,
                [e.target.id]: e.target.value
            });
        }
    };

    useEffect(_ => {
        let check = document.getElementById("opt6");
        check.checked = opt6;
        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleNext = _ => {
        setLogin(state);
        if(checkForm(state)){
            if (opt6) {
                history.push("/register/frecuencia");
            } else {
                history.push("/register/facturacion");
            }
        }
    };

    return (
        <section>
            <Container>
                {/* {opt6 ? (
                    <RegisterHeader
                        title="DATOS DE INICIO DE SESIÓN"
                        step="3"
                        next="frecuencia"
                        handleNext={handleNext}
                    />
                ) : (
                    <RegisterHeader
                        title="DATOS DE INICIO DE SESIÓN"
                        step="3"
                        next="facturacion"
                        handleNext={handleNext}
                    />
                )} */}

                <RegisterHeader
                    title="DATOS DE INICIO DE SESIÓN"
                    step="3"
                    handleNext={handleNext}
                />

                <Row style={{ marginTop: "4rem" }}>
                    <Col>
                        <Form.Label>Correo electrónico</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control
                            type="email"
                            id="email_signin"
                            onChange={handleChange}
                        />
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Label>Contraseña</Form.Label>
                    </Col>
                    <Col>
                        <Form.Control
                            type="password"
                            id="password"
                            onChange={handleChange}
                        />
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col md={6}></Col>
                    <Col md={1} style={{ padding: "2px 0 0 15px" }}>
                        <Form.Check
                            type="checkbox"
                            id="opt6"
                            onChange={handleChange}
                        ></Form.Check>
                    </Col>
                    <Col md={5} style={{ padding: "0 0" }}>
                        <Form.Label htmlFor="opt6">
                            Quiero mantenerme informado acerca de los servicios
                            que ofrece la empresa, y otros aspectos de interés
                        </Form.Label>
                    </Col>
                </Row>
                <Row>
                    <Col style={{ textAlign: "center", marginTop: "20px" }}>
                        <Button
                            className="btn-custom"
                            onClick={_ => history.push("/")}
                        >
                            Cancelar
                        </Button>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

export default InicioSesion;
