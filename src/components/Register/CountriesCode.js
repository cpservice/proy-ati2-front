import React, {useEffect, useState} from 'react'
import countryTelData from 'country-telephone-data'
import ReactCountryFlag from "react-country-flag"
import Select from "react-select";

const formatOptionLabel = ({ value, label }) => (
<div style={{ display: "flex" }}>
    <ReactCountryFlag countryCode={label} svg />
    <div style={{ marginTop: '-4px', marginLeft: '5px'}}> +{value}</div>
</div>
);


export default function CountriesCode({ handleChange }) {

    const [ options, setOptions ] = useState([])

    useEffect( _ => {

        let newCodes = countryTelData.allCountries.map( (e) => {
            return ({
                value: e.dialCode,
                label: e.iso2
            })
        })

        setOptions(newCodes)

    }, [])

    return (
        <Select
            defaultValue={options[0]}
            formatOptionLabel={formatOptionLabel}
            options={options}
            onChange={handleChange}
        />
    )
}
