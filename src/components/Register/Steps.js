import React, { useState, useEffect, Fragment } from "react";
import { Container, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import "./index.css";

const Steps = _ => {
    const [active, setActive] = useState(["active", "", "", "", "", ""]);
    const { opt6 } = useSelector(state => state.settings);

    useEffect(
        _ => {
            const url = window.location.pathname.substr(10);

            let newActive;

            switch (url) {
                case "supo_nosotros":
                    newActive = ["active", "", "", "", "", ""];
                    setActive(newActive);
                    break;
                case "registrar_usuario":
                    newActive = ["active", "active", "", "", "", ""];
                    setActive(newActive);
                    break;
                case "idioma":
                    newActive = ["active", "active", "active", "", "", ""];
                    setActive(newActive);
                    break;
                case "inicio_sesion":
                    newActive = [
                        "active",
                        "active",
                        "active",
                        "active",
                        "",
                        ""
                    ];
                    setActive(newActive);
                    break;
                case "frecuencia":
                    newActive = [
                        "active",
                        "active",
                        "active",
                        "active",
                        "active",
                        ""
                    ];
                    setActive(newActive);
                    break;
                case "facturacion":
                    newActive = [
                        "active",
                        "active",
                        "active",
                        "active",
                        "active",
                        "active"
                    ];
                    setActive(newActive);
                    break;

                default:
                    break;
            }
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [window.location.pathname]
    );

    return (
        <section>
            <Container className="steps">
                <Row>
                    <Col style={{ textAlign: "center" }}>
                        <h3>PANEL DE REGISTRO</h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p>
                            <span>*</span> Estos son los pasos a seguir para
                            completar su registro.{" "}
                        </p>
                        <p>
                            <span>*</span> Si en algún momento necesita acceder
                            a una sección del registro,{" "}
                            <span className="link">
                                haga clic en el panel de color verde, que sea de
                                su preferencia
                            </span>
                        </p>
                    </Col>
                </Row>
                <Row style={{ marginTop: "10px", marginBottom: "10px" }}>
                    <Col className="steps-container">
                        <Row>
                            <Col className={active[0]}>
                                {" "}
                                <Link to="/register/supo_nosotros">
                                    <span>1- ¿Cómo supo de nosotros?</span>
                                </Link>
                            </Col>
                            <Col className={active[1]}>
                                {" "}
                                {active[1] === "active" ? (
                                    <Link to="/register/registrar_usuario">
                                        <span>2- Registrar usuario</span>
                                    </Link>
                                ) : (
                                    <span>2- Registrar usuario</span>
                                )}
                            </Col>
                            <Col className={active[2]}>
                                {active[2] === "active" ? (
                                    <Link to="/register/idioma">
                                        <span>3- Idioma del portal Web</span>
                                    </Link>
                                ) : (
                                    <span>3- Idioma del portal Web</span>
                                )}
                            </Col>
                        </Row>
                        <Row>
                            <Col className={active[3]}>
                                {" "}
                                {active[3] === "active" ? (
                                    <Link to="/register/inicio_sesion">
                                        <span>
                                            4- Datos de inicio de sesión
                                        </span>
                                    </Link>
                                ) : (
                                    <span>4- Datos de inicio de sesión</span>
                                )}
                            </Col>
                            {opt6 ? (
                                <Fragment>
                                    <Col className={active[4]}>
                                        {active[4] === "active" ? (
                                            <Link to="/register/frecuencia">
                                                <span>
                                                    5- Frecuencia e información
                                                    a recibir
                                                </span>
                                            </Link>
                                        ) : (
                                            <span>
                                                5- Frecuencia e información a
                                                recibir
                                            </span>
                                        )}
                                    </Col>
                                    <Col className={active[5]}>
                                        {active[5] === "active" ? (
                                            <Link to="/register/facturacion">
                                                <span>
                                                    6- Datos de facturación
                                                </span>
                                            </Link>
                                        ) : (
                                            <span>6- Datos de facturación</span>
                                        )}
                                    </Col>
                                </Fragment>
                            ) : (
                                <Fragment>
                                    <Col className={active[5]}>
                                        {active[5] === "active" ? (
                                            <Link to="/register/facturacion">
                                                <span>
                                                    5- Datos de facturación
                                                </span>
                                            </Link>
                                        ) : (
                                            <span>5- Datos de facturación</span>
                                        )}
                                    </Col>
                                </Fragment>
                            )}
                        </Row>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

export default Steps;
