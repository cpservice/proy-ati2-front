import React, { useState } from "react";
import Steps from "./Steps";
import Facturacion from "./Facturacion";
import Frecuencia from "./Frecuencia";
import InicioSesion from "./InicioSesion";
import RegistrarUsuario from "./RegistrarUsuario";
import RegistrarUsuarioEmp from "./RegistrarUsuarioEmp";
import SupoNosotros from "./SupoNosotros";
import Idioma from "./Idioma";
import { Switch, Route, Redirect } from "react-router-dom";
/**
 * <Facturacion/>  <Frecuencia/> <InicioSesion/> <RegistrarUsuario/> <SupoNosotros/>
 *  <Idioma/>
 */

const Register = _ => {
    const [steps, setSteps] = useState([true, true, false, true, true, true]);
    const [num] = useState(16.6);
    const setStep = step => {
        console.log(step);
        let newSteps = [true, true, true, true, true, true];
        newSteps[step] = false;
        setSteps(newSteps);
    };

    return (
        <section>
            <Steps setStep={setStep} steps={steps} num={num} />

            <Switch>
                <Redirect exact from="/register" to="/register/supo_nosotros" />
                <Route
                    path="/register/supo_nosotros"
                    component={SupoNosotros}
                />
                <Route
                    path="/register/registrar_usuario"
                    component={RegistrarUsuario}
                />
                <Route
                    path="/register/registrar_usuario_emp"
                    component={RegistrarUsuarioEmp}
                />
                <Route path="/register/idioma" component={Idioma} />
                <Route
                    path="/register/inicio_sesion"
                    component={InicioSesion}
                />
                <Route path="/register/frecuencia" component={Frecuencia} />
                <Route path="/register/facturacion" component={Facturacion} />
            </Switch>
        </section>
    );
};
export default Register;
