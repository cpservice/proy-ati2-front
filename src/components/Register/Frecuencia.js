import React, { useState, useEffect, useCallback } from "react";
import RegisterHeader from "../RegisterHeader";
import { useHistory } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import Select from "react-select";
import axios from "axios";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";

// const url = "https://atinmueble.ml/api/socialmedias";
const url = process.env.REACT_APP_URL + "/api/socialmedias";

const Frecuencia = _ => {
    const history = useHistory();
    const dispatch = useDispatch();

    const [interests, setInterests] = useState([]);
    const [freq, setFreq] = useState("");
    const [other, setOther] = useState(false);
    const [medio1, setMedio1] = useState(false);
    const [medio2, setMedio2] = useState(false);
    const [medio3, setMedio3] = useState(false);
    const [medio4, setMedio4] = useState(false);
    const [medio5, setMedio5] = useState(false);
    const [medio6, setMedio6] = useState(false);
    const [redes1, setRedes1] = useState([]);
    const [redes2, setRedes2] = useState([]);
    const [options, setOptions] = useState([]);
    const [state, setState] = useState({});

    const setFrecuency = useCallback(
        data => dispatch({ type: "SetFrecuency", payload: data }),
        [dispatch]
    );

    const options0 = [
        { label: "Apartamentos de interes", value: "apartamentos_de_interes" },
        { label: "Asesoria profesional", value: "asesoria_profesional" }
    ];

    const checkForm = () => {
        if(freq === ''){
            toast.warn('Error: debe seleccionar la frecuencia.');
            return false;
        }
        if(interests.length === 0 ){
            toast.warn('Error: debe seleccionar al menos una categoria de interes.');
            return false;
        }
        if(medio1 === false && medio2 === false && medio3 === false && medio4 === false && medio5 === false && medio6 === false){
            toast.warn('Error: debe escoger un medio.');
            return false;
        }
        return true;
    };

    useEffect(_ => {
        axios
            .get(url)
            .then(res => {
                if (res.data.success) {
                    const { data } = res.data;

                    let newOpts = data.map(e => ({
                        label: e.name,
                        value: e.name.toLowerCase()
                    }));

                    setOptions(newOpts);
                }
            })
            .catch(err => {
                console.log(err.response);
            });
    }, []);

    const handleFreq = e => {
        if (e.target.value === "other" && e.target.checked) {
            setOther(true);
        } else {
            setOther(false);
            setFreq(e.target.value);
        }
    };

    const handleOther = e => {
        setFreq(e.target.value);
    };

    const handleChange = e => {
        setState({
            ...state,
            [e.target.id]: e.target.value
        });
    };

    const handleNext = _ => {
        //frequency
        // console.log(freq);

        //services_interest
        // console.log(interests);

        //keep_me_updated
        const keep_me_updated = {
            ...state,
            company_sm: redes1, // sm: Social Medias
            my_sm: redes2
        };
        // console.log(keep_me_updated);

        setFrecuency({
            frequency: JSON.stringify(freq),
            services_interest: JSON.stringify(interests),
            keep_me_updated: JSON.stringify(keep_me_updated)
        });
        if(checkForm()){
            history.push("/register/facturacion");
        }
    };

    return (
        <section>
            <Container>
                <RegisterHeader
                    title="FRECUENCIA E INFORMACIÓN A RECIBIR"
                    step="4"
                    next="facturacion"
                    handleNext={handleNext}
                />

                <Row className="mt-4">
                    <Col>
                        <b>
                            ¿Con que frecuencia le gustaría mantenerse informado
                            acerca de los servicios que ofrece la empresa?
                        </b>
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Check
                            type="radio"
                            name="frequency"
                            label="1 vez a la semana"
                            value="one-a-week"
                            onChange={handleFreq}
                        />
                    </Col>
                    <Col>
                        <Form.Check
                            type="radio"
                            name="frequency"
                            value="two-a-week"
                            label="Cada 2 semanas"
                            onChange={handleFreq}
                        />
                    </Col>
                    <Col>
                        <Form.Check
                            type="radio"
                            name="frequency"
                            value="one-a-month"
                            label="1 vez al mes"
                            onChange={handleFreq}
                        />
                    </Col>
                    <Col>
                        <Form.Check
                            type="radio"
                            name="frequency"
                            label="Otra"
                            value="other"
                            onChange={handleFreq}
                        />
                    </Col>
                    {other && (
                        <Col>
                            <Form.Control
                                type="text"
                                onChange={handleOther}
                                placeholder=""
                            />
                        </Col>
                    )}
                </Row>
                <Row className="mt-4">
                    <Col>Servicios de interés</Col>
                    <Col>Puede seleccionar más de 1 categoría, si lo desea</Col>
                    <Col>
                        <Select
                            value={interests}
                            onChange={setInterests}
                            options={options0}
                            isMulti
                        />
                    </Col>
                </Row>
                <Row className="mt-4">
                    <Col>
                        Medio(s) por los que le gustaría mantenerse informado
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Check
                            type="checkbox"
                            label="Correo electrónico según sus preferencias"
                            onChange={e => setMedio1(e.target.checked)}
                        />
                    </Col>
                    {medio1 && (
                        <Col>
                            <Form.Control
                                type="email"
                                id="email_prefer"
                                onChange={handleChange}
                            />
                        </Col>
                    )}
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Check
                            type="checkbox"
                            label="Redes sociales de la empresa"
                            onChange={e => setMedio2(e.target.checked)}
                        />
                    </Col>

                    {medio2 && (
                        <Col>
                            <Select
                                placeholder="Seleccione la(s) red social"
                                value={redes1}
                                onChange={setRedes1}
                                options={options}
                                isMulti
                            />
                        </Col>
                    )}
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Check
                            type="checkbox"
                            label="Mis redes sociales"
                            onChange={e => setMedio3(e.target.checked)}
                        />
                    </Col>

                    {medio3 && (
                        <Col>
                            <Select
                                placeholder="Seleccione la(s) red social"
                                value={redes2}
                                onChange={setRedes2}
                                options={options}
                                isMulti
                            />
                        </Col>
                    )}
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Check
                            type="checkbox"
                            label="Mensajes de texto"
                            onChange={e => setMedio4(e.target.checked)}
                        />
                    </Col>

                    {medio4 && (
                        <Col>
                            <Form.Label>
                                Verifique, o inserte el número por el que desea
                                mantenerse informado
                            </Form.Label>
                            <Form.Control
                                type="text"
                                id="text_number"
                                onChange={handleChange}
                            />
                        </Col>
                    )}
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Check
                            type="checkbox"
                            label="Otros(s)"
                            onChange={e => setMedio5(e.target.checked)}
                        />
                    </Col>

                    {medio5 && (
                        <Col>
                            <Form.Label>
                                Especifique los medios por los que desea
                                mantenerse informado
                            </Form.Label>
                            <Form.Control
                                type="text"
                                id="others"
                                onChange={handleChange}
                            />
                        </Col>
                    )}
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Check
                            type="checkbox"
                            label="Mensaje privado en mi cuenta de Facebook"
                            onChange={e => setMedio6(e.target.checked)}
                        />
                    </Col>

                    {medio6 && (
                        <Col>
                            <Form.Label>
                                Introduzca dirección de correo de Facebook para
                                contactarlo
                            </Form.Label>
                            <Form.Control
                                type="text"
                                id="facebook_message"
                                onChange={handleChange}
                            />
                        </Col>
                    )}
                </Row>
                <Row>
                    <Col style={{ textAlign: "center", marginTop: "20px" }}>
                        <Button
                            className="btn-custom"
                            onClick={_ => history.push("/")}
                        >
                            Cancelar
                        </Button>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

export default Frecuencia;
