import React, { useState, useEffect, useCallback } from "react";
import RegisterHeader from "../RegisterHeader";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import CountriesCode from "./CountriesCode";
import csc from "country-state-city";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";

const RegistrarUsuarioEmp = _ => {
    const history = useHistory();
    const dispatch = useDispatch();

    const [showMovil, setShowMovil] = useState(false);
    const [showFijo, setShowFijo] = useState(false);
    const [state, setState] = useState({});
    const [countries, setCountries] = useState(false);
    const [states, setStates] = useState(false);
    const [cities, setCities] = useState(false);
    const [fijo, setFijo] = useState({ code: "", number: "", Ext:''});
    const [movil, setMovil] = useState({ code: "", number: "" });
    const setSignUpUser = useCallback(
        data => dispatch({ type: "SetSignUpUser", payload: data }),
        [dispatch]
    );

    const checkForm = form => {
        if(form.company_name){
        }else{
            toast.warn('Error: debe ingresar el nombre.');
            return false
        }
        if(form.company_rif){
        }else{
            toast.warn('Error: debe ingresar el rif.');
            return false
        }
        if(form.company_country){
        }else{
            toast.warn('Error: debe seleccionar un pais.');
            return false
        }
        if(form.company_state){
        }else{
            toast.warn('Error: debe seleccionar un estado.');
            return false
        }
        if(form.company_city){
        }else{
            toast.warn('Error: debe seleccionar un ciudad.');
            return false
        }
        if(form.company_address){
        }else{
            toast.warn('Error: debe ingresar la direccion.');
            return false
        }
        if(form.first_name){
        }else{
            toast.warn('Error: debe ingresar el nombre.');
            return false
        }
        if(form.last_name){
        }else{
            toast.warn('Error: debe ingresar el apellido.');
            return false
        }
        if(form.email){
        }else{
            toast.warn('Error: debe ingresar un email.');
            return false
        }
        if(JSON.stringify(movil) === JSON.stringify({ code: '', number: '' }) && JSON.stringify(fijo) === JSON.stringify({ code: '', number: '', Ext:'' })){
            toast.warn('Error: debe ingresar un numero movil y/o fijo.');
            return false;
        }else if(JSON.stringify(movil) !== JSON.stringify({ code: '', number: '' }) && movil.code === '' ){
            toast.warn('Error: debe seleccionar el codigo del movil.');
            return false;
        }else if(JSON.stringify(movil) !== JSON.stringify({ code: '', number: '' }) && movil.number === '' ){
            toast.warn('Error: debe ingresar el numero del movil.');
            return false;
        }else if(JSON.stringify(fijo) !== JSON.stringify({ code: '', number: '', Ext:'' }) && fijo.code === '' ){
            toast.warn('Error: debe seleccionar el codigo del fijo.');
            return false;
        }else if(JSON.stringify(fijo) !== JSON.stringify({ code: '', number: '', Ext:'' }) && fijo.number === '' ){
            toast.warn('Error: debe ingresar el numero del fijo.');
            return false;
        }else if(JSON.stringify(fijo) !== JSON.stringify({ code: '', number: '', Ext:'' }) && fijo.Ext === '' ){
            toast.warn('Error: debe ingresar la extension del fijo.');
            return false;
        }
        return true;
    };

    const handleTel = e => {
        switch (e.target.id) {
            case "movil":
                setShowMovil(e.target.checked);
                break;
            case "fijo":
                setShowFijo(e.target.checked);
                break;
            default:
                break;
        }
    };

    useEffect(_ => {
        setCountries(csc.getAllCountries());
        setStates(csc.getStatesOfCountry("1"));
        setCities(csc.getCitiesOfState("42"));

        // setState({
        //     ...state,
        //     company_country: csc.getAllCountries()[0].name,
        //     company_state: csc.getStatesOfCountry("1")[0].name,
        //     company_city: csc.getCitiesOfState("42")[0].name
        // });
        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleChange = e => {
        if (e.target.id === "full_name") {
            const fullName = e.target.value.split(" ");
            setState({
                ...state,
                first_name: fullName[0] || "",
                last_name: fullName[1] || ""
            });
        } else {
            setState({
                ...state,
                [e.target.id]: e.target.value
            });
        }
    };

    const handleCountry = e => {
        const id = e.target.value;

        setStates(csc.getStatesOfCountry(id));
        setCities([]);

        setState({
            ...state,
            company_country: e.target.value
        });
    };

    const handleState = e => {
        const id = e.target.value;

        setCities(csc.getCitiesOfState(id));

        setState({
            ...state,
            company_state: e.target.value
        });
    };

    const handleCity = e => {
        setState({
            ...state,
            company_city: e.target.value
        });
    };

    const handleCountryCodeF = e => {
        setFijo({
            ...fijo,
            code: e.value
        });
    };

    const handleCountryCodeM = e => {
        setMovil({
            ...movil,
            code: e.value
        });
    };

    const handleNext = _ => {
        setSignUpUser({
            ...state,
            mobile: `${movil.code}${movil.number}`,
            phone: `${fijo.code}${fijo.number}`,
            extension: fijo.ext || "",
            type: "company"
        });
        if (checkForm(state)) {
            history.push('/register/idioma');
        } 
    };

    const handleChangePh = e => {
        switch (e.target.id) {
            case "movil":
                setMovil({
                    ...movil,
                    number: e.target.value
                });
                break;
            case "fijo":
                setFijo({
                    ...fijo,
                    number: e.target.value
                });
                break;
            case "ext":
                setFijo({
                    ...fijo,
                    ext: e.target.value
                });
                break;
            default:
                break;
        }
    };

    return (
        <section>
            <Container>
                <RegisterHeader
                    title="REGISTRAR USUARIO"
                    step="1"
                    next="idioma"
                    handleNext={handleNext}
                />
                <Row>
                    <Col>
                        <Row>
                            <Col style={{ textAlign: "center" }}>
                                Datos de la Empresa
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Nombre de la empresa</Form.Label>
                                <Form.Control
                                    maxLength="100"
                                    minLength="1"
                                    type="text"
                                    id="company_name"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Razón social / RIF</Form.Label>
                                <Form.Control
                                    maxLength="100"
                                    minLength="1"
                                    id="company_rif"
                                    type="text"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>País de procedencia</Form.Label>
                                <Form.Control
                                    as="select"
                                    onChange={handleCountry}
                                >
                                    <option>
                                        Selecciona el pais
                                    </option>
                                    {countries &&
                                        countries.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                </Form.Control>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Estado</Form.Label>
                                <Form.Control
                                    as="select"
                                    onChange={handleState}
                                >
                                    <option>
                                        Selecciona el estado
                                    </option>
                                    {states &&
                                        states.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                </Form.Control>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Ciudad</Form.Label>
                                <Form.Control as="select" onChange={handleCity}>
                                    <option>
                                        Selecciona el ciudad
                                    </option>
                                    {cities &&
                                        cities.map((p, i) => (
                                            <option key={i} value={p.id}>
                                                {p.name}
                                            </option>
                                        ))}
                                </Form.Control>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Dirección</Form.Label>
                                <Form.Control
                                    onChange={handleChange}
                                    id="company_address"
                                    as="textarea"
                                    rows="3"
                                />
                            </Col>
                        </Row>
                    </Col>

                    <Col>
                        <Row>
                            <Col style={{ textAlign: "center" }}>
                                Datos del representante de la empresa
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Nombre y apellido</Form.Label>
                                <Form.Control
                                    id="full_name"
                                    onChange={handleChange}
                                    type="text"
                                    placeholder=""
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>Correo electrónico</Form.Label>
                                <Form.Control
                                    id="email"
                                    onChange={handleChange}
                                    type="email"
                                    placeholder=""
                                />
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <Row>
                                    <Col>Teléfono</Col>
                                </Row>
                                <Row className="mt-2">
                                    <Col>
                                        <b>
                                            Seleccione, el o los teléfonos de su
                                            preferencia
                                        </b>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Check
                                            type="checkbox"
                                            label="Móvil"
                                            onChange={handleTel}
                                            id="movil"
                                        />
                                    </Col>
                                    <Col>
                                        <Form.Check
                                            type="checkbox"
                                            label="Fijo"
                                            onChange={handleTel}
                                            id="fijo"
                                        />
                                    </Col>
                                </Row>
                                <Row className="mt-2">
                                    {showMovil && (
                                        <>
                                            <Col md={3}>
                                                <CountriesCode
                                                    handleChange={
                                                        handleCountryCodeM
                                                    }
                                                />
                                            </Col>
                                            <Col md={3}>
                                                <Form.Control
                                                    type="number"
                                                    placeholder=""
                                                    id="movil"
                                                    onChange={handleChangePh}
                                                    maxLength={20}
                                                />
                                            </Col>
                                        </>
                                    )}
                                </Row>
                                <Row className="mt-2">
                                    {showFijo && (
                                        <>
                                            <Col md={3}>
                                                <CountriesCode
                                                    handleChange={
                                                        handleCountryCodeF
                                                    }
                                                />
                                            </Col>
                                            <Col md={3}>
                                                <Form.Control
                                                    type="number"
                                                    placeholder=""
                                                    id="fijo"
                                                    onChange={handleChangePh}
                                                    maxLength={20}
                                                />
                                            </Col>
                                            <Col md={1}>Ext</Col>
                                            <Col md={3}>
                                                {/* <Form.Label>Ext</Form.Label> */}
                                                <Form.Control
                                                    type="number"
                                                    id="ext"
                                                    onChange={handleChangePh}
                                                    maxLength={20}
                                                />
                                            </Col>
                                        </>
                                    )}
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <p>
                            Por favor verifique que sus datos sean los
                            correctos, ya que serán utilizados para generar su
                            publicación, enviarle sus datos de acceso,
                            iniscribirse en un adiestramiento, o solicitar
                            cotizaciones
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col style={{ textAlign: "center", marginTop: "20px" }}>
                        <Button
                            className="btn-custom"
                            onClick={_ => history.push("/")}
                        >
                            Cancelar
                        </Button>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

export default RegistrarUsuarioEmp;
