import React, { useState, useEffect, useCallback } from 'react';
import RegisterHeader from '../RegisterHeader';
import { useHistory } from 'react-router-dom';
import { Container, Row, Col, Form, Button, Modal } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import csc from 'country-state-city';
import axios from 'axios';
import CheckBox from 'react-animated-checkbox';
import { toast } from "react-toastify";

const Facturacion = _ => {
    const history = useHistory();
    const dispatch = useDispatch();

    const { howKnow, signUpUser, lang, login, frequency } = useSelector(
        state => state.register
    );
    const [countries, setCountries] = useState(false);

    const [from_bank, setFromBank] = useState('');
    const [country_from_bank, setContryFrom] = useState('');
    const [success, setSuccess] = useState(false);

    const setIniciarSesion = useCallback(
        data => dispatch({ type: 'SetIniciarSesion', payload: data }),
        [dispatch]
    );

    const checkForm = () => {
        if(from_bank !== ''){
        }else{
            toast.warn('Error: debe ingresar el banco de origen.');
            return false
        }
        if(country_from_bank !== ''){
        }else{
            toast.warn('Error: debe seleccionar el pais del banco de origen.');
            return false
        }
        return true;
    };

    const to_bank_account =
        'Banesco Panamá - Cuenta nro: 201800948693 - Código SWIFT: BANSPAPA';

    useEffect(_ => {
        setCountries(csc.getAllCountries());
        // setContryFrom(csc.getAllCountries()[0].name);
    }, []);

    const handleCountry = e => {
        setContryFrom(e.target.value);
    };

    const handleSubmit = _ => {
        let data = {
            from_bank,
            country_from_bank,
            to_bank_account,
            lang,
            how_know: JSON.stringify(howKnow),
            ...signUpUser,
            ...login,
            ...frequency
        };
        // console.log(data);

        const url = process.env.REACT_APP_URL;
        // console.log("AAAAAAAAAA");
        if(checkForm()){
            axios
            .post(url + '/api/auth/signup', data)
            .then(res => {
                // console.log(res.data);
                if (res.data.success) {
                    setSuccess(true);
                }
            })
            .catch(err => {
                console.log(err);
            });
        }
    };

    const handleClose = _ => {
        setSuccess(!success);
    };

    const handleClose2 = _ => {
        setSuccess(false);
        history.push('/');
        setTimeout(() => {
            setIniciarSesion(true);
        }, 700);
    };

    return (
        <section>
            <Container>
                <RegisterHeader title="DATOS DE FACTURACIÓN" step="5" />

                <Row className="mt-4">
                    <Col>Su código de cliente es: XXXXXX</Col>
                    <Col>
                        Es importante que usted FORMALICE su pago para poder
                        publicar su vivienda, mediante su PAGO.
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p>
                            Datos a utilizar, para facilitar las notificaciones
                            de pago
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p>
                            Queremos ayudarte, facilitando el pago de la
                            publicación de tu apartamento o casa, y para lo
                            cual, te pedimos que proporciones los datos que
                            consideras utilizar, para realizar tus
                            transferencias o depósitos, especificados a
                            continuación.
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Row>
                            <Col>
                                <span>Banco origen</span>
                            </Col>
                            <Col>
                                <Form.Control
                                    type="text"
                                    onChange={e => setFromBank(e.target.value)}
                                />
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <span>
                                    País donde reside la cuenta del banco origen
                                </span>
                            </Col>
                            <Col>
                                <Form.Control
                                    as="select"
                                    onChange={handleCountry}
                                >
                                    <option>
                                        Seleccione el pais
                                    </option>
                                    {countries &&
                                        countries.map((p, i) => (
                                            <option key={i} value={p.name}>
                                                {p.name}
                                            </option>
                                        ))}
                                </Form.Control>
                            </Col>
                        </Row>
                    </Col>
                    <Col>
                        <Row>
                            <Col>
                                <span>Banco destino</span>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Control as="select">
                                    <option>{to_bank_account}</option>
                                </Form.Control>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Row>
                            <Col>
                                <br/>
                                <b>Sugerencia para realizar sus pagos</b>
                                <p>
                                    Se recomienda que inicie sesión en el portal
                                    Web de la empresa, para que los datos que ha
                                    proporcionado, puedan ser utilizados, para
                                    facilitarle, el proceso de notificación de
                                    pago de su publicación. De lo contrario,
                                    deberá llenar la información solicitada, en
                                    el portal Web de la empresa, y en el portal
                                    de notificación de pagos.
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <b>Pasos para notificar su pago</b>
                                <p>
                                    1- Cuando usted se registra, el portal le
                                    genera el código de cliente, que ve en
                                    pantalla, y que le será enviado a su correo,
                                    junto con sus datos de acceso.
                                </p>

                                <b>Su código de cliente es: XXXXXXX</b>

                                <p>
                                    2- Luego utilice el código proporcionado, y
                                    acceda al sistema de notificiación de pagos
                                    de la empresa, haciendo clic aquí:
                                    notificarPagos
                                </p>

                                <p>
                                    3- Introduzca el código del cliente, en el
                                    sistema de notificiación de pago
                                </p>

                                <p>
                                    4- Presione el botón Notificar pago, en el
                                    sistema de notificación de pagos.
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <b>Para transferencias bancarias o depósito</b>
                                <p style={{ textAlign: 'center' }}>
                                    <b>Datos de pago</b>
                                </p>
                                <p>Banco: Banesco Panama</p>
                                <p>Nro de cuenta: 201800948693</p>
                                <p>Código SWIFT: BANSPAPA</p>
                                <p>A nombre de: nombre d ela empresa C. A</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col>
                        <Row>
                            <Col>
                                <p>Horario de atención al público</p>
                                <p>Lunes a Viernes</p>
                                <p>De 8:00am a 12:00 m y de 1:00pm a 5:00pm</p>
                                <p>Teléfonos:</p>
                                <p>0414-389-74-44</p>
                                <p>0058-0212-362-82-68</p>
                                <p>Email: nirvana01@gmail.com</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col style={{ textAlign: 'center', marginTop: '20px' }}>
                        <Button
                            className="btn-custom mr-4"
                            onClick={handleSubmit}
                        >
                            Registrarme
                        </Button>
                        <Button
                            className="btn-custom"
                            onClick={_ => history.push('/')}
                        >
                            Cancelar
                        </Button>
                    </Col>
                </Row>
            </Container>

            <Modal show={success} onHide={handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Registro Existoso</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container className="text-center">
                        <Row className="my-3">
                            <Col>
                                <CheckBox
                                    checked={success}
                                    checkBoxStyle={{
                                        checkedColor: '#34b93d',
                                        size: 100,
                                        unCheckedColor: '#b8b8b8'
                                    }}
                                    duration={10000}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button
                                    className="btn-custom"
                                    onClick={_ => history.push('/')}
                                >
                                    Inicio
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    className="btn-custom"
                                    onClick={handleClose2}
                                >
                                    Iniciar Sesión
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
            </Modal>
        </section>
    );
};

export default Facturacion;
