import React, { useState, useEffect, useCallback } from 'react';
import RegisterHeader from '../RegisterHeader';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import csc from 'country-state-city';
import CountriesCode from './CountriesCode';
import { useDispatch } from 'react-redux';
import { toast } from "react-toastify";

const RegistrarUsuario = _ => {
    const history = useHistory();
    const dispatch = useDispatch();

    const [showMovil, setShowMovil] = useState(false);
    const [showFijo, setShowFijo] = useState(false);
    const [countries, setCountries] = useState(false);
    const [boolNatural, setBoolNatural] = useState(false);
    const [state, setState] = useState({});
    const [fijo, setFijo] = useState({ code: '', number: '' , Ext:''});
    const [movil, setMovil] = useState({ code: '', number: '' });

    const setSignUpUser = useCallback(
        data => dispatch({ type: 'SetSignUpUser', payload: data }),
        [dispatch]
    );

    const checkForm = form => {
        if (boolNatural === true) {
            if(form.first_name){
                if(form.first_name.length > 20){
                    toast.warn('Error: maximo 20 caracteres para el nombre.');
                    return false;
                }
            }else{
                toast.warn('Error: debe ingresar el nombre.');
                return false
            }
            if(form.last_name){
                if(form.last_name.length > 20){
                    toast.warn('Error: maximo 20 caracteres para el apellido.');
                    return false;
                }
            }else{
                toast.warn('Error: debe ingresar el apellido.');
                return false
            }
            if(form.dni){
                if(form.dni.length > 20){
                    toast.warn('Error: maximo 20 caracteres para la cedula.');
                    return false;
                }
            }else{
                toast.warn('Error: debe ingresar la cedula.');
                return false
            }
            if(form.email){
                if(form.email.length > 100){
                    toast.warn('Error: maximo 100 caracteres para el email.');
                    return false;
                }
            }else{
                toast.warn('Error: debe ingresar la email.');
                return false
            }
            if(form.country){
                if(form.country === ''){
                    toast.warn('Error: debe seleccionar un pais.');
                    return false;
                }
            }else{
                toast.warn('Error: debe seleccionar un pais.');
                return false
            }
            if(JSON.stringify(movil) === JSON.stringify({ code: '', number: '' }) && JSON.stringify(fijo) === JSON.stringify({ code: '', number: '', Ext:'' })){
                toast.warn('Error: debe ingresar un numero movil y/o fijo.');
                return false;
            }else if(JSON.stringify(movil) !== JSON.stringify({ code: '', number: '' }) && movil.code === '' ){
                toast.warn('Error: debe seleccionar el codigo del movil.');
                return false;
            }else if(JSON.stringify(movil) !== JSON.stringify({ code: '', number: '' }) && movil.number === '' ){
                toast.warn('Error: debe ingresar el numero del movil.');
                return false;
            }else if(JSON.stringify(fijo) !== JSON.stringify({ code: '', number: '', Ext:'' }) && fijo.code === '' ){
                toast.warn('Error: debe seleccionar el codigo del fijo.');
                return false;
            }else if(JSON.stringify(fijo) !== JSON.stringify({ code: '', number: '', Ext:'' }) && fijo.number === '' ){
                toast.warn('Error: debe ingresar el numero del fijo.');
                return false;
            }else if(JSON.stringify(fijo) !== JSON.stringify({ code: '', number: '', Ext:'' }) && fijo.Ext === '' ){
                toast.warn('Error: debe ingresar la extension del fijo.');
                return false;
            }
        }
        return true;
    };

    const handleBoolNatural = () => {
        setBoolNatural(!boolNatural);
    };

    const handleTel = e => {
        switch (e.target.id) {
            case 'movil':
                setShowMovil(e.target.checked);
                break;
            case 'fijo':
                setShowFijo(e.target.checked);
                break;
            default:
                break;
        }
    };

    useEffect(_ => {
        setCountries(csc.getAllCountries());
    }, []);

    // useEffect(
    //     _ => {
    //         if (countries[0]) {
    //             setState({
    //                 ...state,
    //                 country: countries[0].name
    //             });
    //         }
    //     },
    //     //eslint-disable-next-line react-hooks/exhaustive-deps
    //     [countries]
    // );

    const handleChange = e => {
        setState({
            ...state,
            [e.target.id]: e.target.value
        });
    };

    const handleNext = _ => {
        setSignUpUser({
            ...state,
            mobile: `${movil.code}${movil.number}`,
            phone: `${fijo.code}${fijo.number}`,
            extension: fijo.ext || '',
            type: 'natural'
        });
        if (checkForm(state)) {
            history.push('/register/idioma');
        } 
    };

    const handleCountry = e => {
        setState({
            ...state,
            country: e.target.value
        });
    };

    const handleCountryCodeF = e => {
        setFijo({
            ...fijo,
            code: e.value
        });
    };

    const handleCountryCodeM = e => {
        setMovil({
            ...movil,
            code: e.value
        });
    };

    const handleChangePh = e => {
        switch (e.target.id) {
            case 'movil':
                setMovil({
                    ...movil,
                    number: e.target.value
                });
                break;
            case 'fijo':
                setFijo({
                    ...fijo,
                    number: e.target.value
                });
                break;
            case 'ext':
                setFijo({
                    ...fijo,
                    ext: e.target.value
                });
                break;
            default:
                break;
        }
    };

    return (
        <section>
            <Container>
                <RegisterHeader
                    title="REGISTRAR USUARIO"
                    step="1"
                    next="idioma"
                    handleNext={handleNext}
                />
                <Row className="mt-2">
                    <Col>
                        <b>Seleccione el tipo de usuario:</b>
                        <Form.Group as={Row}>
                            <Form.Label as="legend" column sm={2}></Form.Label>
                            <Col sm={5}>
                                <Form.Check
                                    type="radio"
                                    label="Persona Natural"
                                    name="natural"
                                    id="natural"
                                    onClick={handleBoolNatural}
                                />
                            </Col>
                            <Col sm={5}>
                                <Form.Check
                                    type="radio"
                                    label="Empresa"
                                    name="empresa"
                                    id="empresa"
                                    onClick={_ =>
                                        history.push(
                                            '/register/registrar_usuario_emp'
                                        )
                                    }
                                />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                {boolNatural && (
                    <div>
                        <Row className="mt-4">
                            <Col>
                                <b>
                                    Ingrese los datos solicitados a
                                    continuación:
                                </b>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <Form.Label>Nombre:</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Nombre completo"
                                    maxLength="50"
                                    minLength="1"
                                    id="first_name"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <Form.Label>Apellido:</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Apellido completo"
                                    maxLength="50"
                                    minLength="1"
                                    id="last_name"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <Form.Label>Cédula/pasaporte/DNI:</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="12345678"
                                    onChange={handleChange}
                                    id="dni"
                                />
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <Form.Label>Correo electrónico</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="correo@ejemplo.com"
                                    id="email"
                                    onChange={handleChange}
                                />
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <Form.Label>País de procedencia</Form.Label>
                                <Form.Control
                                    as="select"
                                    onChange={handleCountry}
                                >
                                    <option>
                                        Selecciona el pais
                                    </option>
                                    {countries &&
                                        countries.map((p, i) => (
                                            <option key={i} value={p.name}>
                                                {p.name}
                                            </option>
                                        ))}
                                </Form.Control>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <Row>
                                    <Col>Teléfono</Col>
                                </Row>
                                <Row className="mt-2">
                                    <Col>
                                        <b>
                                            Seleccione, el o los teléfonos de su
                                            preferencia
                                        </b>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Check
                                            type="checkbox"
                                            label="Móvil"
                                            onChange={handleTel}
                                            id="movil"
                                        />
                                    </Col>
                                    <Col>
                                        <Form.Check
                                            type="checkbox"
                                            label="Fijo"
                                            onChange={handleTel}
                                            id="fijo"
                                        />
                                    </Col>
                                </Row>
                                <Row className="mt-2">
                                    {showMovil && (
                                        <>
                                            <Col md={3}>
                                                {/* <Form.Control as="select" /> */}
                                                <CountriesCode
                                                    handleChange={
                                                        handleCountryCodeM
                                                    }
                                                />
                                            </Col>
                                            <Col md={3}>
                                                <Form.Control
                                                    type="number"
                                                    placeholder=""
                                                    id="movil"
                                                    onChange={handleChangePh}
                                                    maxLength={20}
                                                />
                                            </Col>
                                        </>
                                    )}
                                </Row>
                                <Row className="mt-2">
                                    {showFijo && (
                                        <>
                                            <Col md={3}>
                                                {/* <Form.Control as="select" /> */}
                                                <CountriesCode
                                                    handleChange={
                                                        handleCountryCodeF
                                                    }
                                                />
                                            </Col>
                                            <Col md={3}>
                                                <Form.Control
                                                    type="number"
                                                    placeholder=""
                                                    id="fijo"
                                                    onChange={handleChangePh}
                                                    maxLength={20}
                                                />
                                            </Col>
                                            <Col md={1}>Ext</Col>
                                            <Col md={3}>
                                                {/* <Form.Label>Ext</Form.Label> */}
                                                <Form.Control
                                                    type="number"
                                                    id="ext"
                                                    onChange={handleChangePh}
                                                    maxLength={20}
                                                />
                                            </Col>
                                        </>
                                    )}
                                </Row>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>
                                <p>
                                    Por favor verifique que sus datos sean los
                                    correctos, ya que serán utilizados para
                                    generar su publicación, enviarle sus datos
                                    de acceso, inscribirse en un adiestramiento,
                                    o solicitar cotizaciones.
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <Col
                                style={{
                                    textAlign: 'center',
                                    marginTop: '20px'
                                }}
                            >
                                <Button
                                    className="btn-custom"
                                    onClick={_ => history.push('/')}
                                >
                                    Cancelar
                                </Button>
                            </Col>
                        </Row>
                    </div>
                )}
            </Container>
        </section>
    );
};

export default RegistrarUsuario;
