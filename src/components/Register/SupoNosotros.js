import React, { useState, useEffect, useCallback } from "react";
import RegisterHeader from "../RegisterHeader";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Select from "react-select";
import axios from "axios";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";

const styles = {
    marginTop: "2rem"
};

// const url = 'https://api.atinmueble.ml/api/socialmedias';
const url = process.env.REACT_APP_URL + "/api/socialmedias";

const SupoNosotros = _ => {
    const history = useHistory();
    const dispatch = useDispatch();

    const [boolRedes, setBoolRedes] = useState(false);
    const [boolOther, setBoolOther] = useState(false);
    const [howKnow, setHowKnow] = useState({});
    const [options, setOptions] = useState([]);
    const [redes, setRedes] = useState(null);
    const [other, setOther] = useState("");

    const setHowKnowRedux = useCallback(
        data => dispatch({ type: "SetHowKnow", payload: data }),
        [dispatch]
    );

    const handleChange = e => {
        switch (e.target.value) {
            case "redes":
                setBoolRedes(true);
                setBoolOther(false);

                setHowKnow({
                    option: e.target.value
                });
                break;
            case "otro":
                setBoolRedes(false);
                setBoolOther(true);

                setHowKnow({
                    option: e.target.value
                });
                break;

            default:
                setBoolRedes(false);
                setBoolOther(false);

                setHowKnow({
                    option: e.target.value
                });
                break;
        }
    };

    useEffect(_ => {
        axios
            .get(url)
            .then(res => {
                if (res.data.success) {
                    const { data } = res.data;

                    let newOpts = data.map(e => ({
                        label: e.name,
                        value: e.name.toLowerCase()
                    }));

                    setOptions(newOpts);
                }
            })
            .catch(err => {
                console.log(err.response);
            });
    }, []);

    useEffect(
        _ => {
            setHowKnow({
                ...howKnow,
                data: redes
            });
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [redes]
    );

    useEffect(
        _ => {
            setHowKnow({
                ...howKnow,
                data: other
            });
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [other]
    );

    useEffect(
        _ => {
            // console.log(howKnow);
            setHowKnowRedux(howKnow);
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [howKnow]
    );

    const handleNext = _ => {
        if (howKnow.option) {
            history.push("/register/registrar_usuario");
        } else {
            toast.warn("Debe elegir al menos una opción");
        }
    };

    return (
        <div>
            <Container>
                <RegisterHeader
                    title="¿CÓMO SUPO DE NOSOTROS?"
                    step="0"
                    next="registrar_usuario"
                    handleNext={handleNext}
                />
                <Row style={styles}>
                    <Col>
                        <p>
                            Por favor, coméntenos, como se entero de los
                            servicios de la empresa
                        </p>
                        <p>
                            Es importante para nosotros, porque nos ayuda a
                            mejorar el servicio que le ofrecemos
                        </p>
                    </Col>
                </Row>
                <Row style={styles}>
                    <Col>
                        <Form.Group controlId="formCheckWeb">
                            <Form.Check
                                type="radio"
                                name="how_know"
                                label="Portal Web de la empresa"
                                value="portal"
                                onChange={handleChange}
                            />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group controlId="formCheckRedes">
                            <Form.Check
                                type="radio"
                                name="how_know"
                                label="Redes Sociales"
                                value="redes"
                                onChange={handleChange}
                            />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group controlId="formCheckAmigos">
                            <Form.Check
                                type="radio"
                                name="how_know"
                                label="Amigos"
                                value="amigos"
                                onChange={handleChange}
                            />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group controlId="formCheckOtro">
                            <Form.Check
                                type="radio"
                                name="how_know"
                                label="Otro"
                                value="otro"
                                onChange={handleChange}
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row style={{ marginTop: "1rem" }}>
                    <Col />
                    <Col>
                        {boolRedes && (
                            <Select
                                placeholder="Seleccione la(s) red social"
                                value={redes}
                                onChange={setRedes}
                                options={options}
                                isMulti
                            />
                        )}
                    </Col>
                    <Col />
                    <Col>
                        {boolOther && (
                            <Form.Control
                                type="text"
                                placeholder="Comentenos el medio"
                                onChange={e => setOther(e.target.value)}
                            />
                        )}
                    </Col>
                </Row>
                <Row style={styles}>
                    <Col style={{ textAlign: "center", marginTop: "20px" }}>
                        <Button
                            className="btn-custom"
                            onClick={_ => history.push("/")}
                        >
                            Cancelar
                        </Button>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default SupoNosotros;
