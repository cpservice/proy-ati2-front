import React, { useState, useEffect, useCallback } from "react";
import RegisterHeader from "../RegisterHeader";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";

const Idioma = _ => {
    const history = useHistory();
    const dispatch = useDispatch();

    const [lang, setLang] = useState("");

    const setLanguague = useCallback(
        data => dispatch({ type: "SetLang", payload: data }),
        [dispatch]
    );

    useEffect(
        _ => {
            setLanguague(lang);
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [lang]
    );

    const checkForm = () => {
        if(lang !== ''){
        }else{
            toast.warn('Error: debe seleccionar un idioma.');
            return false
        }
        return true;
    };

    const handleNext = _ => {
        if (checkForm()) {
            history.push('/register/inicio_sesion');
        } 
    };

    return (
        <section>
            <Container>
                <RegisterHeader
                    title="IDIOMA EN QUE DESEA VISUALIZAR EL PORTAL AL INICIAR SESIÓN"
                    step="2"
                    next="inicio_sesion"
                    handleNext={handleNext}
                />

                <Form.Group
                    as={Row}
                    style={{ textAlign: "center" }}
                    className="mt-4"
                >
                    <Form.Label as="legend" column sm={2}></Form.Label>
                    <Col sm={5}>
                        <Form.Check
                            type="radio"
                            label="Español"
                            name="language"
                            id="spanish"
                            className="text-center"
                            onClick={_ => setLang("es")}
                        />
                    </Col>
                    <Col sm={5}>
                        <Form.Check
                            type="radio"
                            label="Inglés"
                            name="language"
                            id="english"
                            className="text-left"
                            onClick={_ => setLang("en")}
                        />
                    </Col>
                </Form.Group>
                <Row>
                    <Col style={{ textAlign: "center", marginTop: "20px" }}>
                        <Button
                            className="btn-custom"
                            onClick={_ => history.push("/")}
                        >
                            Cancelar
                        </Button>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

export default Idioma;
