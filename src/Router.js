import React from 'react';
import Register from './components/Register';
import Contactenos from './components/Contactenos';
import Home from './components/Home';
import Publicar from './components/Publicar';
import VerPublicacion from './components/Publicar/VerPublicacion';
import Empleo from './components/Empleo';
import Vacante from './components/Empleo/Vacante';
import Services from './components/Services';
import See from './components/Services/See';
import ServiceCreate from './components/Services/Create';
import { Switch, Route } from 'react-router-dom';
import Crear from './components/Empleo/Crear';
import Ayuda from './components/Ayuda';
import Conocenos from './components/Conocenos';

const Router = _ => {
    return (
        <React.Fragment>
            <Switch>
                <Route path="/register" component={Register} />
                <Route path="/contact" component={Contactenos} />
                <Route exact path="/empleo" component={Empleo} />
                <Route path="/empleo/vacante" component={Vacante} />
                <Route exact path="/services" component={Services} />
                <Route path="/services/create" component={ServiceCreate} />
                <Route exact path="/services/:see" component={See} />
                <Route exact path="/publicar" component={Publicar} />
                <Route path="/publicar/ver" component={VerPublicacion} />
                <Route path="/crear" component={Crear} />
                <Route path="/ayuda" component={Ayuda} />
                <Route exact path="/conocenos" component={Conocenos} />
                <Route path="/" component={Home} />
            </Switch>
        </React.Fragment>
    );
};
export default Router;
